<?php
/*
Template Name: Form BENGO
*/
get_header(); ?>
<?php $contant_post_id = htmlspecialchars($_GET['post_id']);?>
<div class="front bengoform">
<div class="inner">
<header>
<?php echo get_post_meta($contant_post_id , 'bengo_catch', TRUE);?>
<h1><?php echo get_the_title($contant_post_id);?></h1>
</header>
<article>
<div class="bengo_detail">
<strong>お急ぎの方は、お電話でも無料相談を承っております。</strong>

<div class="contactfield">
<div class="tel">
<p><b>
<?php $jdi = japan_date_info();
if(get_post_meta($contant_post_id , 'bengo_open_option', TRUE) == 3) {
	$jdi_flag = TRUE;
} elseif(get_post_meta($contant_post_id , 'bengo_open_option', TRUE) == 2) {
	if(empty($jdi['2'])) $jdi_flag = TRUE;
	if($jdi['2'] == Sat || $jdi['2'] == Sun) $jdi_flag = TRUE;
} elseif(get_post_meta($contant_post_id , 'bengo_open_option', TRUE) == 1) {
	if(empty($jdi['2'])) $jdi_flag = TRUE;
	if($jdi['2'] == Sat) $jdi_flag = TRUE;
} elseif(get_post_meta($contant_post_id , 'bengo_open_option', TRUE) == 4) {
	if(empty($jdi['2'])) $jdi_flag = TRUE;
	if($jdi['2'] == Sun) $jdi_flag = TRUE;
}
if(empty($jdi['2'])) $jdi_flag = TRUE;//平日なら全部OK
?>
<?php if( $jdi_flag && (date_i18n("Hi") >= get_post_meta($contant_post_id , 'bengo_open_start', TRUE) and date_i18n("Hi") <= get_post_meta($contant_post_id , 'bengo_open_end', TRUE))):?>ただ今 <?php echo date_i18n("H:i");?>です。お気軽にお電話ください。<?php else:?>ただ今 <?php echo date_i18n("H:i");?>。電話受付時間外です。無料相談フォームよりご連絡ください。<?php endif;?></b></p>
<div class="helpline">
<strong>お電話での相談窓口(通話無料)</strong><a href="<?php echo get_permalink($contant_post_id);?>?pid=<?php echo $contant_post_id; ?>" rel="nofollow"><?php echo get_post_meta($contant_post_id , 'bengo_tel', TRUE);?></a>
<table>
<tr>
<th>受付時間</th>
<td><?php echo get_post_meta($contant_post_id , 'bengo_open', TRUE);?></td>
</tr>
</table>
</div>
</div>
<!-- //contactfield--></div>
</div>


<div class="bengo_image">
<?php echo wp_get_attachment_image(get_post_meta($contant_post_id , 'File Upload', TRUE), array('300', '225'), false, array('class' => 'visual', 'alt' => get_the_title($contant_post_id)));?>
</div>
</article>




<article>

<h2>無料相談フォーム</h2>
<?php if(is_page('32')):?>
<img src="<?php bloginfo('template_directory'); ?>/img/bg_flow01.png" />
<?php elseif(is_page('733')):?>
<img src="<?php bloginfo('template_directory'); ?>/img/bg_flow02.png" />
<?php elseif(is_page('735')):?>
<img src="<?php bloginfo('template_directory'); ?>/img/bg_flow03.png" />

<?php /* ◆◆◆◆◆◆  リスティングコンバージョンタグ  ◆◆◆◆◆ */ ?>
<!-- Google Code for &#20132;&#36890;&#20107;&#25925;&#12513;&#12540;&#12523;&#21839;&#12356;&#21512;&#12431;&#12379; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1005400785;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "_l4wCIakq2kQ0eW03wM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1005400785/?label=_l4wCIakq2kQ0eW03wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<?php endif; ?>
<div class="ssl">
<!-- GeoTrust Smart Icon tag. Do not edit. -->
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript" CHARSET="Shift_JIS" SRC="//smarticon.geotrust.com/si.js"></SCRIPT>
<!-- END of GeoTrust Smart Icon tag -->
交通事故弁護士相談広場では、GeoTrustのSSLサーバ証明書を使用して、お客様の個人情報を保護しています。<br>当サイトのすべてのページの情報はSSLで暗号化されてから送受信されます。
</div>
<?php while ( have_posts() ) : the_post();?>
<?php the_content(); ?>
<?php endwhile; ?>

</article>

<!-- //inner--></div>
<!-- //front--></div>

<?php get_footer();