<?php
/*
Template Name: Index
*/
get_header(); ?>


<div class="front top">
<div class="inner">
<h1>弁護士をお探しの交通事故被害者のためのポータルサイト</h1>

<div class="map">
<p class="presearch">地図から弁護士を探す</p>
<ul class="jp">
<li class="pr01"><a href="/bengo/hokkaido">北海道</a></li>
<li class="pr02"><a href="/bengo/aomori">青森</a></li>
<li class="pr03"><a href="/bengo/iwate">岩手</a></li>
<li class="pr04"><a href="/bengo/akita">秋田</a></li>
<li class="pr05"><a href="/bengo/miyagi">宮城</a></li>
<li class="pr06"><a href="/bengo/yamagata">山形</a></li>
<li class="pr07"><a href="/bengo/fukushima">福島</a></li>
<li class="pr08"><a href="/bengo/tokyo">東京</a></li>
<li class="pr09"><a href="/bengo/kanagawa">神奈川</a></li>
<li class="pr10"><a href="/bengo/saitama">埼玉</a></li>
<li class="pr14"><a href="/bengo/chiba">千葉</a></li>
<li class="pr12"><a href="/bengo/tochigi">栃木</a></li>
<li class="pr13"><a href="/bengo/gunma">群馬</a></li>
<li class="pr11"><a href="/bengo/ibaraki">茨城</a></li>
<li class="pr15"><a href="/bengo/niigata">新潟</a></li>
<li class="pr16"><a href="/bengo/toyama">富山</a></li>
<li class="pr17"><a href="/bengo/ishikawa">石川</a></li>
<li class="pr18"><a href="/bengo/fukui">福井</a></li>
<li class="pr19"><a href="/bengo/yamanashi">山梨</a></li>
<li class="pr20"><a href="/bengo/nagano">長野</a></li>
<li class="pr21"><a href="/bengo/gifu">岐阜</a></li>
<li class="pr22"><a href="/bengo/shizuoka">静岡</a></li>
<li class="pr23"><a href="/bengo/aichi">愛知</a></li>
<li class="pr24"><a href="/bengo/mie">三重</a></li>
<li class="pr25"><a href="/bengo/shiga">滋賀</a></li>
<li class="pr26"><a href="/bengo/kyoto">京都</a></li>
<li class="pr27"><a href="/bengo/osaka">大阪</a></li>
<li class="pr28"><a href="/bengo/hyogo">兵庫</a></li>
<li class="pr29"><a href="/bengo/nara">奈良</a></li>
<li class="pr30"><a href="/bengo/wakayama">和歌山</a></li>
<li class="pr31"><a href="/bengo/tottori">鳥取</a></li>
<li class="pr32"><a href="/bengo/shimane">島根</a></li>
<li class="pr33"><a href="/bengo/okayama">岡山</a></li>
<li class="pr34"><a href="/bengo/hiroshima">広島</a></li>
<li class="pr35"><a href="/bengo/yamaguchi">山口</a></li>
<li class="pr36"><a href="/bengo/tokushima">徳島</a></li>
<li class="pr37"><a href="/bengo/kagawa">香川</a></li>
<li class="pr38"><a href="/bengo/ehime">愛媛</a></li>
<li class="pr39"><a href="/bengo/kochi">高知</a></li>
<li class="pr40"><a href="/bengo/fukuoka">福岡</a></li>
<li class="pr41"><a href="/bengo/saga">佐賀</a></li>
<li class="pr42"><a href="/bengo/nagasaki">長崎</a></li>
<li class="pr43"><a href="/bengo/kumamoto">熊本</a></li>
<li class="pr44"><a href="/bengo/oita">大分</a></li>
<li class="pr45"><a href="/bengo/miyazaki">宮崎</a></li>
<li class="pr46"><a href="/bengo/kagoshima">鹿児島</a></li>
<li class="pr47"><a href="/bengo/okinawa">沖縄</a></li>
</ul>

<div class="search_naiyo" id="search_offer">
<h5>相談内容から弁護士を探す</h5>
<ul>
<li><a href="/bengo?jian=jian_isya">慰謝料</a></li>
<li><a href="/bengo?jian=jian_songai">損害賠償</a></li>
<li><a href="/bengo?jian=jian_jidan">示談交渉</a></li>
<li><a href="/bengo?jian=jian_kashitsu">過失割合</a></li>
<li><a href="/bengo?jian=jian_kouisyo">後遺障害</a></li>
<li><a href="/bengo?jian=jian_jinshin">人身事故</a></li>
<li><a href="/bengo?jian=jian_shibo">死亡事故</a></li>
<li><a href="/bengo?status=free">相談無料</a></li>
</ul>
</div>


</div>


<div class="search_area" id="search_area">
<h5>都道府県から弁護士を探す</h5>
<dl>
<dt>北海道・東北地方</dt>
<dd><a href="/bengo/hokkaido">北海道</a></dd>
<dd><a href="/bengo/aomori">青森</a></dd>
<dd><a href="/bengo/iwate">岩手</a></dd>
<dd><a href="/bengo/akita">秋田</a></dd>
<dd><a href="/bengo/miyagi">宮城</a></dd>
<dd><a href="/bengo/yamagata">山形</a></dd>
<dd><a href="/bengo/fukushima">福島</a></dd>
<dt>関東</dt>
<dd><a href="/bengo/tokyo">東京</a></dd>
<dd><a href="/bengo/saitama">埼玉</a></dd>
<dd><a href="/bengo/kanagawa">神奈川</a></dd>
<dd><a href="/bengo/chiba">千葉</a></dd>
<dd><a href="/bengo/ibaraki">茨城</a></dd>
<dd><a href="/bengo/tochigi">栃木</a></dd>
<dd><a href="/bengo/gunma">群馬</a></dd>
<dt>北陸・甲信越</dt>
<dd><a href="/bengo/niigata">新潟</a></dd>
<dd><a href="/bengo/yamanashi">山梨</a></dd>
<dd><a href="/bengo/nagano">長野</a></dd>
<dd><a href="/bengo/ishikawa">石川</a></dd>
<dd><a href="/bengo/toyama">富山</a></dd>
<dd><a href="/bengo/fukui">福井</a></dd>
<dt>東海</dt>
<dd><a href="/bengo/aichi">愛知</a></dd>
<dd><a href="/bengo/shizuoka">静岡</a></dd>
<dd><a href="/bengo/gifu">岐阜</a></dd>
<dd><a href="/bengo/mie">三重</a></dd>
<dt>関西</dt>
<dd><a href="/bengo/osaka">大阪</a></dd>
<dd><a href="/bengo/kyoto">京都</a></dd>
<dd><a href="/bengo/nara">奈良</a></dd>
<dd><a href="/bengo/hyogo">兵庫</a></dd>
<dd><a href="/bengo/shiga">滋賀</a></dd>
<dd><a href="/bengo/wakayama">和歌山</a></dd>
<dt>中国・四国</dt>
<dd><a href="/bengo/hiroshima">広島</a></dd>
<dd><a href="/bengo/okayama">岡山</a></dd>
<dd><a href="/bengo/yamaguchi">山口</a></dd>
<dd><a href="/bengo/tottori">鳥取</a></dd>
<dd><a href="/bengo/shimane">島根</a></dd>
<dd><a href="/bengo/kagawa">香川</a></dd>
<dd><a href="/bengo/tokushima">徳島</a></dd>
<dd><a href="/bengo/ehime">愛媛</a></dd>
<dd><a href="/bengo/kochi">高知</a></dd>
<dt>九州</dt>
<dd><a href="/bengo/fukuoka">福岡</a></dd>
<dd><a href="/bengo/kumamoto">熊本</a></dd>
<dd><a href="/bengo/oita">大分</a></dd>
<dd><a href="/bengo/miyazaki">宮崎</a></dd>
<dd><a href="/bengo/saga">佐賀</a></dd>
<dd><a href="/bengo/nagasaki">長崎</a></dd>
<dd><a href="/bengo/kagoshima">鹿児島</a></dd>
<dd><a href="/bengo/okinawa">沖縄</a></dd>
</dl>
<!-- //search_area--></div>

<div class="btn"><a href="/bengo">交通事故問題に強い弁護士を探す</a></div>
<div class="ranking">
<div class="inner">
<h5>交通事故問題に強い弁護士を「相談件数・閲覧数・おすすめ」でランキング！</h5>
<div class="topics">
    <ul class="menu">
        <li><a href="#tabs-1" class="tags">全国</a></li>
        <li><a href="#tabs-2" class="tags">関東</a></li>
        <li><a href="#tabs-3" class="tags">東海</a></li>
        <li><a href="#tabs-4" class="tags">関西</a></li>
    </ul>

<div id="tabs-1">
<ol>

<!--<?php
// 弁護士表示区分「全国ランキング」の記事リスト取得
$ret = $wpdb->get_results(TBO::getQueryOfPostsByBengoOrderTypeOrdered('rank_zenkoku'));

$args = array(
    'post_type' => 'bengo',
    'order' => 'DESC',
    'posts_per_page' => 5,
);
foreach($ret as $orders):
  $args['post__in'][] = $orders->ID;
endforeach;
$args['orderby'] = 'post__in';
$the_query = new WP_Query( $args );

?>-->
<?php
if($the_query->have_posts() && $args['post__in']): $iii = 1;
while ( $the_query->have_posts() ) : $the_query->the_post();
?>
<li id="rank<?php echo $iii;?>">
<a href="<?php the_permalink();?>"><?php echo wp_get_attachment_image(post_custom('File Upload'), array('130', '175'), false, array('class' => 'visual', 'alt' => get_the_title()));?></a><span id="rank<?php echo $iii;?>"><?php echo $iii;?></span>
<a href="<?php the_permalink();?>" class="ttl"><?php the_title(); ?></a><br />
<?php echo get_post_meta($post->ID , 'bengo_catch', TRUE);?>

<?php if(get_post_meta($post->ID , 'bengo_status', TRUE)):?><?php foreach(get_post_meta($post->ID , 'bengo_status', FALSE) as $tmp_status):?>
<?php if($tmp_status === 'area'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_area.png" class="status" /><?php break; endif;?>
<?php if($tmp_status === 'kantouzen'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_kanto.png" class="status" /><?php break; endif;?>
<?php if($tmp_status === 'kansaizen'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_kansai.png" class="status" /><?php break; endif;?>
<?php if($tmp_status === 'tokaizen'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_tokai.png" class="status" /><?php break; endif;?>
<?php endforeach;?><?php endif;?>
</li>
<?php $iii++; endwhile; endif; wp_reset_postdata();?>


</ol>
</div>
<div id="tabs-2">
<ol>

<!--<?php
// 弁護士表示区分「関東ランキング」の記事リスト取得
$ret = $wpdb->get_results(TBO::getQueryOfPostsByBengoOrderTypeOrdered('rank_kanto'));

$args = array(
    'post_type' => 'bengo',
    'order' => 'DESC',
    'posts_per_page' => 5,
);
foreach($ret as $orders):
  $args['post__in'][] = $orders->ID;
endforeach;
$args['orderby'] = 'post__in';
$the_query = new WP_Query( $args );

?>-->
<?php
if($the_query->have_posts() && $args['post__in']): $iii = 1;
while ( $the_query->have_posts() ) : $the_query->the_post();
?>
<li id="rank<?php echo $iii;?>">
<a href="<?php the_permalink();?>"><?php echo wp_get_attachment_image(post_custom('File Upload'), array('130', '175'), false, array('class' => 'visual', 'alt' => get_the_title()));?></a><span id="rank<?php echo $iii;?>"><?php echo $iii;?></span>
<a href="<?php the_permalink();?>" class="ttl"><?php the_title(); ?></a><br />
<?php echo get_post_meta($post->ID , 'bengo_catch', TRUE);?>

<?php if(get_post_meta($post->ID , 'bengo_status', TRUE)):?><?php foreach(get_post_meta($post->ID , 'bengo_status', FALSE) as $tmp_status):?>
<?php if($tmp_status === 'area'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_area.png" class="status" /><?php break; endif;?>
<?php if($tmp_status === 'kantouzen'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_kanto.png" class="status" /><?php break; endif;?>
<?php if($tmp_status === 'kansaizen'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_kansai.png" class="status" /><?php break; endif;?>
<?php if($tmp_status === 'tokaizen'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_tokai.png" class="status" /><?php break; endif;?>
<?php endforeach;?><?php endif;?>
</li>
<?php $iii++; endwhile; endif; wp_reset_postdata();?>


</ol>
</div>
<div id="tabs-3">
<ol>

<!--<?php
// 弁護士表示区分「東海ランキング」の記事リスト取得
$ret = $wpdb->get_results(TBO::getQueryOfPostsByBengoOrderTypeOrdered('rank_tokai'));

$args = array(
    'post_type' => 'bengo',
    'order' => 'DESC',
    'posts_per_page' => 5,
);
foreach($ret as $orders):
  $args['post__in'][] = $orders->ID;
endforeach;
$args['orderby'] = 'post__in';

$the_query = new WP_Query( $args );

?>-->
<?php
if($the_query->have_posts() && $args['post__in']): $iii = 1;
while ( $the_query->have_posts() ) : $the_query->the_post();
?>
<li id="rank<?php echo $iii;?>">
<a href="<?php the_permalink();?>"><?php echo wp_get_attachment_image(post_custom('File Upload'), array('130', '175'), false, array('class' => 'visual', 'alt' => get_the_title()));?></a><span id="rank<?php echo $iii;?>"><?php echo $iii;?></span>
<a href="<?php the_permalink();?>" class="ttl"><?php the_title(); ?></a><br />
<?php echo get_post_meta($post->ID , 'bengo_catch', TRUE);?>

<?php if(get_post_meta($post->ID , 'bengo_status', TRUE)):?><?php foreach(get_post_meta($post->ID , 'bengo_status', FALSE) as $tmp_status):?>
<?php if($tmp_status === 'area'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_area.png" class="status" /><?php break; endif;?>
<?php if($tmp_status === 'kantouzen'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_kanto.png" class="status" /><?php break; endif;?>
<?php if($tmp_status === 'kansaizen'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_kansai.png" class="status" /><?php break; endif;?>
<?php if($tmp_status === 'tokaizen'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_tokai.png" class="status" /><?php break; endif;?>
<?php endforeach;?><?php endif;?>
</li>
<?php $iii++; endwhile; endif; wp_reset_postdata();?>


</ol>
</div>
<div id="tabs-4">
<ol>

<!--<?php
// 弁護士表示区分「関西ランキング」の記事リスト取得
$ret = $wpdb->get_results(TBO::getQueryOfPostsByBengoOrderTypeOrdered('rank_kansai'));

$args = array(
    'post_type' => 'bengo',
    'order' => 'DESC',
    'posts_per_page' => 5,
);
foreach($ret as $orders):
  $args['post__in'][] = $orders->ID;
endforeach;
$args['orderby'] = 'post__in';

$the_query = new WP_Query( $args );

?>-->
<?php
if($the_query->have_posts() && $args['post__in']): $iii = 1;
while ( $the_query->have_posts() ) : $the_query->the_post();
?>
<li id="rank<?php echo $iii;?>">
<a href="<?php the_permalink();?>"><?php echo wp_get_attachment_image(post_custom('File Upload'), array('130', '175'), false, array('class' => 'visual', 'alt' => get_the_title()));?></a><span id="rank<?php echo $iii;?>"><?php echo $iii;?></span>
<a href="<?php the_permalink();?>" class="ttl"><?php the_title(); ?></a><br />
<?php echo get_post_meta($post->ID , 'bengo_catch', TRUE);?>

<?php if(get_post_meta($post->ID , 'bengo_status', TRUE)):?><?php foreach(get_post_meta($post->ID , 'bengo_status', FALSE) as $tmp_status):?>
<?php if($tmp_status === 'area'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_area.png" class="status" /><?php break; endif;?>
<?php if($tmp_status === 'kantouzen'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_kanto.png" class="status" /><?php break; endif;?>
<?php if($tmp_status === 'kansaizen'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_kansai.png" class="status" /><?php break; endif;?>
<?php if($tmp_status === 'tokaizen'):?><img src="<?php echo get_stylesheet_directory_uri() ?>/img/ic_st_tokai.png" class="status" /><?php break; endif;?>
<?php endforeach;?><?php endif;?>
</li>
<?php $iii++; endwhile; endif; wp_reset_postdata();?>


</ol>
</div>
</div>
<!-- //inner--></div>
<!-- //ranking--></div>

</div>
<!-- //front--></div>

<div id="wrap">


<div class="content_list">
<h2>【徹底解説】交通事故 ～後遺障害から弁護士相談まで～</h2>
<p>交通事故に巻き込まれてしまったら、どのように行動し、対処すべきなのでしょう。ここでは交通事故に関する知っておくべき知識から、示談交渉や後遺障害認定で役に立つ内容を分かりやすく、丁寧に解説しています。不幸にも事故の被害者になってしまった方が本当に「知りたい」内容を掲載しています。</p>
<ul>
<?php $arr_post_oya = array('1','3','9','4','5','8','14','46','7','26','10','6'); //カテゴリID
$num=0;
$term_catch = get_option('term_catch');
?>
<?php foreach($arr_post_oya as $post_oya):?>
<?php
$args = array(
    'post_type' => 'post',
    'category' => $post_oya,
    //'explode' => '';
    'orderby' => 'modified',
    'posts_per_page' => '5'
);
$myposts = get_posts( $args );
?>
<li class="<?php if($num % 2 == 0){ echo "lil";}else{ echo "lir" ;} ?>">
<dl>
	<dt><span class="ttl"><?php echo get_cat_name($post_oya);?></span></dt>
<?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	<dd class="wraplink"><?php catch_that_image('150', '150');?><a href="<?php the_permalink();?>"><?php the_title();?></a>
    <p><?php
$content = apply_filters('the_content',get_the_content());
$content = strip_tags($content);
$content = mb_substr($content, 0, 50);
echo $content; ?>...</p>
    </dd>
<?php endforeach; wp_reset_postdata(); ?>
</dl>
<a href="<?php echo get_category_link($post_oya);?>" class="list_link">「<?php echo get_cat_name($post_oya);?>」一覧 (全<?php $catcount = get_category($post_oya); echo $catcount->count;?>件)</a>
</li>
<?php
$num++;
 endforeach; ?>
</ul>


</div>


<div id="main">

<div class="single_doc">
<article class="top">
<h2><img src="<?php bloginfo('template_directory'); ?>/img/title_top.png" alt="交通事故に巻き込まれたら弁護士!!" /></h2>
<h3>交通事故を弁護士に依頼するメリット</h3>
<h4><strong>慰謝料が2倍以上に増額</strong>可能なケースが数多くある</h4>
<p>交通事故の交渉は、多くの場合、加害者との交渉ではなく保険会社との交渉になります。<br>
交渉相手の保険会社は、交通事故被害者の味方ではなく、あくまで加害者の味方です。保険会社としては少しでも慰謝料を減らしたいため、保険会社の言う通りに交渉を進めると、被害者に支払われるべき慰謝料よりも少ない金額になってしまっているケースが多くあります。</p>
<img src="<?php bloginfo('template_directory'); ?>/img/topimg_graph1.gif" alt="解決事例" class="center" />
<h5>保険会社 提示額なしが、依頼後、数千万円に増額されるケースも</h5>
<p>上記をご覧いただくとよく分かりますが、弁護士に依頼することで、依頼前の保険会社が提示した額の約4倍の慰謝料を受け取ることができました。
中には、慰謝料の提示額が0円だったものが、弁護士に依頼後、数千万円になった事例もあります。</p>

<h4><strong>相談料は無料</strong>！不安があるならまずは相談すべき</h4>
<p>弁護士と聞くと、「相談するだけでお金がかかるんじゃないの？」というイメージを持たれている方が非常に多いのが現状です。<br>
しかし、当サイトに掲載している弁護士は”相談料無料”で安心して相談できる弁護士が多くいますし、着手金も無料にされているところも多くあります。</p>

<h5>弁護士費用の相場は、増額分の10％～20%！支払いは成功報酬。</h5>
<p>「解決後に、高額請求されるんじゃないか？」と心配をされている方も、ご安心ください。当サイトでは、問題解決までにかかる費用を明記していますし、不当に高額な法律事務所は掲載しないように心掛けております。</p>
<p>もちろん中には、成功報酬の金額を抑えて、相談料や着手金が必要な法律事務所もあります。当サイトでは解決までに必要な金額を明示しているので、安心して相談することができます。</p>

<h3>弁護士に依頼すると慰謝料が高くなる理由</h3>

<h4>弁護士は<strong>裁判所基準</strong>での慰謝料交渉が可能になるため</h4>

<table>
<tr>
<th>等級</th>
<th>自賠責保険基準の後遺障害慰謝料</th>
<th>裁判所基準の後遺障害慰謝料</th>
</tr>
<tr>
<th>1級</th>
<td>1,100万円</td>
<td><b>2,800万円</b></td>
</tr>
<tr>
<th>2級</th>
<td>958万円</td>
<td><b>2,370万円</b></td>
</tr>
<tr>
<th>3級</th>
<td>829万円</td>
<td><b>1,990万円</b></td>
</tr>
<tr>
<th>4級</th>
<td>712万円</td>
<td><b>1,670万円</b></td>
</tr>
<tr>
<th>5級</th>
<td>599万円</td>
<td><b>1,400万円</b></td>
</tr>
<tr>
<th>6級</th>
<td>498万円</td>
<td><b>1,180万円</b></td>
</tr>
<tr>
<th>7級</th>
<td>409万円</td>
<td><b>1,000万円</b></td>
</tr>
<tr>
<th>8級</th>
<td>324万円</td>
<td><b>830万円</b></td>
</tr>
<tr>
<th>9級</th>
<td>245万円</td>
<td><b>690万円</b></td>
</tr>
<tr>
<th>10級</th>
<td>187万円</td>
<td><b>550万円</b></td>
</tr>
<tr>
<th>11級</th>
<td>135万円</td>
<td><b>420万円</b></td>
</tr>
<tr>
<th>12級</th>
<td>93万円</td>
<td><b>290万円</b></td>
</tr>
<tr>
<th>13級</th>
<td>57万円</td>
<td><b>180万円</b></td>
</tr>
<tr>
<th>14級</th>
<td>32万円</td>
<td><b>110万円</b></td>
</tr>
</table>

<h5>自分で交渉しても慰謝料は"裁判所基準"にするのは非常に困難</h5>
<p>「自分で保険会社と交渉して、裁判所基準で適用してもらえば、一番得じゃないか。」と考えて、交渉しても、根拠をしっかり示す必要があり、一般人にはまずできないでしょう。<br />
弁護士に相談することが、交通事故問題の納得のいく解決の近道です。</p>

<h3>交通事故 発生から解決までの流れ</h3>

<img src="<?php bloginfo('template_directory'); ?>/img/topimg_graph2.gif" alt="解決の流れ" class="alignright" />
<h4>目安は6か月程度</h4>
<p>交通事故に巻き込まれてから、慰謝料が支払われるまでの流れは、右図のようになります。
詳しくは、交通事故問題解決までの流れを参照ください。</p>
<h5>弁護士に相談するのは少しでも早い方がいい！</h5>
<p>示談交渉がこじれてから、弁護士を探すよりも、早ければ事故発生直後に弁護士に相談する方が納得のいく解決にたどり着く可能性が高まります。</p>
<!-- //single_doc--></div>


<?php get_template_part('parts_common'); ?>

<div class="newarrival">
<h4>新着情報</h4>

<ul>
<?php
$args = array(
    'post_type' => array('post','bengo'),
    'order' => 'DESC',
    'posts_per_page' => '6'
);
query_posts( $args );
?>
<?php while ( have_posts() ) : the_post();?>
<li class="wraplink">
<time><?php echo the_time('Y.m.d');?></time><a href="<?php the_permalink();?>"><?php the_title();?></a><?php if (get_post_type() == 'bengo'):?>を掲載しました。<?php endif?>
</li>
<?php endwhile; wp_reset_query();?>
</ul>
</div>

</article>

<!-- //main--></div>



<?php get_footer(); ?>
