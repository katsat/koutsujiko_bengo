<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title><?php if ( get_post_type() === 'bengo' && !is_tax() && !is_archive('bengo') ): ?><?php wp_title('【'.get_post_meta($post->ID , 'shichouson_area', TRUE).'の交通事故に強い弁護士】', true, 'right'); ?>| <?php elseif(is_post_type_archive('bengo')):?>全国対応で交通事故に強い弁護士に相談 | <?php elseif(is_single()): ?><?php wp_title('|', true, 'right'); ?><?php elseif ( get_post_type() === 'bengo' OR is_tax()): ?><?php (single_cat_title("", false))? single_cat_title(): print '【厳選掲載】実績多数';?>で交通事故に強い弁護士に相談｜<?php else: ?><?php wp_title('|', true, 'right'); ?><?php endif; ?><?php bloginfo('name'); ?><?php $site_description = get_bloginfo( 'description', 'display' ); if ( $site_description && ( is_home() || is_front_page() ) ) echo " | $site_description";?>
</title>
<?php wp_head(); ?>
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" type="text/css" media="screen" />
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<meta name="viewport" content="width=device-width, maximum-scale=1" />
<meta name="format-detection" content="telephone=no" />
<?php if ( is_home() ) {
    $canonical_url=get_bloginfo('url')."/";
} 
else if (is_category())
{
    $canonical_url=get_category_link(get_query_var('cat'));
}
elseif(is_tax('bengo_cat'))
{
	$canonical_url = '/bengo/'.get_query_var('bengo_cat');
}
elseif( is_archive('bengo'))
{
	$canonical_url = '/bengo';
}
else if (is_page()||is_single())
{ 
    $canonical_url=get_permalink();
}
if ( $paged >= 2 || $page >= 2)
{
$canonical_url=$canonical_url.'/page/'.max( $paged, $page ); 
}
?>
<link rel="canonical" href="<?php echo $canonical_url; ?>" />
<?php if(is_single()):?><link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"><?php endif;?>
</head>
<body id="top">

<header id="globalheader">
<div class="inner">
<div id="logo"><a href="/"><img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt="<?php bloginfo('name');?>" /></a></div>
<h1><?php if(is_category()):?><?php $term_catch = get_option('term_catch'); $category_tmp = get_the_category(); echo $term_catch[$category_tmp['0']->cat_ID];?>
<?php elseif(is_tax('bengo_cat')):?><?php $term_catch = get_option('term_catch'); $term_catch2 = get_query_var('bengo_cat'); $rank_termid = get_term_by('slug', $term_catch2, 'bengo_cat'); $term_id = $rank_termid->term_id; echo $term_catch[$term_id];?>
<?php elseif(is_post_type_archive('bengo')):?>交通事故に強い（専門の）弁護士に示談交渉や慰謝料問題を相談！
<?php elseif(post_custom('_catch') && !is_home()):?><?php echo post_custom('_catch');?>
<?php elseif(is_home()):?>交通事故に強い弁護士に無料相談！示談交渉や慰謝料増額で損しないためにランキング活用
<?php elseif(is_page('32')):?><?php echo get_post_meta($_GET['post_id'] , '_catch', TRUE);?>
<?php else:?><?php wp_title('', true); ?>の情報<?php endif;?>
</h1>

<h2><?php if(is_category()):?><?php $term_lead = get_option('term_lead'); $category_tmp = get_the_category(); echo $term_lead[$category_tmp['0']->cat_ID];?>
<?php elseif(is_tax('bengo_cat')):?><?php $term_lead = get_option('term_lead'); echo $term_lead[$term_id];?>
<?php elseif(is_post_type_archive('bengo')):?>交通事故問題に強い（専門の）弁護士を掲載しています。交通事故に巻き込まれて、保険会社の慰謝料提示額に納得がいかない！示談交渉で悩んでいるなら弁護士に相談しましょう。
<?php elseif(post_custom('_lead') && !is_home()):?><?php echo post_custom('_lead');?>
<?php elseif(is_home()):?>実際に交通事故に巻き込まれてしまった時の弁護士基準の慰謝料県債方法や示談交渉、弁護士への相談方法、後遺症など、被害者が知っておくべき情報多数。注目ランキングも掲載中。
<?php elseif(is_page('32')):?><?php echo get_post_meta($_GET['post_id'] , '_lead', TRUE);?>
<?php else:?>交通事故弁護士情報から「<?php wp_title('', true); ?>」の情報などを紹介しています。<?php endif;?>
</h2>

<!-- //inner--></div>
<!-- //globalheader--></header>


<nav id="global_nav">
<ul>
<li><a href="/">HOME</a></li>
<li><a href="/resolve">解決の流れ</a></li>
<li><a href="/calculate">慰謝料計算</a></li>
<li><a href="/lawyer-criteria">弁護士基準</a></li>
<li><a href="/lawyer/cost02.html">弁護士費用</a></li>
<li class="sub"><a href="/grade-certification">後遺障害等級</a></li>
<li><a href="/bengo">弁護士を探す</a></li>
</ul>
</nav>

<?php if(!is_home()): ?>
<div id="breadcrumb"><p><?php get_breadcrumbs(); ?></p></div>
<?php endif; ?>

