<?php get_header(); ?>


<div id="wrap">
<div id="main">
<div class="archive_doc">

<h2><?php
      $key = wp_specialchars($s, 1);
      echo '&#8216;'.$key.'&#8217; で検索した結果';
?><?php if(empty($paged)): single_tag_title();?><?php else: single_tag_title();?>[<?php echo $paged;?>ページ目]<?php endif;?></h2>

<nav class="pager">
<?php custum_pagination($wp_query->max_num_pages); ?>
<div class="count"><?php my_result_count();?></div>
</nav>

<article>
<ul class="list">


<?php
global $wp_query;
$args = array_merge( $wp_query->query, array('post__not_in' => get_option( 'sticky_posts' ), 'post_type' => 'post', 'order' => 'ASC') );
query_posts( $args ); $iii = 0;?>
<?php if (have_posts()) : ?>
<?php while ( have_posts() ) : the_post();?>

<li class="wraplink"><?php catch_that_image('150', '150');?><h3><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3><p><?php
$content = apply_filters('the_content',get_the_content());
$content = strip_tags($content);
$content = mb_substr($content, 0, 114);

echo $content; ?>...</p></li>

<?php $iii++; endwhile; wp_reset_query(); unset($iii);?>

<?php else: ?>
<!--  キーワードが見つからないときの処理 --><li><p>お探しの検索ワードに該当する記事が見つかりませんでした。</p></li>
<?php endif; ?>

</ul>
</article>

<nav class="pager">
<?php custum_pagination($wp_query->max_num_pages); ?>
<div class="count"><?php my_result_count();?></div>
</nav>

<!-- //archive_doc--></div>

<!-- //main--></div>





<?php get_footer();