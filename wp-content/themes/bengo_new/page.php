<?php get_header(); ?>

<div id="wrap">
<div id="main">
<div class="single_doc">
<h2><?php the_title(); ?></h2>
<?php if(is_page('20') OR is_page('1639')):?>
<div class="ssl">
<!-- GeoTrust Smart Icon tag. Do not edit. -->
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript" CHARSET="Shift_JIS" SRC="//smarticon.geotrust.com/si.js"></SCRIPT>
<!-- END of GeoTrust Smart Icon tag -->
交通事故弁護士相談広場では、GeoTrustのSSLサーバ証明書を使用して、お客様の個人情報を保護しています。当サイトのすべてのページの情報はSSLで暗号化されてから送受信されます。
</div>
<?php endif;?>
<article>
<?php while ( have_posts() ) : the_post();?>
<?php the_content(); ?>
<?php endwhile; ?>
</article>


<!-- //single_doc--></div>

<?php if(!is_page('20')):?>
<?php get_template_part('parts_common'); ?>
<?php endif;?>


<!-- //main--></div>






<?php get_footer();