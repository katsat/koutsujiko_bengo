<?php get_header(); ?>

<div id="wrap">
<div id="main">
<div class="single_doc">
<h2>ページが見つかりませんでした。</h2>

<article>
<p>お探しのページは移動したか削除されてしまった可能性があります。</p>
<p>お手数ですが<a href="/">トップページ</a>からお探しください。</p>
</article>


<!-- //single_doc--></div>

<?php if(!is_page('20')):?>
<?php get_template_part('parts_common'); ?>
<?php endif;?>


<!-- //main--></div>






<?php get_footer();