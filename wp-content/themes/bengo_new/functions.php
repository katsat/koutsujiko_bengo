<?php

//サムネイルサイズ
add_image_size( 'thumbnail001', 150, 150, true );

function custom_search($search, $wp_query) {
if(!is_admin()){
    //サーチページ以外だったら終了
    if (!$wp_query->is_search) return;
    //投稿記事のみ検索
    $search .= " AND post_type = 'post'";
}
    return $search;
}
add_filter('posts_search','custom_search', 10, 2);


add_action( 'pre_get_posts', 'my_pre_get_posts' );
function my_pre_get_posts( $query ) {
    if ( is_admin() || ! $query -> is_main_query() ) return;
    if ( is_tax('bengo_cat') ) {

    	//$query -> set( 'post__in', $post__in_num );
    	//$query -> set( 'order', 'desc' );
    	$query -> set( 'posts_per_page', '-1' );
		//print_r($query);


    }
}



//カロニカル一旦削除
remove_action('wp_head', 'rel_canonical');

function head_original_load(){
    if(is_page('32')){
    echo '<meta name="robots" content="noindex,nofollow" />';
    }
}
add_action('wp_head', 'head_original_load');

function my_scripts() {
wp_enqueue_script( 'jquery' );

wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/js/custom-script.js', array( 'jquery' ) );
wp_enqueue_script( 'jquery-ui-tabs' );
if(is_single()):
wp_enqueue_script( 'jquery_module', get_template_directory_uri() . '/js/jquery.module.js', array( 'jquery' ), '');
endif;
if(is_page('1170')):
wp_enqueue_script( 'calculate', get_template_directory_uri() . '/js/calculate.js', array( 'jquery' ) );
endif;
if(is_page(array('32','733','735'))):
wp_enqueue_script( 'ajaxzip3-script', 'https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3-https.js', array( 'jquery' ));
endif;
}
add_action( 'wp_enqueue_scripts', 'my_scripts' );


//サムネイル
add_image_size( 'thumbnail-list', 100, 100, true );

//ページング
function custum_pagination($pages = '', $range = 5){
     $showitems = ($range * 2)+1;

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".rtrim(get_pagenum_link(1), '/')."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".rtrim(get_pagenum_link($paged - 1), '/')."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".rtrim(get_pagenum_link($i), '/')."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".rtrim(get_pagenum_link($paged + 1), '/')."'>&rsaquo;</a>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".rtrim(get_pagenum_link($pages), '/')."'>&raquo;</a>";
         echo "</div>\n";
     }
}

//アーカイブページの件数表示など

function my_result_count($wp_query = NULL) {
  if(!$wp_query) global $wp_query;

  $paged = get_query_var( 'paged' ) - 1;
  $ppp   = get_query_var( 'posts_per_page' );
  $count = $total = $wp_query->post_count;
  $from  = 0;
  if ( 0 < $ppp ) {
    $total = $wp_query->found_posts;
    if ( 0 < $paged )
      $from  = $paged * $ppp;
  }
  printf(
    '%2$s - %3$s件を表示<span>(全%1$s件)</span>',
    $total,
    ( 1 < $count ? ($from + 1 . '') : '' ),
    ($from + $count )
  );
}

//都道府県別弁護士の件数表示
function my_result_count_tax($wp_total, $wp_post_count) {

  $paged = get_query_var( 'paged' ) - 1;
  $ppp   = '10';
  $count = $total = $wp_post_count;
  $from  = 0;
  if ( 0 < $ppp ) {
    $total = $wp_total;
    if ( 0 < $paged )
      $from  = $paged * $ppp;
  }
  printf(
    '%2$s - %3$s件を表示<span>(全%1$s件)</span>',
    $total,
    $from + 1 . '',
    ($from + $count )
  );
}



/**
 * my_mail
 * @param object $Mail
 * @param array $data
 */
function my_mail( $Mail, $data ) {
	$nums = preg_replace("/[^0-9]+/", "", $data["_wp_http_referer"]);
	if(get_post_meta($nums, 'contactmail', true)):
    $Mail->to = get_post_meta($nums, 'contactmail', true);
    $Mail->subject = get_the_title($nums).'へのお問い合わせ';
    endif;
    // $Mail->send(); で送信もできます。
    return $Mail;
}
add_filter( 'mwform_admin_mail_mw-wp-form-34', 'my_mail', 10, 2 );




//絶対パスを相対パスに
class relative_URI {
function relative_URI() {
add_action('get_header', array(&$this, 'get_header'), 1);
add_action('wp_footer', array(&$this, 'wp_footer'), 99999);
}
function replace_relative_URI($content) {
$home_url = trailingslashit(get_home_url('/'));
//サブディレクトリに置いている場合は./等と変更する
return str_replace($home_url, '/', $content);
}
function get_header(){
ob_start(array(&$this, 'replace_relative_URI'));
}
function wp_footer(){
ob_end_flush();
}
}
new relative_URI();

add_filter( 'author_rewrite_rules', '__return_empty_array' );
add_filter( 'date_rewrite_rules', '__return_empty_array' );

//wordpress headの不要情報を非表示
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);


//投稿の一番最初の画像を表示
function catch_that_image($www, $hhh) {
	$images = get_children( array(
	  'post_parent' => get_the_ID(),
	  'post_type' => 'attachment',
	  'post_mime_type' => 'image',
	  'orderby' => 'menu_order',
	  'order' => 'ASC',
	  'numberposts' => -1
	) );
	if($images) {
		foreach ( (array) $images as $attachment_id => $attachment ) {
		 if(!$www) $www = '100';
		 if(!$hhh) $hhh = '100';
		  echo wp_get_attachment_image( $attachment_id , array($www ,$hhh) );
		  break;
		}
	} else {
        echo '<img src="'.get_stylesheet_directory_uri().'/img/dummy.gif" width="'.$www.'" height="'.$hhh.'" class="thumb" />';
	}
}

register_post_type(
'bengo',
  array(
  'label' => '弁護士事務所',
  'menu_icon' => 'dashicons-businessman',
  'hierarchical' => false,
  'public' => true,
  'query_var' => false,
  'exclude_from_search' => false,
  'menu_position' => 5,
  'supports' => array('title','editor','comments','revisions'),
  'has_archive' => true
  )
);

// カスタムタクソノミーを定義
  register_taxonomy(
    'bengo_cat',
    'bengo',
    array(
    'label' => '事務所所在地',//なぜかタイトルに出てきてしまうので削除
    'hierarchical' => true,
    //'query_var' => true,
    'rewrite' => array('slug' => 'bengo')
    )
  );
  // カスタムタクソノミーを定義ここまで

// カスタムタクソノミー（並び順タイプの管理）を定義
register_taxonomy(
  'bengo_order_type',
  'bengo',
  array(
    'label' => '事務所表示順区分',
    'hierarchical' => true,
    'rewrite' => array('slug' => 'bengo_order_type')
    )
  );
// カスタムタクソノミー（並び順タイプの管理）を定義ここまで

// 管理画面一覧にカテゴリを表示
function manage_bengo_columns($columns) {
  $columns['bengo_category'] = "弁護事務所所在地";
  return $columns;
}

function add_bengo_column($column_name, $post_id){
  //カテゴリー名取得
  if( 'bengo_category' == $column_name ) {
  $faqcategory = get_the_term_list($post_id, 'bengo_cat');
  }
  if (isset($faqcategory) && $faqcategory) {
  echo $faqcategory;
  } else {
  echo __('None');
  }
  }
  add_filter('manage_edit-bengo_columns', 'manage_bengo_columns');
  add_action('manage_posts_custom_column',  'add_bengo_column', 10, 2);
  // 管理画面一覧にカテゴリを表示ここまで


//電話用URLなどに誘引するための仕掛け
function single_template_asp_redirect($template) {
	if($_GET['pid']){
		global $ag_jump_to_url;
		$ag_jump_to_url = 'tel:'.get_post_meta($_GET['pid'], 'bengo_tel', true);
	}
	$new_template = $template;
	if($ag_jump_to_url) {
	        $new_template = 'asp_redirect.php';
		    $new_template = preg_replace('/[^\/]+\.php$/i', $new_template, $template);
	}
    if (!file_exists($new_template)) {
        $new_template = $template;
    }
    return $new_template;
}
add_filter('single_template', 'single_template_asp_redirect');



//パンくずリスト
function get_breadcrumbs(){
	global $wp_query;
	$list_start = ''/*'<li>'*/;
	$list_end = ''/*'</li>'*/;

	// Start the UL
	//echo '<ul>';
	// Add the Home link
	echo $list_start.'<a href="/">HOME</a>'.$list_end;

	if ( is_category() )
	{
		$catTitle = single_cat_title( "", false );
		$cat = get_cat_ID( $catTitle );
		echo $list_start." &raquo; ". get_category_parents( $cat, TRUE, " &raquo; " ) .$list_end;
	}
	elseif ( is_archive() && !is_category() )
	{
		if(is_tax('bengo_cat')){
			$term_catch2 = get_query_var('bengo_cat');
			$tem_cat_name = get_term_by('slug', $term_catch2, 'bengo_cat');
			if($tem_cat_name){
				echo $list_start.' &raquo; '.'<a href="'.get_option('home'). '/bengo/">弁護士を探す</a>'. $list_end.$list_start.' &raquo; '.$tem_cat_name->name .'で交通事故に強い弁護士'. $list_end;
			} else {
				echo $list_start.' &raquo; 弁護士事務所'. $list_end;
			}
		} else {
			echo $list_start."<span> &raquo; 弁護士を探す</span>".$list_end;
		}
	}
	elseif ( is_404() )
	{
		echo $list_start."<span> &raquo; 404 Not Found</span>".$list_end;
	}
	elseif ( is_single() )
	{
		if(get_post_type() === 'bengo'){
			global $post;
			$parent_ids = $post->post_parent;
			if ($parent_ids) {
				echo $list_start.' &raquo; '.'<a href="'.get_option('home'). '/bengo/' . basename(get_page_link($parent_ids)).'">'.get_the_title($parent_ids).'</a>'. $list_end.$list_start.'<span> &raquo; ' .the_title('','', FALSE) .'</span>'.$list_end;
			} else {
				$category_id = get_the_terms($post->ID, 'bengo_cat');
				$catt = array_shift($category_id);

				echo $list_start.' &raquo; '.'<a href="'.get_option('home'). '/bengo/">弁護士を探す</a>'. $list_end. $list_start.' &raquo; '. '<a href="/bengo/'. $catt->slug .'">'. $catt->name .'で交通事故に強い弁護士</a>'. $list_end.$list_start.'<span> &raquo; ' .the_title('','', FALSE) ."</span>".$list_end;

			}
		} else {
			$category = get_the_category();
			$category_id = get_cat_ID( $category[0]->cat_name );
			echo $list_start.' &raquo; '. get_category_parents( $category_id, TRUE, NULL ). $list_end.$list_start.'<span> &raquo; ' .the_title('','', FALSE) ."</span>".$list_end;
		}

	}
	elseif ( is_page() )
	{
		$post = $wp_query->get_queried_object();

		if ( $post->post_parent == 0 ){

			echo $list_start."<span> &raquo; ".the_title('','', FALSE)."</span>".$list_end;

		} else {
			$title = the_title('','', FALSE);
			$ancestors = array_reverse( get_post_ancestors( $post->ID ) );
			array_push($ancestors, $post->ID);

			foreach ( $ancestors as $ancestor ){
				if( $ancestor != end($ancestors) ){
					echo $list_start.' &raquo; <a href="'. get_permalink($ancestor) .'">'. strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) ) .'</a>'.$list_end;
				} else {
					echo $list_start.'<span> &raquo; '. strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) ) .'</span>'.$list_end;
				}
			}
		}
	}

	// End the UL
	//echo "</ul>";
}



/*
Plugin Name: Top Level Categories
Plugin URI: http://fortes.com/projects/wordpress/top-level-cats/
Description: カテゴリのリンクから/category/を除去
*/

if (function_exists('add_action'))
{
	register_activation_hook(__FILE__, 'top_level_cats_activate');
	register_deactivation_hook(__FILE__, 'top_level_cats_deactivate');

	// Setup filters
	add_filter('category_rewrite_rules', 'top_level_cats_category_rewrite_rules');
	add_filter('generate_rewrite_rules', 'top_level_cats_generate_rewrite_rules');
	add_filter('category_link', 'top_level_cats_category_link', 10, 2);

	global $clean_category_rewrites, $clean_rewrites;
	$clean_category_rewrites = array();
}

function top_level_cats_activate()
{
	global $wp_rewrite;
	$wp_rewrite->flush_rules();
}

function top_level_cats_deactivate()
{
	// Remove the filters so we don't regenerate the wrong rules when we flush
	remove_filter('category_rewrite_rules', 'top_level_cats_category_rewrite_rules');
	remove_filter('generate_rewrite_rules', 'top_level_cats_generate_rewrite_rules');
	remove_filter('category_link', 'top_level_cats_category_link');

	global $wp_rewrite;
	$wp_rewrite->flush_rules();
}

function top_level_cats_generate_rewrite_rules($wp_rewrite)
{
	global $clean_category_rewrites;
	$wp_rewrite->rules = $wp_rewrite->rules + $clean_category_rewrites;
}

function top_level_cats_category_rewrite_rules($category_rewrite)
{
	global $clean_category_rewrites;

  global $wp_rewrite;
  // Make sure to use verbose rules, otherwise we'll clobber our
  // category permalinks with page permalinks
  $wp_rewrite->use_verbose_page_rules = true;

	while (list($k, $v) = each($category_rewrite)) {
		// Strip off the category prefix
		$new_k = top_level_cats_remove_cat_base($k);
		$clean_category_rewrites[$new_k] = $v;
	}

	return $category_rewrite;
}

function top_level_cats_category_link($cat_link, $cat_id)
{
	return top_level_cats_remove_cat_base($cat_link);
}

function top_level_cats_remove_cat_base($link)
{
	$category_base = get_option('category_base');

	// WP uses "category/" as the default
	if ($category_base == '')
		$category_base = 'category';

	// Remove initial slash, if there is one (we remove the trailing slash in the regex replacement and don't want to end up short a slash)
	if (substr($category_base, 0, 1) == '/')
		$category_base = substr($category_base, 1);

	$category_base .= '/';

	return preg_replace('|' . $category_base . '|', '', $link, 1);
}





//DBに保存
function comment_field( $comment_id ) {
    if ( !$comment = get_comment( $comment_id ) )
        return false;
    $custom_key_title  = 'comment-title';
    $get_comment_title = esc_attr( $_POST[$custom_key_title] );
    if ( '' == get_comment_meta( $comment_id, $custom_key_title ) ) {
        add_comment_meta( $comment_id, $custom_key_title, $get_comment_title, true );
    } else if ( $get_comment_title != get_comment_meta( $comment_id, $custom_key_title ) )
{
        update_comment_meta( $comment_id, $custom_key_title, $get_comment_title );
    } else if ( '' == $get_comment_title ) {
        delete_comment_meta( $comment_id, $custom_key_title );
    }
    return false;
}

add_action( 'comment_post', 'comment_field' );
add_action( 'edit_comment', 'comment_field' );


//編集画面に表示
add_action( 'add_meta_boxes_comment', 'comment_field_box' );
function comment_field_box() {
    global $comment;
    $comment_ID = $comment->comment_ID;
    $custom_key         = 'post_reviews_date' ;
    $custom_key_title   = 'comment-title' ;
    $noncename          = $custom_key . '_noncename' ;
    $get_comment_title  = esc_attr( get_comment_meta( $comment_ID, $custom_key_title, true ) );
    echo '<input type="hidden" name="' . $noncename . '" id="' . $noncename . '" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />' . "\n";
    echo '<p><b><label for="comment-title">【コメントタイトル】</label></b></p><p><input id="' . $custom_key_title . '" name="' . $custom_key_title . '" type="text" value="' . $get_comment_title . '" size="56" maxlength="30"/></p>' . "\n";
}


//一覧に表示
function manage_comment_columns($columns) {
    $columns['comment-title'] = "コメントタイトル";
    return $columns;
}

function add_comment_columns($column_name, $comment_id) {
    if( $column_name == 'comment-title' ) {
        $comment_title = get_comment_meta( $comment_id, 'comment-title', true );
        echo attribute_escape($comment_title);
    }
}
add_filter( 'manage_edit-comments_columns', 'manage_comment_columns' );
add_action( 'manage_comments_custom_column', 'add_comment_columns',null, 2);


//土日祝日判別
function japan_date_info($str = NULL) {
	if(!$str) $str = date_i18n('Y-m-d');
	/*if(!preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/ui', $str)) {
	$data[0] = FALSE;	// 日付
	$data[1] = FALSE;	// 曜日
	$data[2] = FALSE;	// 祝日フラグ
	$data[3] = FALSE;	// 休日フラグ
	$data[4] = FALSE;	// 備考
	return $data;
	}*/
	if($data = is_public_horiday($str)) return $data;
	if($data = is_horiday($str)) return $data;
	if(date('D', strtotime($str)) == 'Sat') {
		$data[0] = $str;
		$data[1] = date('D', strtotime($str));
		$data[2] = 0;
		$data[3] = 0;
		$data[4] = '';
		return $data;
	}
	$data[0] = $str;
	$data[1] = date('D', strtotime($str));
	$data[2] = 0;
	$data[3] = 0;
	$data[4] = '';
	//return $data;
	return FALSE;
}

function is_public_horiday($str) {
$data = FALSE;
if(!$str) $str = date_i18n('Y-m-d');
if(!preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/ui', $str)) return $data;
$brake_date = explode('-', $str);
//祝日リスト(日付)
$ph_list[] = $brake_date[0] . '-01-01';
$ph_list[] = $brake_date[0] . '-01-02';
$ph_list[] = $brake_date[0] . '-01-03';
$ph_list[] = date('Y-m-d', strtotime('second mon of ' . $brake_date[0] . '-01'));
$ph_list[] = $brake_date[0] . '-02-11';
$ph_list[] = $brake_date[0] . '-03-' . (floor(20.8431 + 0.242194 * ($brake_date[0] - 1980)) - floor(($brake_date[0] - 1980) / 4));
$ph_list[] = $brake_date[0] . '-04-29';
$ph_list[] = $brake_date[0] . '-05-03';
$ph_list[] = $brake_date[0] . '-05-04';
$ph_list[] = $brake_date[0] . '-05-05';
$ph_list[] = date('Y-m-d', strtotime('third mon of ' . $brake_date[0] . '-07'));
if($brake_date[0] >= 2016) $ph_list[] = $brake_date[0] . '-08-11';
$ph_list[] = date('Y-m-d', strtotime('third mon of ' . $brake_date[0] . '-09'));
$ph_list[] = $brake_date[0] . '-09-' . (floor(23.2488 + 0.242194 * ($brake_date[0] - 1980)) - floor(($brake_date[0] - 1980) / 4));
$ph_list[] = date('Y-m-d', strtotime('second mon of ' . $brake_date[0] . '-10'));
$ph_list[] = $brake_date[0] . '-11-03';
$ph_list[] = $brake_date[0] . '-11-23';
$ph_list[] = $brake_date[0] . '-12-23';
$ph_list[] = $brake_date[0] . '-12-29';
$ph_list[] = $brake_date[0] . '-12-30';
$ph_list[] = $brake_date[0] . '-12-31';
//祝日リスト(名前)
$ph_name[] = '元日';
$ph_name[] = '年末年始休業';
$ph_name[] = '年末年始休業';
$ph_name[] = '成人の日';
$ph_name[] = '建国記念の日';
$ph_name[] = '春分の日';
$ph_name[] = '昭和の日';
$ph_name[] = '憲法記念日';
$ph_name[] = 'みどりの日';
$ph_name[] = 'こどもの日';
$ph_name[] = '海の日';
if($brake_date[0] >= 2016) $ph_name[] = '山の日';
$ph_name[] = '敬老の日';
$ph_name[] = '秋分の日';
$ph_name[] = '体育の日';
$ph_name[] = '文化の日';
$ph_name[] = '勤労感謝の日';
$ph_name[] = '天皇誕生日';
$ph_name[] = '年末年始休業';
$ph_name[] = '年末年始休業';
$ph_name[] = '年末年始休業';
//日付評価
foreach($ph_list as $key => $value) {
if($str == $value) {
$data[0] = $str;
$data[1] = date('D', strtotime($str));
$data[2] = 1;
$data[3] = 1;
$data[4] = $ph_name[$key];
break;
}
}
return $data;
}

function is_horiday($str) {
$data = FALSE;
if(!$str) $str = date_i18n('Y-m-d');
if(!preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/ui', $str)) return $data;
$brake_date = explode('-', $str);
//日曜評価
if(date('D', strtotime($str)) == 'Sun') {
$data[0] = $str;
$data[1] = date('D', strtotime($str));
$data[2] = 0;
$data[3] = 1;
$data[4] = '';
return $data;
}
//振替休日
if(is_public_horiday(date('Y-m-d', strtotime('yesterday ' . $str))) && is_horiday(date('Y-m-d', strtotime('yesterday ' . $str)))) {
$data[0] = $str;
$data[1] = date('D', strtotime($str));
$data[2] = 0;
$data[3] = 1;
$data[4] = '振替休日';
return $data;
}
//国民の休日
if(is_public_horiday(date('Y-m-d', strtotime('yesterday ' . $str))) && is_public_horiday(date('Y-m-d', strtotime('tomorrow ' . $str)))) {
$data[0] = $str;
$data[1] = date('D', strtotime($str));
$data[2] = 0;
$data[3] = 1;
$data[4] = '国民の休日';
return $data;
}
return $data;
}
