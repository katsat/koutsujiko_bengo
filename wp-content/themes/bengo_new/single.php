<?php get_header(); ?>

<div id="wrap">
<div id="main">
<div class="single_doc">
<h2><?php the_title(); ?></h2>

<article>
<?php while ( have_posts() ) : the_post();?>
<?php the_content(); ?>
<?php endwhile; ?>
</div>

<?php get_template_part('parts_common'); ?>

<div class="sharebtn">

<ul>
<li><a href="http://www.facebook.com/sharer.php?u=<?php bloginfo('url');?><?php the_permalink();?>&t=<?php echo the_title(); ?>" class="fa" id="facebook" title="facebook" rel="nofollow" target="_blank">&#xf230;</a></li>
<li><a href="http://twitter.com/share?url=<?php bloginfo('url');?><?php the_permalink();?>&amp;text=<?php echo the_title(); ?>" class="fa" id="twitter" title="twitter" rel="nofollow" target="_blank">&#xf099;</a></li>
<li><a href="https://plus.google.com/share?url=<?php bloginfo('url');?><?php the_permalink();?>" class="fa" id="googleplus" title="googleplus" rel="nofollow" target="_blank">&#xf1a0;</a></li>
<li class="pocket"><a href="http://getpocket.com/edit?url=<?php bloginfo('url');?><?php the_permalink();?>&title=<?php echo the_title(); ?>" class="fa fa-chevron-down" id="pocket" title="pocket" rel="nofollow" target="_blank">&#xf078;</a></li>
<?php /* ◆◆◆◆◆◆<li class="feedly"><a href='http://cloud.feedly.com/#subscription%2Ffeed%2F<?php bloginfo('url');?>?xml' rel="nofollow" class="fa" title="feedly" id="feedly" target="_blank">&#xf0c8;</a></li>
<li><a href="<?php bloginfo('url');?>?xml" class="fa" id="rss" title="rss" target="_blank">&#xf09e;</a></li>◆◆◆◆◆ */ ?>
<li class="line"><a href="http://line.naver.jp/R/msg/text/?<?php echo the_title(); ?>%0D%0A<?php bloginfo('url');?><?php the_permalink();?>" id="line" title="line" rel="nofollow" target="_blank">LINE</a></li>
<li class="hatena"><a href="http://b.hatena.ne.jp/add?mode=confirm&url=<?php bloginfo('url');?><?php the_permalink();?>&title=<?php echo the_title(); ?>" id="hatena" title="hatenabookmark" rel="nofollow" target="_blank">B</a></li>
</ul>
</div>

<?php $category_info = get_the_category(); ?>
<div class="linkage">
<h4>関連コンテンツ</h4>
<?php
foreach((get_the_category()) as $cat) {
$cat_id = $cat->cat_ID ;
$cat_name = $cat->cat_name;
$cat_slug = $cat->slug;
break ;
}
$query = 'cat=' . $cat_id;
?>

<ul>
<?php previous_post_links('%link', '%title', TRUE, '', 5, 'ASC'); ?>
<?php next_post_links('%link', '%title', TRUE, '', 5, 'ASC'); ?>
</ul>
<?php /* ◆◆◆◆◆<p><a href="/<?php echo $category_info[0]->slug;?>"><?php echo $category_info[0]->name;?> 一覧（全<?php echo $category_info[0]->count;?>件）</a></p>◆◆◆◆◆◆ */ ?>
</div>

<!-- //main--></div>






<?php get_footer();