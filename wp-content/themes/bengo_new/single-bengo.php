<?php get_header(); ?>

<div class="front bengosingle">
<div class="inner">
<header>
<p><span><?php echo get_post_meta($post->ID , 'bengo_area', TRUE);?></span><strong><?php echo get_post_meta($post->ID , 'bengo_catch', TRUE);?></strong></p>
<h1><?php the_title(); ?></h1>
</header>
<article>
<div class="bengo_image">
<table>
<tr>
<td><em>相談料</em><?php echo get_post_meta($post->ID , 'bengo_price', TRUE);?></td>
<td><em>着手金</em><?php echo nl2br(get_post_meta($post->ID , 'bengo_start', TRUE));?></td>
<td><em>報酬金</em><?php echo nl2br(get_post_meta($post->ID , 'bengo_success', TRUE));?></td>
</tr>
</table>
<?php echo wp_get_attachment_image(post_custom('File Upload'), array('596', '396'), false, array('class' => 'visual', 'alt' => get_the_title()));?>
<?php if(get_post_meta($post->ID , 'thumbnail', TRUE)):?>
<?php if(is_array(post_custom('thumbnail'))):?>
<?php $iii = 0; foreach(post_custom('thumbnail') as $tmp_images):
$image_path = wp_get_attachment_image_src($tmp_images, 'medium');
echo wp_get_attachment_image($tmp_images, array('200', '136'), false, array('class' => 'thumb tmb'.$iii, 'alt' => get_the_title().'サムネイル'.$iii));
$iii++; endforeach;?>
<?php else:?>
<?php echo wp_get_attachment_image(get_post_meta($post->ID , 'thumbnail', TRUE), array('200', '136'), false, array('class' => 'thumb tmb'.$iii, 'alt' => get_the_title().'サムネイル'));?>
<?php endif;?>
<?php endif;?>

</div>

<div class="bengo_detail">
<h2>取扱い可能な事案</h2>
<?php $tmp_jian = get_post_meta($post->ID , 'bengo_jian', FALSE);?>
<?php $tmp_status = get_post_meta($post->ID , 'bengo_status', FALSE);?>
<ul>
<li<?php if(!in_array('jian_isya' ,$tmp_jian)) echo ' class="n"';?>>慰謝料</li>
<li<?php if(!in_array('jian_songai' ,$tmp_jian)) echo ' class="n"';?>>損害賠償</li>
<li<?php if(!in_array('jian_jidan' ,$tmp_jian)) echo ' class="n"';?>>示談交渉</li>
<li<?php if(!in_array('jian_kashitsu' ,$tmp_jian)) echo ' class="n"';?>>過失割合</li>
<li<?php if(!in_array('jian_busson' ,$tmp_jian)) echo ' class="n"';?>>物損事故</li>
<li<?php if(!in_array('jian_jinshin' ,$tmp_jian)) echo ' class="n"';?>>人身事故</li>
<li<?php if(!in_array('jian_shibo' ,$tmp_jian)) echo ' class="n"';?>>死亡事故</li>
<li<?php if(!in_array('jian_kouisyo' ,$tmp_jian)) echo ' class="n"';?>>後遺障害</li>
<li<?php (in_array('area' ,$tmp_status))? print ' class="r"' : print ' class="n"';?>>全国対応</li>
<li<?php (in_array('pps' ,$tmp_status))? print ' class="r"' : print ' class="n"';?>>後払い可</li>
<li<?php (in_array('jian_24h' ,$tmp_jian))? print ' class="r"' : print ' class="n"'; ?>>24H電話</li>
<li<?php (in_array('jian_donichi' ,$tmp_jian))? print ' class="r"' : print ' class="n"'; ?>>土日対応</li>
</ul>

<h2>料金体系</h2>
<p><?php echo nl2br(get_post_meta($post->ID , 'bengo_ryokin', TRUE));?></p>
<?php  /* ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★ ?>
<h2>対応エリア</h2>
<p><?php echo nl2br(get_post_meta($post->ID , 'bengo_area', TRUE));?>（所在地：<?php echo nl2br(get_post_meta($post->ID , 'shozai_area', TRUE));?>）
</p>
<?php ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★　*/ ?>
<?php if(in_array('tokuyaku' ,$tmp_status)){ ?>
<a href="#tokuyaku"><img src="<?php bloginfo('template_directory'); ?>/img/btn_bengotokuyaku.png" alt="弁護士費用特約利用可能" /></a>
<p>※ご加入の自動車保険をご確認ください</p>
<?php }; ?>

<?php $kaiketsu_status = get_post_meta($post->ID , 'kaiketsu_status', FALSE);?>

<?php if(in_array('jisseki1' ,$kaiketsu_status)){ ?>
<a href="#solve"><img src="<?php bloginfo('template_directory'); ?>/img/btn_jisseki.png" alt="解決実績を見る" /></a>
<?php }; ?>
</div>



</article>


</div>

<div class="contactfield">
<div class="inner">
<h2>「<?php the_title(); ?>」ご相談窓口</h2>
<div class="tel">
<p><b>
<?php $jdi = japan_date_info();
if(get_post_meta($post->ID , 'bengo_open_option', TRUE) == 3) {
	$jdi_flag = TRUE;
} elseif(get_post_meta($post->ID , 'bengo_open_option', TRUE) == 2) {
	if(empty($jdi['2'])) $jdi_flag = TRUE;
	if($jdi['2'] == Sat || $jdi['2'] == Sun) $jdi_flag = TRUE;
} elseif(get_post_meta($post->ID , 'bengo_open_option', TRUE) == 1) {
	if(empty($jdi['2'])) $jdi_flag = TRUE;
	if($jdi['2'] == Sat) $jdi_flag = TRUE;
} elseif(get_post_meta($post->ID , 'bengo_open_option', TRUE) == 4) {
	if(empty($jdi['2'])) $jdi_flag = TRUE;
	if($jdi['2'] == Sun) $jdi_flag = TRUE;
}
if(empty($jdi['2'])) $jdi_flag = TRUE;//平日なら全部OK
?>
<?php if( $jdi_flag && (date_i18n("Hi") >= get_post_meta($post->ID , 'bengo_open_start', TRUE) and date_i18n("Hi") <= get_post_meta($post->ID , 'bengo_open_end', TRUE))):?>
現在<?php echo date_i18n("H:i");?>です。お気軽にお電話ください。
<?php else:?>
現在<?php echo date_i18n("H:i");?>。電話受付時間外です。無料相談フォームよりご連絡ください。
<?php endif;?></b></p>
<div class="helpline">
<strong>お電話での相談窓口【通話無料】</strong><a href="<?php the_permalink(); ?>?pid=<?php echo $post->ID?>" rel="nofollow"><?php echo get_post_meta($post->ID , 'bengo_tel', TRUE);?></a>
<table>
<tr>
<th>受付時間</th>
<td><?php echo get_post_meta($post->ID , 'bengo_open', TRUE);?></td>
</tr>
</table>
</div>
<p>※スマホなら電話番号をタッチして無料相談。</p>
</div>

<div class="net">

<?php if(get_post_meta($post->ID , 'mail_flag', FALSE)):?>
<?php if(in_array('tokuyaku' ,$tmp_status)){ ?>
<h3 class="int">弁護士費用特約利用可能</h3><p>自己負担なしで弁護士への依頼が可能です。</p>
<a href="#tokuyaku"><img src="<?php bloginfo('template_directory'); ?>/img/btn_bengotokuyaku.png" alt="弁護士費用特約利用可能" /></a>
<p>※ご加入の自動車保険をご確認ください</p>
<?php } ?>
<?php else: ?>
<h3>インターネットから直接相談する</h3><p>【24時間365日受付】無料でフォームから相談する</p><a href="/bengo_contact?post_id=<?php the_ID();?>" rel="nofollow"><img src="<?php bloginfo('template_directory'); ?>/img/btn_freeform.png" alt="無料相談フォームはこちら" /></a>
<?php endif; ?>

<?php  /* ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★ ?>
<div class="ssl">
<!-- GeoTrust Smart Icon tag. Do not edit. -->
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript" CHARSET="Shift_JIS" SRC="//smarticon.geotrust.com/si.js"></SCRIPT>
<!-- END of GeoTrust Smart Icon tag -->
GeoTrustのSSLサーバ証明書を使用して、利用者の個人情報を保護しています。
</div>
<?php ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★　*/ ?>
</div>
</div>
<!-- //contactfield--></div>


<!-- //front--></div>


<div id="wrap">
<div id="main">
<div class="single_doc">

<article>

<?php if(in_array('jisseki1' ,$kaiketsu_status)){ ?>
<article class="solve" id="solve">
<?php if(get_post_meta($post->ID , 'bengo_performance1', TRUE) OR get_post_meta($post->ID , 'bengo_performance2', TRUE) OR get_post_meta($post->ID , 'bengo_performance3', TRUE)):?>
<h3>解決実績</h3>
<?php if(get_post_meta($post->ID , 'bengo_performance1', TRUE)):?>
<div class="p1"><?php echo get_post_meta($post->ID , 'bengo_performance1', TRUE);?></div>
<?php endif;?>

<?php if(in_array('jisseki2' ,$kaiketsu_status)){ ?>
<?php if(get_post_meta($post->ID , 'bengo_performance2', TRUE)):?>
<div class="p2"><?php echo get_post_meta($post->ID , 'bengo_performance2', TRUE);?></div>
<?php endif;?>
<?php }; ?>

<?php if(in_array('jisseki3' ,$kaiketsu_status)){ ?>
<?php if(get_post_meta($post->ID , 'bengo_performance3', TRUE)):?>
<div class="p3"><?php echo get_post_meta($post->ID , 'bengo_performance3', TRUE);?></div>
<?php endif;?>
<?php }; ?>

<?php if(get_post_meta($post->ID , 'bengo_performance2', TRUE) OR get_post_meta($post->ID , 'bengo_performance3', TRUE)):?>
<div class="btn"><img src="<?php bloginfo('template_directory'); ?>/img/btn_solve.png" alt="その他の解決実績を見る" /></div>
<?php endif;?>
<?php endif;?>
</article>
<?php }; ?>


<?php while ( have_posts() ) : the_post();?>
<?php remove_filter ('the_content', 'wpautop'); ?>
<?php the_content();?>
<?php endwhile;?>
</article>


<article class="detail" id="dtl">
<h3>アクセス</h3>
<p><?php echo nl2br(get_post_meta($post->ID , 'bengo_access', TRUE));?></p>
<div class="map">
<?php /* ◆◆◆◆◆心法律事務所用個別対応◆◆◆◆◆◆ */ ?>
<?php if(is_single('2261')):?>
<?php echo do_shortcode('[map addr="〒500-8844 岐阜県岐阜市吉野町４丁目１１ ＫＪビル4F"]'); ?>
<?php elseif(is_single('2247')):?>
<?php echo do_shortcode('[map addr="〒453-0015 名古屋市中村区椿町14-13 ウエストポイント7F"]'); ?>
<?php /* ◆◆◆◆◆弁護士法人あさかぜ法律事務所用個別対応◆◆◆◆◆◆ */ ?>
<?php elseif(is_single('4590')):?>
<?php echo do_shortcode('[map addr="〒730-0017 広島県広島市中区鉄砲町1 佐々木ビル"]'); ?>
<?php /* ◆◆◆◆◆藤沢かわせみ法律事務所◆◆◆◆◆◆ */ ?>
<?php elseif(is_single('4544')):?>
<?php echo do_shortcode('[map addr="神奈川県藤沢市藤沢４３ あいおいニッセイ同和損保藤沢ビル"]'); ?>
<?php /* ◆◆◆◆◆湖北法律事務所◆◆◆◆◆◆ */ ?>
<?php elseif(is_single('8142')):?>
<?php echo do_shortcode('[map addr="滋賀県長浜市高田町9-17 20MORINOビル２F"]'); ?>
<?php /* ◆◆◆◆◆サリュ全国◆◆◆◆◆◆ */ ?>
<?php elseif(is_single('7629')):?>
<?php else:?>
<?php echo do_shortcode('[map addr="'.get_post_meta($post->ID , 'bengo_address', TRUE).'"]'); ?>
<?php endif;?>
</div>
<?php if(!is_single('2891') AND !is_single('7629') AND !is_single('9310')):?><p><?php echo get_post_meta($post->ID , 'bengo_address', TRUE);?></p><?php endif;?>
<h3>事務所概要</h3>
<table id="dtl">
<tr>
<th>事務所名</th>
<td><?php if(get_post_meta($post->ID , 'bengo_officialname', TRUE)):?><?php echo get_post_meta($post->ID , 'bengo_officialname', TRUE);?><?php else:?><?php the_title(); ?><?php endif;?></td>
</tr>
<tr>
<th>代表者</th>
<td><?php echo get_post_meta($post->ID , 'bengo_daihyou', TRUE);?></td>
</tr>
<tr>
<th>住所</th>
<td><?php echo get_post_meta($post->ID , 'bengo_address', TRUE);?></td>
</tr>
<tr>
<th>電話番号</th>
<td><a href="<?php the_permalink(); ?>?pid=<?php echo $post->ID?>"><?php echo get_post_meta($post->ID , 'bengo_tel', TRUE);?></a></td>
</tr>
<tr>
<th>受付時間</th>
<td><?php echo get_post_meta($post->ID , 'bengo_open', TRUE);?></td>
</tr>
<tr>
<th>定休日</th>
<td><?php echo get_post_meta($post->ID , 'bengo_close', TRUE);?></td>
</tr>
<tr>
<th>備考</th>
<td><?php echo get_post_meta($post->ID , 'bengo_bikou', TRUE);?></td>
</tr>
</table>


<div class="sharebtn">

<ul>
<li><a href="http://www.facebook.com/sharer.php?u=<?php bloginfo('url');?><?php the_permalink();?>&t=<?php echo the_title(); ?>" class="fa" id="facebook" title="facebook" rel="nofollow" target="_blank">&#xf230;</a></li>
<li><a href="http://twitter.com/share?url=<?php bloginfo('url');?><?php the_permalink();?>&amp;text=<?php echo the_title(); ?>" class="fa" id="twitter" title="twitter" rel="nofollow" target="_blank">&#xf099;</a></li>
<li><a href="https://plus.google.com/share?url=<?php bloginfo('url');?><?php the_permalink();?>" class="fa" id="googleplus" title="googleplus" rel="nofollow" target="_blank">&#xf1a0;</a></li>
<li class="pocket"><a href="http://getpocket.com/edit?url=<?php bloginfo('url');?><?php the_permalink();?>&title=<?php echo the_title(); ?>" class="fa fa-chevron-down" id="pocket" title="pocket" rel="nofollow" target="_blank">&#xf078;</a></li>
<?php /* ◆◆◆◆◆◆<li class="feedly"><a href='http://cloud.feedly.com/#subscription%2Ffeed%2F<?php bloginfo('url');?>?xml' rel="nofollow" class="fa" title="feedly" id="feedly" target="_blank">&#xf0c8;</a></li>
<li><a href="<?php bloginfo('url');?>?xml" class="fa" id="rss" title="rss" target="_blank">&#xf09e;</a></li>◆◆◆◆◆ */ ?>
<li class="line"><a href="http://line.naver.jp/R/msg/text/?<?php echo the_title(); ?>%0D%0A<?php bloginfo('url');?><?php the_permalink();?>" id="line" title="line" rel="nofollow" target="_blank">LINE</a></li>
<li class="hatena"><a href="http://b.hatena.ne.jp/add?mode=confirm&url=<?php bloginfo('url');?><?php the_permalink();?>&title=<?php echo the_title(); ?>" id="hatena" title="hatenabookmark" rel="nofollow" target="_blank">B</a></li>
</ul>
</div>



<?php /* ◆◆◆◆◆　SQL　◆◆◆◆◆◆ */ ?>
<?php
$term = array_shift(get_the_terms($post->ID, 'bengo_cat'));

if(in_array($term->term_id, array('11', '15', '17', '18', '22', '30', '31')))://関東
$tmp_area = '11, 15, 17, 18, 22, 30, 31';
elseif(in_array($term->term_id, array('12', '19', '24', '34', '38', '43', '54')))://関西
$tmp_area = '12, 19, 24, 34, 38, 43, 54';
elseif(in_array($term->term_id, array('16', '21', '29')))://東海
$tmp_area = '16, 21, 29';
elseif(in_array($term->term_id, array('25', '28', '55', '48', '51', '57')))://北陸甲信越
$tmp_area = '25, 28, 55, 48, 51, 57';
elseif(in_array($term->term_id, array('20', '35', '36', '40', '42', '47', '50', '56')))://九州沖縄
$tmp_area = '20, 35, 36, 40, 42, 47, 50, 56';
elseif(in_array($term->term_id, array('27', '32', '44', '45', '49', '52')))://東北
$tmp_area = '27, 32, 44, 45, 49, 52';
elseif(in_array($term->term_id, array('23', '33', '37', '41', '53', '58', '59', '60', '61')))://中国四国
$tmp_area = '23, 33, 37, 41, 53, 58, 59, 60, 61';
else:
$tmp_area = $term->term_id;
endif;
?>

<?php
$ret =$wpdb->get_results($wpdb->prepare("
SELECT * FROM $wpdb->posts
INNER JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
INNER JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
WHERE $wpdb->term_taxonomy.term_taxonomy_id IN ($tmp_area)
AND $wpdb->term_taxonomy.taxonomy = 'bengo_cat'
AND $wpdb->posts.post_status = 'publish'
AND $wpdb->posts.post_date > '2012-12-31'
" , $xxx));?>

<?php /* ◆◆◆◆◆　前後の4件を取得　◆◆◆◆◆◆ */ ?>
<?php 
foreach($ret as $rr):
$tmp_postin[] = $rr->ID;
endforeach;

$tmp_postkeys = array_search($post->ID, $tmp_postin);
$tmp_count = count($tmp_postin);

if($tmp_postkeys == 1) {
$tmp_postin_pre = array($tmp_postin[0]);
$tmp_postin_nex = array_slice($tmp_postin, 2, 3);
$data_postin = array_merge_recursive($tmp_postin_pre, $tmp_postin_nex);

} elseif($tmp_postkeys == 2) {
$tmp_postin_pre = array($tmp_postin[0], $tmp_postin[1]);
$tmp_postin_nex = array_slice($tmp_postin, 3, 2);
$data_postin = array_merge_recursive($tmp_postin_pre, $tmp_postin_nex);

} elseif($tmp_postkeys == ($tmp_count-1)) {
$data_postin = array_slice($tmp_postin, -5, 4);

} elseif($tmp_postkeys == ($tmp_count-2)) {
$tmp_postin_pre = array($tmp_postin[($tmp_count-1)]);
$tmp_postin_nex = array_slice($tmp_postin, -6, 3);
$data_postin = array_merge_recursive($tmp_postin_pre, $tmp_postin_nex);

} elseif($tmp_postkeys >= 3) {
$tmp_postin_pre = array_slice($tmp_postin, ($tmp_postkeys-2), 2);
$tmp_postin_nex = array_slice($tmp_postin, ($tmp_postkeys+1), 2);
$data_postin = array_merge_recursive($tmp_postin_pre, $tmp_postin_nex);
} else {
$data_postin = array_slice($tmp_postin, 1, 4);
}


$args = array(
'post_type' => 'bengo',
'orderby' => 'ID',
'order' => 'ASC',
'post__in' => $data_postin
);
?>

<?php /* ◆◆◆◆◆　ここで表示　◆◆◆◆◆◆ */ ?>

<?php $myposts = get_posts($args); if($myposts) : ?>
<aside class="relate-jimusho">
<h3>その他の交通事故に強い弁護士事務所</h3>
<ul>
<?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
<li><a href="<?php the_permalink(); ?>"><?php echo wp_get_attachment_image(post_custom('File Upload'), array('200', '136'), false, array('alt' => get_the_title().'サムネイル'));?><?php the_title(); ?></a>
<p><?php echo get_post_meta($post->ID , 'bengo_address', TRUE);?></p>
</li>
<?php endforeach; ?>
</ul>
</aside>
<?php endif; wp_reset_postdata(); ?>









</article>

<!-- //single_doc--></div>
<!-- //main--></div>




<?php
wp_reset_query();
?>

<script type="text/javascript">
//<![CDATA[
(function($){
	var m_visual = $('#single_bengo .image img.visual').attr('src');
	$('#single_bengo .image img.thumb').mouseover(function(){
		var tm_pass = $(this).attr('alt');
		$('#single_bengo .image img.visual').attr('src', tm_pass);
	});
	$('#single_bengo .image img.thumb').mouseout(function(){
		$('#single_bengo .image img.visual').attr('src', m_visual);
	});
	//console.log($area);
})(jQuery);
//]]>
</script>


<?php get_footer();