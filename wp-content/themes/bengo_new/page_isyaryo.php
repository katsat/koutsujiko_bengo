<?php
/*
Template Name: ISHYARYO
*/

get_header(); ?>


<div class="front bengoarchive compensation">
<div class="inner">
<header>
<span>【完全保存版】弁護士に依頼すると「慰謝料」はどう変わる？</span>
<h1>交通事故の慰謝料（賠償金）増額・解決事例</h1>
</header>

<div id="solvecase">
<dl>
	<dt>後遺障害等級認定・死亡事故</dt>
	<dd><a href="#case00">死亡事故の増額事例</a></dd>
    <dd><a href="#case01">後遺障害1級の増額事例</a></dd>
    <dd><a href="#case07">後遺障害7級の増額事例</a></dd>
    <dd><a href="#case08">後遺障害8級の増額事例</a></dd>
    <dd><a href="#case09">後遺障害9級の増額事例</a></dd>
    <dd><a href="#case10">後遺障害10級の増額事例</a></dd>
    <dd><a href="#case11">後遺障害11級の増額事例</a></dd>
    <dd><a href="#case12">後遺障害12級の増額事例</a></dd>
    <dd><a href="#case14">後遺障害14級の増額事例</a></dd>
</dl>
<dl>
	<dt>その他</dt>
	<dd><a href="#busson">物損事故の解決事例</a></dd>
    <dd><a href="#keiji">刑事事件の解決事例</a></dd>
    <dd><a href="#kashitsu">過失割合の解決事例</a></dd>
</dl>
</div>

<div class="keisanhe"><a href="/calculate" >交通事故「慰謝料 自動計算シュミレーション」</a></div>

<article>
<ul class="list">

<li id="case00"><?php //▲▲▲▲▲▲▲事例ここから  ?>
<h2>「死亡事故」の慰謝料増額・解決事例</h2>
<p>死亡事故の裁判所基準の慰謝料額は2800万円（一家の支柱が亡くなられた場合の目安）。<br>
死亡事故の場合、本人が請求を行うことができませんので、遺族が慰謝料請求を行います。保険会社の提示額が、納得のいく慰謝料額に至っていない場合、弁護士に相談しましょう。</p>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>鹿児島ファースト法律事務所へのご依頼で、<b>約3400万円</b>の賠償額に。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/kagoshimaf01.jpg" alt="鹿児島ファースト法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/kagoshima/kagoshima-first.html">鹿児島ファースト法律事務所</a>
<ul>
	<li><span>相談料</span><b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>鹿児島</li>
	<li class="clear all"><span>住所</span>〒892-0815 鹿児島県鹿児島市易居町2-10-2F</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼後</sup>約<b>3400</b>万円（＋遅延損害金約680万円）</td>
</tr>
</table>

<table>

<tr>
<th>事故状況</th>
<td>自転車対車</td>
</tr>
<tr>
<th>事案</th>
<td>被害者が自転車で道路を渡っている途中，直進してきた加害車両と衝突。高次脳機能障害の後遺症が残存し，寝たきり状態になった後，約２年後に死亡。 </td>
</tr>
<tr>
<th>裁判期間</th>
<td>2年</td>
</tr>

<tr>
<th>ご依頼後</th>
<td>①過失割合<br>
事故状況を「横断」中の事故と評価して過失相殺は20%との原告側の主張に対し，被告側は「右折」中の事故と評価すべきとして40%の過失相殺を主張。原告側は実況見分調書をもとに走行車線上の被害者の滞留時間を算定する等して，主張・立証したが，裁判所はいずれであったかを明示することなく，30%の過失相殺を認定した。<br>
②死亡慰謝料　2,800万円<br>
被害者の看護にあたった妻の陳述書や事故後の支出増（家庭菜園の手入れをするものがいなくなり，自給自足の生活ができなくなった等）の証拠資料を提出することにより，高齢者事案としては，比較的高額な2,800万円の慰謝料を認定してもらうことができた。</td>
</tr>
</table>
<a href="/bengo/kagoshima/kagoshima-first.html" class="btn">鹿児島ファースト法律事務所</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>
</li>

<li id="case01"><?php //▲▲▲▲▲▲▲事例ここから  ?>
<h2>「後遺障害1級」の慰謝料増額・解決事例</h2>
<p>後遺障害1級の裁判所基準の慰謝料額は2800万円。自賠責基準の慰謝料が1100万円となっています。<br>
差額が1700万円にもなります。保険会社の提示額が、納得のいく慰謝料額に至っていない場合、弁護士に相談しましょう。</p>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>のぞみ総合法律事務所へのご依頼で、<b>総額1億1855万円</b>の賠償額に。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/nozomi01.jpg" alt="のぞみ総合法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/kagawa/nozomi-law.html">のぞみ総合法律事務所</a>
<ul>
	<li><span>相談料</span>初回<b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>四国</li>
	<li class="clear all"><span>住所</span>〒760-0026 高松市磨屋町6番地5 のぞみビル2階～6階</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼後</sup>総額<b>1億1855</b>万円</td>
</tr>
</table>

<table>

<tr>
<th>後遺障害等級</th>
<td>1級</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>今回の裁判において問題となったのは、「過失割合」、「常時介護」か「随時介護」か、「自宅改造の必要性」、「将来介護費」などでした。保険会社側は、随時介護・自宅改造費は全て認められないと主張しており、最大の争点の過失割合も5:5を主張していました。<br>最終的な判決では、過失割合について、被害者側(ご依頼者)の過失が2、加害者の過失が8と認定されました。結果、自賠責保険3743万円とは別に約8112万円を獲得しました。（総額1億1855万円） <br>さらに、この事案においては、保険会社が搭乗者傷害保険の支払いをしていなかったことを、我々が指摘し、上記の指定特供金とは別に500万円の保険金を受領しました。保険会社の支払いにおいては、こうした保険金の受領漏れもありますので、専門家への相談は大切です。</td>
</tr>
</table>
<a href="/bengo/kagawa/nozomi-law.html" class="btn">のぞみ総合法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>井上剛法律事務所へのご依頼で、<b>約2億円</b>の賠償額。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/inoue.jpg" alt="井上剛法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/aichi/inoue-law.html">井上剛法律事務所</a>
<ul>
	<li><span>相談料</span>初回<b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>愛知県</li>
	<li class="clear all"><span>住所</span>〒448-8525　愛知県刈谷市相生町3-3 富士ビルディング6階</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼後</sup>民事訴訟における和解で<b>約2億円</b></td>
</tr>
</table>

<table>
<tr>
<th>ご依頼者</th>
<td>男・20代</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>1級1号相当</td>
</tr>
<tr>
<th>事故の状況</th>
<td>信号機により交通整理の行われていない二輪車と四輪車の出合い頭衝突事故</td>
</tr>
<tr>
<th>ご依頼内容</th>
<td>後遺障害等級1級1号と認定された被害者につき、適正賠償を得たい。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>最大争点は、将来介護費。裁判所は、被害者の母親が67歳に達するまでについても近親者介護と一部職業介護の併用を認め、介護費日額1万2500円を、母親が67歳に達した以降は職業介護として介護費日額2万円を、各提示(介護費全体は約1億1000万円との提示)し、和解成立。なお、被告(保険会社)は、近親者介護日額6000円程度が相当と主張していた。</td>
</tr>
</table>
<a href="/bengo/aichi/inoue-law.html" class="btn">井上剛法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>



<a href="#solvecase">目次へ戻る</a>
</li><?php //▲▲▲▲▲▲▲事例ここまで  ?>


<li id="case07"><?php //▲▲▲▲▲▲▲事例ここから  ?>
<h2>「後遺障害7級」の慰謝料増額・解決事例</h2>
<p>自賠責の後遺障害等級認定結果に対しては、自賠責に対する異議申立てなどで争うことも可能ですが、それが難しい場合には、裁判所で争うことも可能です。後遺障害等級認定には、怪我の内容に応じたノウハウが必要になりますので、弁護士に相談しましょう。</p>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>自賠責14級認定を<b>裁判で7級認定</b>に変更させた事例。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/enoki01.jpg" alt="榎木法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/aichi/enoki-law.html">榎木法律事務所</a>
<ul>
	<li><span>相談料</span><b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>東海地方</li>
	<li class="clear all"><span>住所</span>〒450-0002 愛知県名古屋市中村区名駅4-13-7 西柳パークビル2階</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>裁判前</sup>自賠責<b>14</b>級認定 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>裁判後</sup>裁判で<b>7</b>級認定</td>
</tr>
</table>

<table>
<tr>
<th>後遺障害等級</th>
<td>自賠責14級認定→裁判で7級認定</td>
</tr>
<tr>
<th>交通事故の状況</th>
<td>これは、私が弁護士1年目のころに受任し、最近ようやく解決に至った事案です。<br>被害者の方は、後方からの追突事故によって腰椎捻挫などの怪我を負い、その後、片足が動かせなくなるという症状（下垂足）を発症しました。様々な検査を受けたものの、交通事故との因果関係は明らかではなく、医師の意見も分かれているような状況でした。
</td>
</tr>
<tr>
<th>ご依頼内容</th>
<td>片足が動かせなくなったという症状（下垂足）について、納得のできる後遺障害等級が認定されるのかが最大の問題でした。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>私は、複数の医師と面談し、その結果を踏まえ、自賠責保険に対し後遺障害等級認定を求めました。しかし、自賠責保険は、画像等による裏付けが十分ではないとして、症状と交通事故との因果関係を否定しました（14級の認定）。<br>
　そこで、私は、裁判において自賠責保険の判断を争う方針を立て、その資料として、協力医と共に医学意見書の作成を進めました。珍しい症例のため、全国の大学図書館等から医学論文を集め、それを医師と一緒に読み込み、主張を組み立てていきました。<br>
検討の結果、主位的には、神経が圧迫・損傷を受け麻痺が生じていると主張し、それが否定されそうな場合には、予備的に、転換性障害という主張を追加するという方針を考えました。転換性障害とは、神経の異常ではなく、心因的な原因によって生じる麻痺のことです。これは、詐病とは違い、本当に自らの意志では体を動かすことができなくなってしまうものです。このような２本立て主張を用意したのは、事前の検討の中で、主位的な主張だけでは不十分だと思われたためです。<br>
訴訟戦略としては、なるべく早期に専門家による鑑定を求め、その結果を見た上で、予備的主張の追加の要否を検討することとしました。したがって、鑑定においても、予備的主張を意識した鑑定を行ってもらえるよう、鑑定事項を工夫しておきました。鑑定人の意見は、主位的主張に関しては否定的なものでしたが、予備的主張には好意的なものでした。そこで、その時点で予備的主張を追加し、それに関連する主張、立証を行いました。<br>
予備的主張には好意的と思われる鑑定意見が出ているにもかかわらず、当初、裁判所は、こちら側に厳し目の心証を持っていました。そこで、私は、追加の診断書や類似の裁判例等を提出し、裁判所の心証をこちら側に引き寄せるよう努力しました。その結果、裁判所は、交通事故と下垂足の因果関係を認め、後遺障害等級としても、自賠責保険では14級の認定だったのに対し、7級と認定しました。
</td>
</tr>
</table>
<a href="/bengo/aichi/enoki-law.html" class="btn">榎木法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>東京・京橋法律事務所へのご依頼で、<b>約1100万円</b>の増額。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/tokyo-kyoubashi01.jpg" alt="東京・京橋法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/tokyo/tokyo-kyoubashi.html">東京・京橋法律事務所</a>
<ul>
	<li><span>相談料</span><b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>関東地方</li>
	<li class="clear all"><span>住所</span>〒104-0031 東京都中央区京橋２丁目１１−６ 京橋彌生ビル 8F</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup><b>120</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup>約<b>1200</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>依頼者</th>
<td>男性・82歳</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>後遺障害7級(高次機能障害）</td>
</tr>
<tr>
<th>交通事故の状況</th>
<td>歩行者である被害者とバイクとの事故。過失相殺が大きく争われている一方、被害者は高次機能障害により事故の記憶なし。
</td>
</tr>
<tr>
<th>ご依頼内容</th>
<td>高齢者の高次機能障害であったところ、病院(主治医）により加齢による物忘れと診断される。
それゆえ、そもそも後遺障害がないものとしての示談しか進められなくなってしまった。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>後遺障害の等級申請には、医師の作成した後遺障害診断書が必要不可欠です。しかし、医師側の不適切な対応によって、この後遺障害診断書を作成して貰えないことは、大変残念なことです。
今回の事例は、病院長への抗議書を提出することで、高次機能障害の詳細な後遺障害診断書を作成してもらうことが出来、結果、後遺障害７級を獲得できました。
（なお、本件は過失相殺が大きな争いになる危険等を踏まえ、上記金額により示談となったものです。）
</td>
</tr>
</table>
<a href="/bengo/tokyo/tokyo-kyoubashi.html" class="btn">東京・京橋法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>100万円の支払い請求が<b>1100万円</b>の賠償額に。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/asuka01.jpg" alt="あすか法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/saitama/asuka-low.html">あすか法律事務所</a>
<ul>
	<li><span>相談料</span>初回<b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>関東</li>
	<li class="clear all"><span>住所</span>〒330-0063 埼玉県さいたま市浦和区高砂1-2-1　エイペックスタワー・オフィス東館510号</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup><b>100</b>万円の請求 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup><b>1100</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>依頼者</th>
<td>男性・70代</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>併合7級</td>
</tr>
<tr>
<th>交通事故の状況</th>
<td>交通事故で足を複雑骨折。</td>
</tr>
<tr>
<th>ご依頼内容</th>
<td>保険会社から「過失相殺した結果、払いすぎた治療費の一部を支払いなさい」と言われ、困って相談に訪れました。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>当事務所で受任後、足に関する後遺症の等級申請を行い、併合7級を獲得しました。保険会社から請求されていた医療費用も病院と減額交渉を行い、差額として約1100万円の損害賠償を受領して和解に至りました。<br>依頼者が高齢であったため、若干の判断能力の低下を理由に後遺症の認定も受けさせず、治療費請求をしていた保険会社の対応に非常に問題があった事案だと思います。本件ほど極端な例は希ですが、死亡事案や後遺症が残る事案については、弁護士が介入することで数十万～数百万円の賠償額の上乗せができることは珍しくはありません。</td>
</tr>
</table>
<a href="/bengo/saitama/asuka-low.html" class="btn">あすか法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>


<a href="#solvecase">目次へ戻る</a>
</li><?php //▲▲▲▲▲▲▲事例ここまで  ?>

<li id="case08"><?php //▲▲▲▲▲▲▲事例ここから  ?>
<h2>「後遺障害8級」の慰謝料増額・解決事例</h2>
<p>後遺障害8級の裁判所基準の慰謝料額は830万円。自賠責基準の慰謝料が324万円となっています。<br>
差額が506万円にもなります。保険会社の提示額が、納得のいく慰謝料額に至っていない場合、弁護士に相談しましょう。</p>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>小前田法律事務所へのご依頼後、<b>2倍以上増額</b>しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/komaeda01.jpg" alt="小前田法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/fukui/komaeda-law.html">小前田法律事務所</a>
<ul>
	<li><span>相談料</span>初回<b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>福井県</li>
	<li class="clear all"><span>住所</span>〒910-0005 福井市大手3-14-10 TMY大名町ビル5階</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup><b>1097</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup>約<b>2371</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>依頼者</th>
<td>女性・70代</td>
</tr>
<tr>
<th>受傷部位</th>
<td>顔面、左肩関節、左下腿</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>併合8級</td>
</tr>

<tr>
<th>交通事故の状況</th>
<td>被害者（ご依頼者）が道路を横断中に、前方不注視の加害者車両にはねられ、受傷しました。
</td>
</tr>
<tr>
<th>ご依頼内容</th>
<td>被害者は、顔面の醜状障害について第9級16号、左肩関節の機能障害について第12級6号、左下腿痛について第14級9号の後遺症が認定され、併合第8級に該当するものと認定されていました。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>保険会社の示談の提示額は後遺症による逸失利益や慰謝料が明らかに低い金額でした。弁護士が代理人となり、交渉を行えば大幅な増額が可能と判断しました。保険会社との交渉を経て、裁判に至ることなく和解となり、損害賠償額の大幅の増額を認めてもらうことができました。
</td>
</tr>
</table>
<a href="/bengo/fukui/komaeda-law.html" class="btn">小前田法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>

<a href="#solvecase">目次へ戻る</a>
</li><?php //▲▲▲▲▲▲▲事例ここまで  ?>



<li id="case09"><?php //▲▲▲▲▲▲▲事例ここから  ?>
<h2>「後遺障害9級」の慰謝料増額・解決事例</h2>
<p>後遺障害9級の裁判所基準の慰謝料額は690万円。自賠責基準の慰謝料が245万円となっています。<br>
差額が445万円にもなります。保険会社の提示額が、納得のいく慰謝料額に至っていない場合、弁護士に相談しましょう。</p>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>あい湖法律事務所へのご依頼で、<b>約750万円増額</b>しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/aiko01.jpg" alt="あい湖法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/shiga/aiko-jimusho.html">あい湖法律事務所</a>
<ul>
	<li><span>相談料</span><b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>滋賀県</li>
	<li class="clear all"><span>住所</span>滋賀県大津市 京町3丁目3-1 A&amp;M・OTSUビル2階</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup><b>100</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup>約<b>857</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>受傷部位</th>
<td>顔面・腰・頚部</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>9級16号</td>
</tr>
<tr>
<th>交通事故の状況</th>
<td>自転車（被害者） 対 自動車（加害者）。<br>交差点にて、横断歩道を横断中に衝突された。</td>
</tr>
<tr>
<th>ご依頼内容</th>
<td>自動車に衝突され、生死をさまよう交通事故にも関わらず、加害者・保険会社の誠意ない対応と賠償額の低さに不安を感じ相談されました。適正な賠償額を得られることを希望されました。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>加害者側保険会社は、不当に低い賠償額を提示していました。そこで、複数の判例を提示し、提示された賠償額が不当に低いことを伝え交渉を続けました。交渉の結果、600万円程度の増額が出来ましたが、
まだ納得できる賠償額に達していなかったため、訴訟することにしました。<br>当事者尋問まで済ませ、裁判官の心証の開示を頂き、お互いに納得した上で、和解に至りました。</td>
</tr>
</table>
<a href="/bengo/shiga/aiko-jimusho.html" class="btn">あい湖法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>南谷綜合法律事務所へのご依頼で、<b>約1100万円増額</b>しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/minami04.jpg" alt="南谷綜合法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/tokyo/minamitani-law.html">南谷綜合法律事務所</a>
<ul>
	<li><span>相談料</span><b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>関東</li>
	<li class="clear all"><span>住所</span>〒150-0021　東京都渋谷区恵比寿西2-19-9フランセスビル1階</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup><b>-100</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup>約<b>1000</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>ご依頼者</th>
<td>男・20代</td>
</tr>
<tr>
<th>受傷部位</th>
<td>顎骨骨折、顔面挫傷等</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>9級16号</td>
</tr>
<tr>
<th>交通事故の状況</th>
<td>被害者が自動二輪車にて走行中、一時停止規制を守らず交差点に進入した加害車両が衝突</td>
</tr>
<tr>
<th>ご依頼内容</th>
<td>逸失利益の金額の増額、過失割合への反論</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>本件は、後遺障害として容姿への影響が問題となった事案であり、当初の提示では、保険会社は、非常に低い額の逸失利益しか認めませんでした。また、事故態様にも争いがあり、当初の提示は、被害者の大きな過失を前提としていました。<br>そして、被害者は既に自賠責から賠償金の一部を受け取っていたため、保険会社の提示は、マイナス100万円という被害者に返金を要求するものでした。<br>
容姿への影響による逸失利益については、過去の裁判例では逸失利益が否定されたケースも多く、認められた場合も事件により金額が大きく異なります。<br>
そのため、事件処理の方針によって、金額が大きく変わり得る案件でしたが、事件処理の結果として、逸失利益は1100万円を超える増額、被害者の過失も約半分となり、相当な金額の賠償金を獲得することができました。</td>
</tr>
</table>
<a href="/bengo/tokyo/minamitani-law.html" class="btn">南谷綜合法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>



<a href="#solvecase">目次へ戻る</a>
</li><?php //▲▲▲▲▲▲▲事例ここまで  ?>

<li id="case10"><?php //▲▲▲▲▲▲▲事例ここから  ?>
<h2>「後遺障害10級」の慰謝料増額・解決事例</h2>
<p>後遺障害10級の裁判所基準の慰謝料額は550万円。自賠責基準の慰謝料が187万円となっています。<br>
差額が363万円にもなります。保険会社の提示額が、納得のいく慰謝料額に至っていない場合、弁護士に相談しましょう。</p>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>湘南平塚法律事務所へのご依頼で、<b>約950万円増額</b>しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/syounannhiratukalaw01.jpg" alt="湘南平塚法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/kanagawa/syounannhiratuka-law.html">湘南平塚法律事務所</a>
<ul>
	<li><span>相談料</span>初回<b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>神奈川県</li>
	<li class="clear all"><span>住所</span>〒254-0043 神奈川県平塚市紅谷町3-3　本陣ビル2階</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup>約<b>550</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup>約<b>1500</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>受傷部位</th>
<td>顔面・腰・頚部</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>12級 → 10級</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>受任当初は、12級との後遺障害等級認定でしたが、脊索についての症状が判断されていないと考え、追加認定を小職が資料を手配して申請しました。その結果として10級との認定を受けました。当初、約550万円の提示額が、最終的には約1500万円の損害賠償金が認められました。約950万円の増額となりました。追加認定を受けた結果飛躍的に賠償額が上昇したケースです。受任後の提示であったため、当初の提示額についても保険会社基準としては評価できるものでしたが、被害者の病状や現状を考え、後遺障害について争ったことが大きかったと考えています。</td>
</tr>
</table>
<a href="/bengo/kanagawa/syounannhiratuka-law.html" class="btn">湘南平塚法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>常川総合法律事務所へのご依頼で、<b>約600万円増額</b>しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/tunekawa01.jpg" alt="常川総合法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/aichi/goto-suzuki-law.html">常川総合法律事務所</a>
<ul>
	<li><span>相談料</span>初回<b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>愛知県・岐阜県・三重県</li>
	<li class="clear all"><span>住所</span>〒460-0002 名古屋市中区丸の内3-18-1 三晃丸の内ビル10階</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup>約<b>600</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup>約<b>1200</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>依頼者</th>
<td>男性・30代</td>
</tr>

<tr>
<th>受傷部位</th>
<td>左股関節脱臼骨折</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>10級相当</td>
</tr>
<tr>
<th>事故の状況
</th>
<td>原動機付自転車を運転中、加害車両（トラック）が大きくセンターラインオーバーしたため、正面衝突をした事故でした。</td>
</tr>
<tr>
<th>ご依頼内容</th>
<td>相手方保険会社からの提示額が余りに低いので、どうにかしてほしい</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>保険会社の提示額を検討すると、特に通院慰謝料、後遺症慰謝料及び逸失利益について裁判所基準からかけ離れた金額だったため、裁判所基準に基づいて示談交渉を行いました。依頼者の都合によりあまり長い時間をかけられなかったため、難しい交渉となりましたが、最終的に総額として裁判所基準を上回る金額で合意することができ、保険会社の当初の提示額より総額で約６００万円、２倍に増額することができました。</td>
</tr>
</table>
<a href="/bengo/aichi/goto-suzuki-law.html" class="btn">常川総合法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>


<a href="#solvecase">目次へ戻る</a>
</li><?php //▲▲▲▲▲▲▲事例ここまで  ?>

<li id="case11"><?php //▲▲▲▲▲▲▲事例ここから  ?>
<h2>「後遺障害11級」の慰謝料増額・解決事例</h2>
<p>後遺障害11級の裁判所基準の慰謝料額は420万円。自賠責基準の慰謝料が135万円となっています。<br>
差額が285万円にもなります。保険会社の提示額が、納得のいく慰謝料額に至っていない場合、弁護士に相談しましょう。</p>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>吉原法律事務所へのご依頼で、<b>約330万円増額</b>しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/yoshihara01.jpg" alt="吉原法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/hokkaido/yoshihara-law.html">吉原法律事務所</a>
<ul>
	<li><span>相談料</span><b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>北海道</li>
	<li class="clear all"><span>住所</span>〒064-0820 札幌市中央区大通西20丁目2番20号 エクセルS1ビル（旧道新円山ビル）8階</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup>約<b>881</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup>約<b>1211</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>受傷部位</th>
<td>第二腰椎圧迫骨折</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>11級</td>
</tr>
<tr>
<th>結果</th>
<td>示談交渉による和解</td>
</tr>
<tr>
<th>事故の状況</th>
<td>搭乗中の事故</td>
</tr>
<tr>
<th>ご依頼内容</th>
<td>提示額の妥当性</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>主婦の休業損害につき、提示では入院及び実通院日数のみを対象としていたが、通院日以外の治療期間中についても一定割合の休業損害を主張、逸出利益の計算での労働の能力喪失期間の是正、障害慰謝料及び後遺障害慰謝料につき、裁判所基準での請求で交渉した結果、増額和解となった。</td>
</tr>
</table>
<a href="/bengo/hokkaido/yoshihara-law.html" class="btn">吉原法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>


<a href="#solvecase">目次へ戻る</a>
</li><?php //▲▲▲▲▲▲▲事例ここまで  ?>



<li id="case12"><?php //▲▲▲▲▲▲▲事例ここから  ?>
<h2>「後遺障害12級」の慰謝料増額・解決事例</h2>
<p>後遺障害12級の裁判所基準の慰謝料額は550万円。自賠責基準の慰謝料が187万円となっています。<br>
差額が363万円にもなります。保険会社の提示額が、納得のいく慰謝料額に至っていない場合、弁護士に相談しましょう。</p>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>三吉法律事務所へのご依頼で、<b>約540万円増額</b>しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/miyos01.jpg" alt="三吉法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/tokyo/miyoshi-law.html">三吉法律事務所</a>
<ul>
	<li><span>相談料</span><b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>関東</li>
	<li class="clear all"><span>住所</span>〒103-0014　東京都中央区日本橋蛎殻町1-22-1デュークスカーラ日本橋301</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup>約<b>260</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup>約<b>800</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>依頼者</th>
<td>男性・30代</td>
</tr>
<tr>
<th>受傷部位</th>
<td>鎖骨</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>12級5号</td>
</tr>
<tr>
<tr>
<th>交通事故の状況</th>
<td>タクシー運転手（男性）が交差点で出会い頭にバイクと衝突事故を起こしました。運転手は勤務外に自家用車を運転している最中の事故でした。今回の事案で男性は、鎖骨の骨を折る大怪我を負い、後遺障害第12級5号と認定を受けました。</td>
</tr>
<th>ご依頼内容</th>
<td>相手側保険会社の賠償金提示額が260万円でしたが、その賠償額が果たして適正な金額なのかが分からず相談に来られました。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>保険会社の提示した賠償額では、逸失利益が低く算定されており、”労働能力喪失率が認められる”ということを主張し紛争処理センターへ申し立てを行い、逸失利益560万の提示に総額800万円での示談となりました。</td>
</tr>

</table>
<a href="/bengo/tokyo/miyoshi-law.html" class="btn">三吉法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>京都グリーン法律事務所へのご依頼で、<b>約875万円増額</b>しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/green01.jpg" alt="京都グリーン法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/kyoto/green-law.html">京都グリーン法律事務所</a>
<ul>
	<li><span>相談料</span><b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>京都・滋賀</li>
	<li class="clear all"><span>住所</span>京都府京都市中京区虎屋町５７７−２　太陽生命御池ビル2階</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup>約<b>500</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup>約<b>1375</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>依頼者</th>
<td>男性・40代</td>
</tr>
<tr>
<th>受傷部位</th>
<td>頚椎捻挫、左中指中節骨骨折</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>12級相当</td>
</tr>
<tr>
<tr>
<th>交通事故の状況</th>
<td>加害者が大きくセンターラインオーバーし、衝突された。</td>
</tr>
<th>ご依頼内容</th>
<td>保険会社の提示額に納得できない。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>保険会社の提示額を検討すると、慰謝料について裁判所基準とかけ離れた金額だったことに加え、逸失利益について労働能力喪失期間の認定が３年と短く、極めて低い提示額であった。当職は、慰謝料について裁判所基準に基づき請求すると共に、労働能力喪失期間を長期に認めるよう関連判例を保険会社に提示しながら強く交渉した。結果として、慰謝料、逸失利益共に大幅に増額することに成功し、当初の提示よりも９００万円近く増額することができた。</td>
</tr>
</table>
<a href="/bengo/kyoto/green-law.html" class="btn">京都グリーン法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>



<a href="#solvecase">目次へ戻る</a>
</li><?php //▲▲▲▲▲▲▲事例ここまで  ?>



<li id="case14"><?php //▲▲▲▲▲▲▲事例ここから  ?>
<h2>「後遺障害14級」の慰謝料増額・解決事例</h2>
<p>後遺障害14級の裁判所基準の慰謝料額は110万円。自賠責基準の慰謝料が32万円となっています。<br>
差額が78万円にもなります。保険会社の提示額が、納得のいく慰謝料額に至っていない場合、弁護士に相談しましょう。</p>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>ブランシュ法律事務所へのご依頼で、<b>250万円増額</b>しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/blanche01.jpg" alt="ブランシュ法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/osaka/blanche-law.html">ブランシュ法律事務所</a>
<ul>
	<li><span>相談料</span>初回<b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>兵庫・大阪</li>
	<li class="clear all"><span>住所</span>〒650-0015 兵庫県神戸市中央区多聞通２丁目１−７ 法友会館 6F‎</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup><b>220</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup><b>470</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>依頼者</th>
<td>女性・40代</td>
</tr>
<tr>
<th>受傷部位</th>
<td>頸椎捻挫</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>認定なし → 14級</td>
</tr>
<tr>
<th>事故の状況</th>
<td>40代の女性がご依頼者様でした。交通事故に巻き込まれて、頸椎捻挫の診断を受けました。症状固定の後、保険会社の慰謝料提示額は約220万円でした。しかしその後も、ご依頼者様は頸椎捻挫の症状に悩まされて続けており、保険会社の慰謝料提示額に到底納得がいかないとご相談に来られました。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>当事務所にご依頼を頂き、保険会社との交渉を行い、通院慰謝料の算定基準の引き上げと後遺障害申請による後遺障害逸失利益等で、合計約470万円の損害賠償金を支払うことで合意に至りました。
約250万円の賠償金増額に、ご依頼者様も大変喜んでおられました。</td>
</tr>
</table>
<a href="/bengo/osaka/blanche-law.html" class="btn">ブランシュ法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>神垣守法律事務所へのご依頼で、お二人の総額<b>730万円</b>の賠償金を獲得。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/kamigaki01.jpg" alt="神垣守法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/hyogo/kamigaki-law.html">神垣守法律事務所</a>
<ul>
	<li><span>相談料</span>初回<b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>関西</li>
	<li class="clear all"><span>住所</span>〒530-0047 大阪府大阪市北区西天満4-1-4第三大阪弁護士ビル301</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼後</sup>後遺障害12級を獲得し、お二人総額<b>730</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>依頼者</th>
<td>男性・女性（2名）</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>認定なし → 14級9号</td>
</tr>
<tr>
<th>事故の状況</th>
<td>カップルで乗車中に、追突事故に遭われた方のご相談です。男性の方は自営業を営んでおりましたが、確定申告をしていなかったため、相手保険会社から休業損害の額が、相当に低い金額で提案を受けていました。<br>また、お二人とも交通事故の後遺症に苦しんでおられましたが、レントゲン等で異常がなく、自賠責保険の後遺障害には該当しないと判断されていました。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>ご依頼を受けた後、休業損害についての交渉を行いました。売上を記載したノートとそれを補強する他の資料（証拠）を元に保険会社と交渉を続け、ご依頼者も納得のいく休業損害額の支払いを受けられることになりました。<br>後遺症については、病院へ照会を行い、その結果を踏まえて、異議申立を行いました。お二人とも後遺障害等級14級9号の認定をもらいました。<br>最終的には交渉で、お二人総額730万円で和解が成立しました。お二人とも時間が掛かるため後遺障害の異議申立を諦めるようかともおっしゃっていましたが、諦めず、異議申立を行ったのがよかったと思います。</td>
</tr>
</table>
<a href="/bengo/hyogo/kamigaki-law.html" class="btn">神垣守法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>


<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>ありあけ法律事務所へのご依頼で、<b>440万円増額</b>しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/ariake01.jpg" alt="ありあけ法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/saga/ariake-law.html">ありあけ法律事務所</a>
<ul>
	<li><span>相談料</span><b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>佐賀県</li>
	<li class="clear all"><span>住所</span>〒840-0833 佐賀県佐賀市中の小路７－７</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup><b>117</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup><b>557</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>依頼者</th>
<td>女性・40代</td>
</tr>
<tr>
<th>受傷部位</th>
<td>頚椎捻挫</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>非該当 → 14級</td>
</tr>
<tr>
<th>ご依頼内容</th>
<td>ご依頼者は、交通事故に巻き込まれて頚椎捻挫と診断されました。そして、痛みがまだ残っているにも関わらず、自賠責では後遺障害は非該当と認定されてしまいました。保険会社の提示額は、賠償額117万円でした。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>保険会社の認定・賠償額に納得がいかない依頼者は、当事務所にご相談に訪れ依頼となりました。<br>裁判を通じて、痛みなどの後遺障害を主張。後遺障害14級を獲得しました。賠償額は400万円増額の557万円で和解が成立しました。<br>保険会社の後遺障害非該当の認定をあきらめずに痛みを訴えたことで、賠償額が400万円増額して、<strong>保険会社の提示額の4.7倍</strong>になりました。</td>
</tr>
</table>
<a href="/bengo/saga/ariake-law.html" class="btn">ありあけ法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>


<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>河野邦広法律事務所へのご依頼で、<b>320万円増額</b>しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/kouno01.jpg" alt="河野邦広法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/saitama/kouno-law.html">河野邦広法律事務所</a>
<ul>
	<li><span>相談料</span><b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>関東</li>
	<li class="clear all"><span>住所</span>埼玉県さいたま市浦和区東高砂町2-3 プリムローズビル3階D</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup>約<b>110</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup>約<b>430</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>依頼者</th>
<td>女性・40代</td>
</tr>
<tr>
<th>受傷部位</th>
<td>首・腰</td>
</tr>
<tr>
<th>後遺障害等級</th>
<td>14級</td>
</tr>
<tr>
<th>事故の状況</th>
<td>依頼者の夫が運転する車の助手席に乗って居眠りをしていたところ、追突された。</td>
</tr>
<tr>
<th>ご依頼内容</th>
<td>賠償額の引き上げ</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>依頼時、既に頸椎捻挫で１４級を取得していたが、非該当であった腰椎捻挫についても異議申立てを行いました。医師の意見書、夫の陳述書等を添付したところ異議が認められ、１４級を取得しました。その後、交渉の結果、当初提示額の４倍近い金額で和解することができました。</td>
</tr>
</table>
<a href="/bengo/saitama/kouno-law.html" class="btn">河野邦広法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>



<a href="#solvecase">目次へ戻る</a>
</li><?php //▲▲▲▲▲▲▲事例ここまで  ?>


<li id="busson"><?php //▲▲▲▲▲▲▲事例ここから  ?>
<h2>「物損事故」の慰謝料増額・解決事例</h2>
<p>物損事故でも弁護士に依頼すると、賠償額が増額する可能性はあります。<br />
評価額に不満があるなら、まずは弁護士に相談したほうがよいでしょう。</p>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>たおく法律事務所へのご依頼で、<b>30万円増額</b>しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/taoku01.jpg" alt="たおく法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/hiroshima/taoku-law.html">たおく法律事務所</a>
<ul>
	<li><span>相談料</span><b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>広島県</li>
	<li class="clear all"><span>住所</span>〒737-0046 広島県呉市中通2丁目1-26 呉中通ビル401-1号室</li>
</ul>

<table>
<tr>
<th>慰謝料増額</th>
<td><sup>ご依頼前</sup><b>107</b>万円 <img src="<?php bloginfo('template_directory'); ?>/img/ic_uparrow.gif" alt="UP" /> <sup>ご依頼後</sup><b>137</b>万円</td>
</tr>
</table>

<table>
<tr>
<th>依頼者</th>
<td>男性・30代</td>
</tr>
<tr>
<th>交通事故の状況</th>
<td>居眠り運転中の車に追突された。高速道路の出口、一般道との合流地点にて。</td>
</tr>
<tr>
<th>ご依頼内容</th>
<td>保険会社に評価損の支払いを拒否されている。格落ち損害分を支払ってもらいたい。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>加害者側の保険会社の主張としては、「評価損については一切認めない」ということでした。当事務所にご相談頂きお話を伺ったところ、ほぼ新車であるにも関わらず、今回の事故によって、修復歴のある車と評価されることを指摘しました。訴訟も覚悟した上で保険会社との交渉を進め、30万円の評価損の支払いで合意となりました。<br />ご依頼からそれほどの時間もかからず解決に至りました。</td>
</tr>
</table>
<a href="/bengo/hiroshima/taoku-law.html" class="btn">たおく法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>


<a href="#solvecase">目次へ戻る</a>
</li><?php //▲▲▲▲▲▲▲事例ここまで  ?>

<li id="keiji"><?php //▲▲▲▲▲▲▲事例ここから  ?>
<h2>「刑事事件」の慰謝料増額・解決事例</h2>
<p>交通事故が刑事事件に…そんな場合は弁護士に相談すべきです。<br>
刑事事件を弁護士に相談するとどんな解決を迎えるのでしょう。</p>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>小島法律事務所へのご依頼で、<b>無実が証明</b>されました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/kojima01.jpg" alt="小島法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/fukuoka/kojima-law.html">小島法律事務所</a>
<ul>
	<li><span>相談料</span>初回<b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>福岡県</li>
	<li class="clear all"><span>住所</span>〒820-0040 福岡県飯塚市吉原町6番1号 あいタウン3F</li>
</ul>

<table>
<tr>
<th>結論</th>
<td><sup>ご依頼後</sup><b>無実</b>が証明されました。</td>
</tr>
</table>

<table>
<tr>
<th>ご依頼内容</th>
<td>ご相談者に身の覚えがないのに接触されたと訴えられたケース</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>ご相談者の自動車が接触してきて、交通事故が起こったと訴えられたケース。当事務所が、刑事記録を精査し、相手の主張の矛盾点を発見したことから、公判廷において担当警察官の尋問を実施しました。その結果、当初「加害者」とされていた相談者は当該事故と無関係であることが明らかとなりました。</td>
</tr>
</table>
<a href="/bengo/fukuoka/kojima-law.html" class="btn">小島法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>


<a href="#solvecase">目次へ戻る</a>
</li><?php //▲▲▲▲▲▲▲事例ここまで  ?>

<li id="kashitsu"><?php //▲▲▲▲▲▲▲事例ここから  ?>
<h2>「過失割合」の解決事例</h2>
<p>交通事故の中でもトラブルになり易い過失割合。納得いかない場合は弁護士に相談すべきです。<br>
過失割合で揉めた事案を弁護士に相談するとどんな解決を迎えるのでしょう。</p>

<article><?php //▲▲▲▲▲▲▲事務所ここから  ?>
<h3>堀江重尊法律事務所へのご依頼で、<b>過失ゼロ</b>で解決しました。</h3>
<img src="<?php bloginfo('template_directory'); ?>/img/jimusho/horie01.jpg" alt="堀江重尊法律事務所" width="200" height="135" class="alignleft" />
<a href="/bengo/ishikawa/horieshigetaka-law.html">堀江重尊法律事務所</a>
<ul>
	<li><span>相談料</span>初回<b>0</b>円</li>
	<li><span>着手金</span><b>0</b>円</li>
	<li><span>弁護士特約</span>利用可能</li>
	<li><span>対応エリア</span>石川県</li>
	<li class="clear all"><span>住所</span>〒926-0047　石川県七尾市大手町5番地3　坂口ビル2階</li>
</ul>

<table>
<tr>
<th>結論</th>
<td><sup>ご依頼後</sup><b>過失なし</b>が証明されました。</td>
</tr>
</table>

<table>
<tr>
<th>ご依頼内容</th>
<td>コンビニの駐車場内での交通事故。バックで空いたスペースに駐車しているときに、別の駐車車両がバックで進んできて、衝突。相手方は、過失割合「5:5」を主張し、当方の主張と食い違い協議が進まない状況でした。</td>
</tr>
<tr>
<th>ご依頼後</th>
<td>当方に過失がないという根拠を示した内容証明郵便を送付しました。その後、相手方が当方に過失がないことを認め、当方の損害をすべて負担するという内容で示談が成立しました。</td>
</tr>
<tr>
<th>弁護士からのコメント</th>
<td>交通事故事案では、例え損害額が軽微であっても、過失割合が争いとなることは珍しくありません。協議が平行線をたどると、時間ばかり経過していくことになります。弁護士が関与することで、解決の糸口を発見できることもあります。<br>最近の自動車保険には弁護士費用特約という、弁護士費用を保険会社が負担する特約がついている場合もありますので、積極的に利用されたらよろしいかと思います。</td>
</tr>


</table>
<a href="/bengo/ishikawa/horieshigetaka-law.html" class="btn">堀江重尊法律事務所の事務所紹介を見る</a>
</article><?php //▲▲▲▲▲▲▲事務所ここまで  ?>


<a href="#solvecase">目次へ戻る</a>
</li><?php //▲▲▲▲▲▲▲事例ここまで  ?>



</ul>
</article>




</div>
<!-- //front--></div>


<?php get_footer();