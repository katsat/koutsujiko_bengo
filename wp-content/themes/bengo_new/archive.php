<?php get_header(); ?>


<?php
foreach((get_the_category()) as $cat) {
$cat_id = $cat->cat_ID ;
$cat_name = $cat->cat_name;
$cat_slug = $cat->slug;
$cat_description = $cat->description;
break ;
}
?>

<div id="wrap">
<div id="main">
<div class="archive_doc">

<h2><?php echo $cat_name; ?></h2>

<nav class="pager">
<?php custum_pagination($wp_query->max_num_pages); ?>
<div class="count"><?php my_result_count();?></div>
</nav>

<article>
<ul class="list">


<?php
global $wp_query;
$args = array_merge( $wp_query->query, array('post__not_in' => get_option( 'sticky_posts' ), 'order' => 'ASC') );
query_posts( $args ); $iii = 0;?>
<?php while ( have_posts() ) : the_post();?>

<li class="wraplink"><?php catch_that_image('150', '150');?><h3><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3><p><?php
$content = apply_filters('the_content',get_the_content());
$content = strip_tags($content);
$content = mb_substr($content, 0, 114);

echo $content; ?>...</p></li>

<?php $iii++; endwhile; wp_reset_query(); unset($iii);?>



</ul>
</article>

<nav class="pager">
<?php custum_pagination($wp_query->max_num_pages); ?>
<div class="count"><?php my_result_count();?></div>
</nav>

<!-- //archive_doc--></div>

<!-- //main--></div>





<?php get_footer();