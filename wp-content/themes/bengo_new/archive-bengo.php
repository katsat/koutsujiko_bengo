<?php get_header(); ?>


<div class="front bengoarchive">
<div class="inner">
<header>
<?php
if(isset($_GET['jian'])){

  if($_GET['jian'] == "jian_isya"){
    $jian = "「慰謝料」に強い弁護士";
  }elseif($_GET['jian'] == "jian_songai"){
    $jian = "「損害賠償」に強い弁護士";
  }elseif($_GET['jian'] == "jian_jidan"){
    $jian = "「示談交渉」に強い弁護士";
  }elseif($_GET['jian'] == "jian_kashitsu"){
    $jian = "「過失割合」に強い弁護士";
  }elseif($_GET['jian'] == "jian_busson"){
    $jian = "「物損事故」に強い弁護士";
  }elseif($_GET['jian'] == "jian_jinshin"){
    $jian = "「人身事故」に強い弁護士";
  }elseif($_GET['jian'] == "jian_shibo"){
    $jian = "「死亡事故」に強い弁護士";
  }elseif($_GET['jian'] == "jian_kouisyo"){
    $jian = "「後遺障害」に強い弁護士";
  }elseif($_GET['jian'] == "jian_donichi"){
    $jian = "「土日対応」可能な弁護士";
  }elseif($_GET['jian'] == "jian_24h"){
    $jian = "「24時間電話」可能な弁護士";
  }else{
    $jian = "交通事故に強い弁護士";
  }

}elseif(isset($_GET['status'])){
  if($_GET['status'] == "tyakusyu"){
    $jian = "「着手金0円」の弁護士";
  } elseif($_GET['status'] == "free"){
    $jian = "「相談料0円」の弁護士";
  }
}else{
  $jian = "交通事故に強い弁護士";
}
$jian_title = $jian;
?>
<span>【厳選掲載】交通事故問題に強い弁護士ランキング</span>
<h1>実績多数!<?php echo $jian; ?></h1>
</header>
<div class="check">
<ul>
<?php
$jian = htmlspecialchars($_GET['jian']);
$status = htmlspecialchars($_GET['status']);
//if($jian) $query_tmp = $wpdb->prepare(" AND meta_value LIKE %s" , '%'. like_escape($jian). '%');
//if($status) $query_tmp = $wpdb->prepare(" AND meta_value LIKE %s" , '%'. like_escape($status). '%');
if ($jian != "") {
  $bengo_order_jian = $jian;
} elseif ($status != "") {
  $bengo_order_jian = $status;
} else {
  $bengo_order_jian = null;
}

?>
<li<?php if($jian == 'jian_isya') echo' class="r"';?>><a href="/bengo?jian=jian_isya"><em>慰謝料</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_songai') echo' class="r"';?>><a href="/bengo?jian=jian_songai"><em>損害賠償</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_jidan') echo' class="r"';?>><a href="/bengo?jian=jian_jidan"><em>示談交渉</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_kashitsu') echo' class="r"';?>><a href="/bengo?jian=jian_kashitsu"><em>過失割合</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_busson') echo' class="r"';?>><a href="/bengo?jian=jian_busson"><em>物損事故</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_jinshin') echo' class="r"';?>><a href="/bengo?jian=jian_jinshin"><em>人身事故</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_shibo') echo' class="r"';?>><a href="/bengo?jian=jian_shibo"><em>死亡事故</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_kouisyo') echo' class="r"';?>><a href="/bengo?jian=jian_kouisyo"><em>後遺障害</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_donichi') echo' class="r"';?>><a href="/bengo?jian=jian_donichi"><em>土日対応</em>可能な弁護士</a></li>
<li<?php if($jian == 'jian_24h') echo' class="r"';?>><a href="/bengo?jian=jian_24h"><em>24H電話</em>可能な弁護士</a></li>
<li<?php if($status == 'tyakusyu') echo' class="r"';?>><a href="/bengo?status=tyakusyu"><em>着手金0円</em>の弁護士</a></li>
<li<?php if($status == 'free') echo' class="r"';?>><a href="/bengo?status=free"><em>相談料0円</em>の弁護士</a></li>
</ul>
</div>

<div class="button">
<a href="#tofuken">都道府県から弁護士を探す</a>
</div>

<?php

$args = array(
    'post_type' => 'bengo',
    'order' => 'DESC',
/*    'meta_key' => 'bengo_order',
    'meta_query' => array(
    	'relation'=>'AND',
    	array('relation'=>'OR',
	    	array( 'key' => 'bengo_taiou', 'value' => 'japan', 'compare' => 'IN' ),
	    	array( 'key' => 'bengo_taiou', 'value' => $term_slug, 'compare' => 'IN' )
    	),
    	$query_tmp,
    ),*/
    //'posts_per_page' => -1,
    'paged' => $paged
);



//$ret =$wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE meta_key = 'bengo_order' AND meta_value LIKE %s" , '%'. like_escape('zenkoku'). '%') . $query_tmp);

/*
if($query_tmp):
 if($status){
  $order_key2 = $status;
 }else{
  $order_key2 = $jian;
 }
 $order_key = 'zenkoku';
else:
 $order_key = 'zenkoku';
endif;
foreach($ret as $orders):
$sss = json_decode($orders->meta_value, true);
$ooo[$orders->post_id] = $sss[$order_key] + $sss[$order_key2];
endforeach;
*/

// 弁護士表示区分「関西ランキング」の記事リスト取得
$ret = $wpdb->get_results(TBO::getQueryOfPostsByBengoOrderTypeOrdered('zenkoku', $bengo_order_jian));

foreach($ret as $orders):
$sss = json_decode($orders->meta_value, true);
//$ooo[$orders->post_id] = $sss[$order_key] + $sss[$order_key2];
if ($status){
  $sort = $orders->sort + $sss[$status];
} elseif ($jian) {
  $sort = $orders->sort + $sss[$jian];
} else {
  $sort = $orders->sort;
}
$ooo[$orders->ID] = $sort;
endforeach;

//print_r($status);
if(is_array($ooo)){
arsort($ooo);
$args['post__in'] = array_keys($ooo);
} else {
}
$args['orderby'] = 'post__in';
$the_query = new WP_Query( $args );

?>
<!--<?php //print_r($query_tmp);?>-->



<nav class="pager">
<?php if($the_query->have_posts() && $args['post__in']): ?>
<?php custum_pagination($the_query->max_num_pages); ?>
<div class="count"><?php my_result_count($the_query);?></div>
<?php endif;?>
</nav>


<article>
<ul class="list">


<?php
if($the_query->have_posts() && $args['post__in']):
while ( $the_query->have_posts() ) : $the_query->the_post();?>

<li<?php if(mb_strlen(get_the_title()) > 20):?> class="ll<?php if(get_post_meta($post->ID , 'osusume_flag', FALSE)):?> osusume<?php endif;?>"<?php endif;?><?php if(get_post_meta($post->ID , 'osusume_flag', FALSE)):?> class="osusume"<?php endif;?>>
<p><span class="area">対応：<?php echo get_post_meta($post->ID , 'bengo_area', TRUE);?></span>
<?php if(get_post_meta($post->ID , 'shozai_area', TRUE)): ?><span class="shozai">事務所：<?php echo get_post_meta($post->ID , 'shozai_area', TRUE);?></span><?php endif; ?></p>
<h2><?php echo get_post_meta($post->ID , 'bengo_catch', TRUE);?></h2>
<div class="bengo_image">
<a href="<?php the_permalink();?>"><?php echo wp_get_attachment_image(post_custom('File Upload'), 'medium', false, array('class' => 'visual', 'alt' => get_the_title()));?></a><div class="helpline">
<strong>お電話での相談窓口【通話無料】</strong>
<a href="<?php the_permalink(); ?>?pid=<?php echo $post->ID?>" rel="nofollow"><?php echo get_post_meta($post->ID , 'bengo_tel', TRUE);?></a>
<table>
<tr>
<th>受付時間</th>
<td><?php echo get_post_meta($post->ID , 'bengo_open', TRUE);?></td>
</tr>
</table>
</div>
</div>


<div class="bengo_detail">
<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
<table>
<tr>
<td><em>相談料</em><?php echo get_post_meta($post->ID , 'bengo_price', TRUE);?></td>
<td><em>着手金</em><?php echo nl2br(get_post_meta($post->ID , 'bengo_start', TRUE));?></td>
<td><em>報酬金</em><?php echo nl2br(get_post_meta($post->ID , 'bengo_success', TRUE));?></td>
</tr>
</table>
<div class="sub">
<ul>
<?php $tmp_status = get_post_meta($post->ID , 'bengo_status', FALSE);?>
<?php $tmp_jian = get_post_meta($post->ID , 'bengo_jian', FALSE);?>
<li<?php if(!in_array('area' ,$tmp_status)) echo ' class="n"';?>>全国対応</li>
<li<?php if(!in_array('pps' ,$tmp_status)) echo ' class="n"';?>>後払い可</li>
<li<?php if(!in_array('tokuyaku' ,$tmp_status)) echo ' class="n"';?>>弁護特約</li>
<li<?php if(!in_array('jian_donichi' ,$tmp_jian)) echo ' class="n"';?>>土日対応</li>
</ul>
<h4>料金体系</h4>
<p><?php echo nl2br(get_post_meta($post->ID , 'bengo_ryokin', TRUE));?></p>
</div>
<div class="view">
<aside>
<?php if(get_post_meta($post->ID , 'mail_flag', FALSE)):?>

<?php if(in_array('tokuyaku' ,$tmp_status) && !wp_is_mobile()){ ?>
<h5 class="int">弁護士特約利用可能</h5>
<a href="<?php the_permalink(); ?>#tokuyaku"><img src="<?php bloginfo('template_directory'); ?>/img/btn_s_tokuyaku.png" alt="弁護士費用特約利用可能" /></a>
<p>自己負担なしで弁護士への依頼が可能</p>
<?php } ?>
<?php else: ?>

<h5>インターネットから相談</h5>
<a href="/bengo_contact?post_id=<?php the_ID();?>" rel="nofollow"><img src="<?php bloginfo('template_directory'); ?>/img/btn_s_freeform.png" alt="無料相談フォーム" class="btn_form" /></a>
<p>※無料相談フォームは24時間365日受付</p>

<?php endif; ?>
</aside>
<a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/btn_s_detail.png" alt="詳細情報を見る" class="btn_detail"  /></a>
</div>

</div>

<?php if(get_post_meta($post->ID , 'osusume_flag', FALSE)):?>
<div class="osusume_flag_title">【実績多数】<?php echo $jian_title; ?></div>
<span class="osusume_flag_badge"><img src="<?php bloginfo('template_directory'); ?>/img/ic_osusume_badge.png" alt="注目弁護士" /></span>
<?php endif;?>

</li>
<?php endwhile; wp_reset_query();?>
<?php else:?>
<li><h2>検索結果がありませんでした。</h2></li>
<?php endif;?>
</ul>
</article>


<nav class="pager">
<?php if($the_query->have_posts() && $args['post__in']): ?>
<?php custum_pagination($the_query->max_num_pages); ?>
<div class="count"><?php my_result_count($the_query);?></div>
<?php endif;?>
</nav>

<header id="tofuken">
<span>お住まいの地域に対応した交通事故に強い弁護士</span>
<h1>「都道府県」から弁護士事務所を探す</h1>
</header>

<div class="check">
<dl>
<dt>北海道・東北地方</dt>
<dd><a href="/bengo/hokkaido">北海道</a></dd>
<dd><a href="/bengo/aomori">青森</a></dd>
<dd><a href="/bengo/iwate">岩手</a></dd>
<dd><a href="/bengo/akita">秋田</a></dd>
<dd><a href="/bengo/miyagi">宮城</a></dd>
<dd><a href="/bengo/yamagata">山形</a></dd>
<dd><a href="/bengo/fukushima">福島</a></dd>
<dt>関東</dt>
<dd><a href="/bengo/tokyo">東京</a></dd>
<dd><a href="/bengo/saitama">埼玉</a></dd>
<dd><a href="/bengo/kanagawa">神奈川</a></dd>
<dd><a href="/bengo/chiba">千葉</a></dd>
<dd><a href="/bengo/ibaraki">茨城</a></dd>
<dd><a href="/bengo/tochigi">栃木</a></dd>
<dd><a href="/bengo/gunma">群馬</a></dd>
<dt>北陸・甲信越</dt>
<dd><a href="/bengo/niigata">新潟</a></dd>
<dd><a href="/bengo/yamanashi">山梨</a></dd>
<dd><a href="/bengo/nagano">長野</a></dd>
<dd><a href="/bengo/ishikawa">石川</a></dd>
<dd><a href="/bengo/toyama">富山</a></dd>
<dd><a href="/bengo/fukui">福井</a></dd>
<dt>東海</dt>
<dd><a href="/bengo/aichi">愛知</a></dd>
<dd><a href="/bengo/shizuoka">静岡</a></dd>
<dd><a href="/bengo/gifu">岐阜</a></dd>
<dd><a href="/bengo/mie">三重</a></dd>
<dt>関西</dt>
<dd><a href="/bengo/osaka">大阪</a></dd>
<dd><a href="/bengo/kyoto">京都</a></dd>
<dd><a href="/bengo/nara">奈良</a></dd>
<dd><a href="/bengo/hyogo">兵庫</a></dd>
<dd><a href="/bengo/shiga">滋賀</a></dd>
<dd><a href="/bengo/wakayama">和歌山</a></dd>
<dt>中国・四国</dt>
<dd><a href="/bengo/hiroshima">広島</a></dd>
<dd><a href="/bengo/okayama">岡山</a></dd>
<dd><a href="/bengo/yamaguchi">山口</a></dd>
<dd><a href="/bengo/tottori">鳥取</a></dd>
<dd><a href="/bengo/shimane">島根</a></dd>
<dd><a href="/bengo/kagawa">香川</a></dd>
<dd><a href="/bengo/tokushima">徳島</a></dd>
<dd><a href="/bengo/ehime">愛媛</a></dd>
<dd><a href="/bengo/kochi">高知</a></dd>
<dt>九州</dt>
<dd><a href="/bengo/fukuoka">福岡</a></dd>
<dd><a href="/bengo/kumamoto">熊本</a></dd>
<dd><a href="/bengo/oita">大分</a></dd>
<dd><a href="/bengo/miyazaki">宮崎</a></dd>
<dd><a href="/bengo/saga">佐賀</a></dd>
<dd><a href="/bengo/nagasaki">長崎</a></dd>
<dd><a href="/bengo/kagoshima">鹿児島</a></dd>
<dd><a href="/bengo/okinawa">沖縄</a></dd>
</dl>
</div>


</div>
<!-- //front--></div>




<div id="wrap" class="bengoarchive">

<h2><span>全国の交通事故問題について知る</span></h2>

<article>

<h3>全国の交通事故 特徴</h3>
<p>全国の交通事故の推移を見てみると、いわゆる「交通戦争」と呼ばれた昭和40年代に多く発生しています。特に昭和45年は昭和時代のピークでした。その後、減少していきますが、平成になるとまた増えていき、平成5年は昭和45年の件数を超えてしまいます。平成16年に過去最高を記録しますが、そこからは減少していきました。負傷者数も同様の推移になっていますが、死亡者数は昭和45年が過去最多となっています。平成に入ってから一時期増加しますが、平成13年からは13年連続で減少しています。</p>
<p>死亡者数がもっとも多いのは愛知県で219人、次いで兵庫県の187人、千葉県の186人が続いています。全国的な傾向としては、歩行中の死亡者数がもっとも多く、また、高齢者の死亡者数が全死亡者数の52.6%と高い割合を占めています。一方、一時期多発した飲酒運転による死亡事故は、罰則の強化や取り締まりなどが功を奏して減少しています。</p>
<p>ただし、交通事故では死亡に至らないケースでも重い後遺障害が残るケースがあります。そういった場合は、慰謝料や休業補償など多くの補償を受けられるようになっています。相手側の提示額に疑問を感じたときは、ためらわずに弁護士事務所で相談してみましょう。</p>

<h3>全国の交通事故傾向</h3>
<h4>発生件数</h4>
<h5>平成16年がピーク</h5>
<p>全国の人身事故の発生件数は統計に物損事故件数を含まなくなった昭和41年以降では、まず昭和45年にピークを迎えました。昭和40年代は「交通戦争」という言葉が生まれた時期で、各地で交通事故が多発しています。全国でも昭和41年は521,481件でしたが、その3年後には20万件近くも増えて720,880件となり、昭和時代では最多となります。昭和46年までは沖縄県のデータは含まれていないのですが、それでもこれだけ多く発生しています。その後は減少傾向になりますが、平成になるとまた増えていきました。平成5年に再び70万件を超え、平成16年は過去最多の952,709件を記録。その後は減少していき、平成26年のデータでは573,465件でピーク時の60％にまで減少しました。</p>

<h4>死傷者数</h4>
<h5>死亡者数は13年連続で減少しています</h5>
<p>昭和41年の負傷者数は517,775人でしたが、昭和45年には981,096人にまで増加し昭和時代のピークを迎えます。その後は減少しますが、平成10年には990,676人となり昭和の最多記録を超えてしまいました。全国でもっとも負傷者数が多かったのは平成16年の1,183,616人で、昭和のピーク時よりさらに20％も多くなっています。その後は年々減少していきました。</p>
<p>死亡者数は昭和45年がピークで16,765人でした。6年後の昭和51年は初めて1万人を下回りますが、昭和63年からはまた1万人を超えていきました。平成8年にようやく1万人を切り、減少していきます。平成14年の死亡者数は8,396人で昭和45年のピーク時の半分になりました。平成21年は4,968人で57年ぶりに4,000人台にまで減少。平成26年は4,113人でピーク時の25％でした。また、1日当たりの死亡者数は約12人で、2時間に1人が交通事故で死亡していることになります。この年の飲酒運転での死亡事故は238件で、これは平成2年以降で最少でした。</p>

<h4>高齢者</h4>
<h5>死亡者数は横ばい、全死亡者に占める割合は増加</h5>
<p>全国の高齢者の交通事故での死亡者数は、平成16年は3,071人でした。その後、緩やかに減少していき、平成26年は2,193人にまで減少しました。ただ、全年齢の死亡者数が大幅に減少しているのに対して、高齢者の死亡者数は横ばいに近い状態です。その結果、全死亡者数のうち、高齢者が占める割合は平成26年は53.3%と過去10年間でもっとも高くなりました。</p>
<p>これは高齢者人口が増えたことも一因であると思われます。日本は今後も高齢化が進むことを考えると、さらに高齢者の交通安全対策が求められるでしょう。</p>

<h4>訴訟件数</h4>
<h5>交通事故の訴訟は10年間で5割増加</h5>
<p>東京地裁での新規の訴訟件数は1,485件（平成22年）で10年前の1.5倍に、大阪地裁は1,091件で5年前の1.2倍に増えているそうです。また、簡易裁判所の控訴事件は5年で約2倍に増えています。中でも交通事故の訴訟件数が10年前より5割も増加しているということです。これにはさまざまな背景がありますが、弁護士の数が増えたことに加えて自動車の任意保険に弁護士費用特約をつける人が増えたことが要因として考えられます。</p>
<p>実際に事故に遭った人の中には、弁護士に相談することで過失割合が見直された、賠償金が増額されたなどの解決例が多く見られます。事故に遭った場合は、まずは交通事故専門の弁護士事務所で相談されることをおすすめします。もちろん、歩行中の事故などで保険に入っていない場合の対応もしてもらえますし、運転中の事故でも任意保険に弁護士費用特約が付いていなくても対応は可能ですので、安心して相談してみましょう。</p>



<h4>弁護士をお探しの方へ</h4>
<p><?php single_cat_title();?>にお住まいで、万が一交通事故問題に巻き込まれてしまったら、<?php single_cat_title();?>に事務所を構える弁護士事務所か、交通事故問題に強い出張可能な弁護士に相談することで示談交渉がスムーズに進み、賠償金（慰謝料）の増額につながることは間違いありません。</p>
<p>まずはお気軽に相談してみてください。</p>
</article>
<?php get_footer();
