<?php
/*
Template Name: calculate
*/
get_header(); ?>



<div class="front bengoform cal">
<div class="inner">
<header>
弁護士基準での損害賠償額の目安が分かる！自動計算シュミレーション
<h1><?php if($_POST):?>弁護士基準での慰謝料計算結果<?php else:?>交通事故「慰謝料 自動計算」<?php endif;?></h1>
</header>

<?php
if(!$_POST):
?>

<article>
<ul class="formtab">
<li><a href="#koui">後遺症の慰謝料</a></li>
<li><a href="#shib">死亡事故の慰謝料</a></li>
</ul>
<p>算出される結果は、あくまで弁護士に依頼した場合の目安の慰謝料です。個々の状況により慰謝料の金額は異なります。</p>
</article>




<article>

<form method="post" action="/calculate?fl=k" enctype="multipart/form-data" id="koui"><fieldset>
<input type="hidden" value="" name="koui_flag" id="koui_flag">
<h2>後遺障害編</h2>


<table class="calc">
<tbody>
<tr>
<th>お住まいの地域</th>
<td><select id="a01" name="a01">
<option value="" selected="selected">選択してください</option>
<optgroup label="北海道・東北地方">
<option value="hokkaido">北海道</option>
<option value="aomori">青森</option>
<option value="iwate">岩手</option>
<option value="akita">秋田</option>
<option value="miyagi">宮城</option>
<option value="yamagata">山形</option>
<option value="fukushima">福島</option>
<optgroup label="関東">
<option value="tokyo">東京</option>
<option value="saitama">埼玉</option>
<option value="kanagawa">神奈川</option>
<option value="chiba">千葉</option>
<option value="ibaraki">茨城</option>
<option value="tochigi">栃木</option>
<option value="gumma">群馬</option>
<optgroup label="北陸・甲信越">
<option value="niigata">新潟</option>
<option value="yamanashi">山梨</option>
<option value="nagano">長野</option>
<option value="ishikawa">石川</option>
<option value="toyama">富山</option>
<option value="fukui">福井</option>
<optgroup label="東海">
<option value="aichi">愛知</option>
<option value="shizuoka">静岡</option>
<option value="gifu">岐阜</option>
<option value="mie">三重</option>
<optgroup label="関西">
<option value="osaka">大阪</option>
<option value="kyoto">京都</option>
<option value="nara">奈良</option>
<option value="hyougo">兵庫</option>
<option value="shiga">滋賀</option>
<option value="wakayama">和歌山</option>
<optgroup label="中国・四国">
<option value="hiroshima">広島</option>
<option value="okayama">岡山</option>
<option value="yamaguchi">山口</option>
<option value="tottori">鳥取</option>
<option value="shimane">島根</option>
<option value="kagawa">香川</option>
<option value="tokushima">徳島</option>
<option value="ehime">愛媛</option>
<option value="kochi">高知</option>
<optgroup label="九州">
<option value="fukuoka">福岡</option>
<option value="kumamoto">熊本</option>
<option value="oita">大分</option>
<option value="miyazaki">宮崎</option>
<option value="saga">佐賀</option>
<option value="nagasaki">長崎</option>
<option value="kagoshima">鹿児島</option>
<option value="okinawa">沖縄</option>
</select></td>
</tr>
<tr>
<th class="th01 must">治療費等</th>
<td class="td01"><input id="f01" name="f01" class="m" value="" type="number"><span>円</span>
<br><span class="hosoku">実費を入力してください。</span>
</td>
</tr>
<tr>
<th class="th02 must">通院交通費</th>
<td class="td02"><input id="f02" name="f02" class="m" value="" type="number"><span>円</span>
<br><span class="hosoku">実費を入力してください。</span>
</td>
</tr>
<tr>
<th class="th03 must">入院雑費</th>
<td class="td03"><samp>--</samp><span>円</span>
<input type="hidden" name="f03" id="f03" value="" />
<div>
入院日数<input id="f03-01" name="f03-01" class="s" value="" type="number"><span>日</span><br />
</div>
<span class="hosoku">入院した日数を入力してください。</span>
</td>
</tr>
<tr>
<th class="th04 must">付添費用</th>
<td class="td04"><samp>--</samp><span>円</span>
<input type="hidden" name="f04" id="f04" value="" />
<div>
通院付添日数<input id="f04-01" class="s" value="" type="number"><span>日</span><br />
入院付添日数<input id="f04-02" class="s" value="" type="number"><span>日</span><br />
</div>
<span class="hosoku">入院・通院の付き添い日数を入力してください。</span>
</td>
</tr>
<tr>
<th class="th05">その他費用</th>
<td class="td05"><input id="f05" name="f05" class="m" value="" type="number"><span>円</span><br />
<span class="hosoku">家屋改造費など、その他費用を入力してください。</span>
</td>
</tr>
<tr>
<th class="th06">休業障害</th>
<td class="td06"><samp>--</samp><span>円</span>
<input type="hidden" name="f06" id="f06" value="" />
<div>
一日当たりの収入<input id="f06-01" class="s" value="" type="number"><span>円</span><br />
休業日数（／日）<input id="f06-02" class="s" value="" type="number"><span>日</span>
</div>
</td>
</tr>

<tr>
<th class="th07 must">入通院慰謝料</th>
<td class="td07"><samp>--</samp><span>円</span>
<input type="hidden" name="f07" id="f07" value="" />
<div>
他覚所見の有無
<select id="f07-01">
<option value="0" selected="selected">他覚所見がある場合</option>
<option value="1">むち打ち症で他覚所見がない場合</option>
</select>
<br />
入院期間<input id="f07-02" class="s" maxlength="4" value="" type="number"><span>日</span><br />
通院期間<input id="f07-03" class="s" maxlength="4" value="" type="number"><span>日</span>
</div>



</td>
</tr>
<tr>
<th class="th08">逸失利益</th>
<td class="td08">
<samp>--</samp><span>円</span>
<input type="hidden" name="f08" id="f08" value="" />

<div>
事故前年度の年収
<select id="f08-01">
<option value="" selected="selected">選択してください</option>
<optgroup label="働いている人">
<option value="5267600">30歳未満（男性）</option>
<option value="3559000">30歳未満（女性）</option>
<option value="0">30歳以上</option>
</optgroup>
<optgroup label="働いていない男性">
<option value="6460200">大学・大学院卒</option>
<option value="4775500">高専・短大卒</option>
<option value="4588900">高校卒</option>
<option value="3883100">中学卒</option>
<option value="3559000">家事従事者</option>
<option value="5267600">年少者</option>
</optgroup>
<optgroup label="働いていない女性">
<option value="4448240">大学・大学院卒</option>
<option value="3830600">高専・短大卒</option>
<option value="2957700">高校卒</option>
<option value="2410100">中学卒</option>
<option value="3559000">家事従事者</option>
<option value="4709300">年少者</option>
</optgroup>
</select>
</div>
<div class="over30" style="display:none;">前年度の収入<input id="f08-01-01" class="s" value="" type="number" /><span>円</span></div>
<div>
後遺障害等級
<select id="f08-02">
<option value="">選択してください</option>
<option value="1">第1級</option>
<option value="1">第2級</option>
<option value="1">第3級</option>
<option value="0.92">第4級</option>
<option value="0.79">第5級</option>
<option value="0.67">第6級</option>
<option value="0.56">第7級</option>
<option value="0.45">第8級</option>
<option value="0.35">第9級</option>
<option value="0.27">第10級</option>
<option value="0.2">第11級</option>
<option value="0.14">第12級</option>
<option value="0.09">第13級</option>
<option value="0.05">第14級</option>
</select>
</div>

<div>
症状固定時の年齢
<select id="f08-03" class="cs" order="col">
<option value="">選択してください</option>
<option value="7.549"> 0</option>
<option value="7.927"> 1</option>
<option value="8.323"> 2</option>
<option value="8.739"> 3</option>
<option value="9.176"> 4</option>
<option value="9.635"> 5</option>
<option value="10.117"> 6</option>
<option value="10.623"> 7</option>
<option value="11.154"> 8</option>
<option value="11.712"> 9</option>
<option value="12.297">10</option>
<option value="12.912">11</option>
<option value="13.558">12</option>
<option value="14.236">13</option>
<option value="14.947">14</option>
<option value="15.695">15</option>
<option value="16.48">16</option>
<option value="17.304">17</option>
<option value="18.169">18</option>
<option value="18.077">19</option>
<option value="17.981">20</option>
<option value="17.88">21</option>
<option value="17.774">22</option>
<option value="17.663">23</option>
<option value="17.546">24</option>
<option value="17.423">25</option>
<option value="17.294">26</option>
<option value="17.159">27</option>
<option value="17.017">28</option>
<option value="16.868">29</option>
<option value="16.711">30</option>
<option value="16.547">31</option>
<option value="16.374">32</option>
<option value="16.193">33</option>
<option value="16.003">34</option>
<option value="15.803">35</option>
<option value="15.593">36</option>
<option value="15.372">37</option>
<option value="15.141">38</option>
<option value="14.898">39</option>
<option value="14.643">40</option>
<option value="14.375">41</option>
<option value="14.094">42</option>
<option value="13.799">43</option>
<option value="13.489">44</option>
<option value="13.163">45</option>
<option value="12.821">46</option>
<option value="12.462">47</option>
<option value="12.085">48</option>
<option value="11.69">49</option>
<option value="11.274">50</option>
<option value="10.838">51</option>
<option value="10.38">52</option>
<option value="9.899">53</option>
<option value="9.899">54</option>
<option value="9.899">55</option>
<option value="9.394">56</option>
<option value="9.394">57</option>
<option value="8.863">58</option>
<option value="8.863">59</option>
<option value="8.863">60</option>
<option value="8.306">61</option>
<option value="8.306">62</option>
<option value="7.722">63</option>
<option value="7.722">64</option>
<option value="7.722">65</option>
<option value="7.108">66</option>
<option value="7.108">67</option>
<option value="6.643">68</option>
<option value="6.643">69</option>
<option value="6.643">70</option>
<option value="5.786">71</option>
<option value="5.786">72</option>
<option value="5.786">73</option>
<option value="5.076">74</option>
<option value="5.076">75</option>
<option value="5.076">76</option>
<option value="4.329">77</option>
<option value="4.329">78</option>
<option value="4.329">79</option>
<option value="4.329">80</option>
<option value="3.546">81</option>
<option value="3.546">82</option>
<option value="3.546">83</option>
<option value="3.546">84</option>
<option value="2.723">85</option>
<option value="2.723">86</option>
<option value="2.723">87</option>
<option value="2.723">88</option>
<option value="2.723">89</option>
<option value="2.723">90</option>
<option value="1.859">91</option>
<option value="1.859">92</option>
<option value="1.859">93</option>
<option value="1.859">94</option>
<option value="1.859">95</option>
<option value="1.859">96</option>
<option value="1.859">97</option>
<option value="1.859">98</option>
<option value="1.859">99</option>
<option value="1.859">100</option>
<option value="0.952">101以上</option>
</select>
</div>



</td>
</tr>
<tr>
<th class="th09 must">後遺症慰謝料</th>
<td class="td09"><samp>--</samp><span>円</span>
<input type="hidden" name="f09" id="f09" value="" />

<div>
後遺障害等級
<select id="f09-01">
<option value="">選択してください</option>
<option value="28000000">第1級</option>
<option value="23700000">第2級</option>
<option value="19900000">第3級</option>
<option value="16700000">第4級</option>
<option value="14000000">第5級</option>
<option value="11800000">第6級</option>
<option value="10000000">第7級</option>
<option value="8300000">第8級</option>
<option value="6900000">第9級</option>
<option value="5500000">第10級</option>
<option value="4200000">第11級</option>
<option value="2900000">第12級</option>
<option value="1800000">第13級</option>
<option value="1100000">第14級</option>
</select>
</div>

</td>
</tr>
<tr>
<th class="th10">将来介護費</th>
<td class="td10"><samp>--</samp><span>円</span>
<input type="hidden" name="f10" id="f10" value="" />

<div>
性別
<select id="f10-01">
  <option value="" selected="selected">選択してください</option>
  <option value="1">男</option>
  <option value="2">女</option>
</select>
</div>

<div>
症状固定時の年齢
<select id="f10-01-01" class="cs" order="col">
<option value="">選択してください</option>
<option value="0"> 0</option>
<option value="1"> 1</option>
<option value="2"> 2</option>
<option value="3"> 3</option>
<option value="4"> 4</option>
<option value="5"> 5</option>
<option value="6"> 6</option>
<option value="7"> 7</option>
<option value="8"> 8</option>
<option value="9"> 9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
<option value="32">32</option>
<option value="33">33</option>
<option value="34">34</option>
<option value="35">35</option>
<option value="36">36</option>
<option value="37">37</option>
<option value="38">38</option>
<option value="39">39</option>
<option value="40">40</option>
<option value="41">41</option>
<option value="42">42</option>
<option value="43">43</option>
<option value="44">44</option>
<option value="45">45</option>
<option value="46">46</option>
<option value="47">47</option>
<option value="48">48</option>
<option value="49">49</option>
<option value="50">50</option>
<option value="51">51</option>
<option value="52">52</option>
<option value="53">53</option>
<option value="54">54</option>
<option value="55">55</option>
<option value="56">56</option>
<option value="57">57</option>
<option value="58">58</option>
<option value="59">59</option>
<option value="60">60</option>
<option value="61">61</option>
<option value="62">62</option>
<option value="63">63</option>
<option value="64">64</option>
<option value="65">65</option>
<option value="66">66</option>
<option value="67">67</option>
<option value="68">68</option>
<option value="69">69</option>
<option value="70">70</option>
<option value="71">71</option>
<option value="72">72</option>
<option value="73">73</option>
<option value="74">74</option>
<option value="75">75</option>
<option value="76">76</option>
<option value="77">77</option>
<option value="78">78</option>
<option value="79">79</option>
<option value="80">80</option>
<option value="81">81</option>
<option value="82">82</option>
<option value="83">83</option>
<option value="84">84</option>
<option value="85">85</option>
<option value="86">86</option>
<option value="87">87</option>
<option value="88">88</option>
<option value="89">89</option>
<option value="90">90</option>
<option value="91">91</option>
<option value="92">92</option>
<option value="93">93</option>
<option value="94">94</option>
<option value="95">95</option>
<option value="96">96</option>
<option value="97">97</option>
<option value="98">98</option>
<option value="99">99</option>
<option value="100">100以上</option>
</select>
</div>

<div>
介護の種類
<select id="f10-02">
  <option value="" selected="selected">選択してください</option>
  <optgroup label="自宅での介護">
  <option value="1">自宅での近親者介護</option>
  <option value="2">自宅での職業介護人</option>
  </optgroup>
  <option value="3">施設での介護</option>
</select>
</div>

<div class="price p2" style="display:none">
一日あたりの介護費用
<input id="f10-03" class="m" value="" type="number"><span>円</span>
</div>
<div class="price p3" style="display:none">
1ヶ月の施設利用料
<input id="f10-04" class="m" value="" type="number"><span>円</span>
</div>
<input id="f10-05" class="m" value="" type="hidden">
<input id="f10-06" class="m" value="" type="hidden">
<span class="hosoku">症状固定後に介護が必要な場合、入力してください。</span>
</td>
</tr>
<tr>
<th class="th11">過失割合</th>
<td class="td11">
<select id="f11" name="f11">
<option value="0" selected="selected">0</option>
<option value="5">5</option>
<option value="10">10</option>
<option value="15">15</option>
<option value="20">20</option>
<option value="25">25</option>
<option value="30">30</option>
<option value="35">35</option>
<option value="40">40</option>
<option value="45">45</option>
<option value="50">50</option>
<option value="55">55</option>
<option value="60">60</option>
<option value="65">65</option>
<option value="70">70</option>
<option value="75">75</option>
<option value="80">80</option>
<option value="85">85</option>
<option value="90">90</option>
<option value="95">95</option>
<option value="100">100</option>
</select>
<span>%</span><br />
<span class="hosoku">ご自分の過失割合を選択してください。</span>
</td>

</tr>
<tr>
<th class="th12">既払額</th>
<td class="td12"><input id="f12" name="f12" class="m" value="" type="number"><span>円</span><br />
<span class="hosoku">すでに支払われた金額がある場合は、入力してください。</span>
</td>
</tr>
</tr>
<tr>
<th class="th13">保険会社の提示額</th>
<td class="td13"><input id="f13" name="f13" class="m" value="" type="number"><span>円</span></td>
</tr>

</tbody></table>

<div id="button">
		<input name="cal_submit2" id="cal_submit2" value="慰謝料を計算する" type="submit">
	</div>
</fieldset>
</form>






<form method="post" action="/calculate?fl=s" enctype="multipart/form-data" id="shib" style="display: none"><fieldset>
<input type="hidden" value="" name="shib_flag" id="shib_flag">

<h2>死亡事故編</h2>


<table class="calc">
<tbody>
<tr>
<th>お住まいの地域</th>
<td><select id="a15" name="a15">
<option value="" selected="selected">選択してください</option>
<optgroup label="北海道・東北地方">
<option value="hokkaido">北海道</option>
<option value="aomori">青森</option>
<option value="iwate">岩手</option>
<option value="akita">秋田</option>
<option value="miyagi">宮城</option>
<option value="yamagata">山形</option>
<option value="fukushima">福島</option>
<optgroup label="関東">
<option value="tokyo">東京</option>
<option value="saitama">埼玉</option>
<option value="kanagawa">神奈川</option>
<option value="chiba">千葉</option>
<option value="ibaraki">茨城</option>
<option value="tochigi">栃木</option>
<option value="gumma">群馬</option>
<optgroup label="北陸・甲信越">
<option value="niigata">新潟</option>
<option value="yamanashi">山梨</option>
<option value="nagano">長野</option>
<option value="ishikawa">石川</option>
<option value="toyama">富山</option>
<option value="fukui">福井</option>
<optgroup label="東海">
<option value="aichi">愛知</option>
<option value="shizuoka">静岡</option>
<option value="gifu">岐阜</option>
<option value="mie">三重</option>
<optgroup label="関西">
<option value="osaka">大阪</option>
<option value="kyoto">京都</option>
<option value="nara">奈良</option>
<option value="hyougo">兵庫</option>
<option value="shiga">滋賀</option>
<option value="wakayama">和歌山</option>
<optgroup label="中国・四国">
<option value="hiroshima">広島</option>
<option value="okayama">岡山</option>
<option value="yamaguchi">山口</option>
<option value="tottori">鳥取</option>
<option value="shimane">島根</option>
<option value="kagawa">香川</option>
<option value="tokushima">徳島</option>
<option value="ehime">愛媛</option>
<option value="kochi">高知</option>
<optgroup label="九州">
<option value="fukuoka">福岡</option>
<option value="kumamoto">熊本</option>
<option value="oita">大分</option>
<option value="miyazaki">宮崎</option>
<option value="saga">佐賀</option>
<option value="nagasaki">長崎</option>
<option value="kagoshima">鹿児島</option>
<option value="okinawa">沖縄</option>
</select></td>
</tr>
<tr>
<th class="th16 must">治療費等</th>
<td class="td16"><input id="f16" name="f16" class="m" value="" type="number"><span>円</span>
<br /><span class="hosoku">実費を入力してください。</span>
</td>
</tr>
<tr>
<th class="th17 must">通院交通費</th>
<td class="td17"><input id="f17" name="f17" class="m" value="" type="number"><span>円</span>
<br /><span class="hosoku">実費を入力してください。</span>
</td>
</tr>
<tr>
<th class="th18 must">入院雑費</th>
<td class="td18"><samp>--</samp><span>円</span>
<input type="hidden" name="f18" id="f18" value="" />
<div>
入院日数<input id="f18-01" name="f18-01" class="s" value="" type="number"><span>日</span><br />
</div>
</td>
</tr>
<tr>
<th class="th19 must">付添費用</th>
<td class="td19"><samp>--</samp><span>円</span>
<input type="hidden" name="f19" id="f19" value="" />
<div>
通院付添日数<input id="f19-01" class="s" value="" type="number"><span>日</span><br />
入院付添日数<input id="f19-02" class="s" value="" type="number"><span>日</span>
</div>
<span class="hosoku">入院・通院の付き添い日数を入力してください。</span>
</td>
</tr>
<tr>
<th class="th20">その他費用</th>
<td class="td20"><input id="f20" name="f20" class="m" value="" type="number"><span>円</span>
<br />
<span class="hosoku">家屋改造費など、その他費用を入力してください。</span>
</td>
</tr>
<tr>
<th class="th21">休業障害</th>
<td class="td21"><samp>--</samp><span>円</span>
<input type="hidden" name="f21" id="f21" value="" />
<div>
一日当たりの収入<input id="f21-01" class="s" value="" type="number"><span>円</span><br />
休業日数（／日）<input id="f21-02" class="s" value="" type="number"><span>日</span>
</div>
</td>
</tr>

<tr>
<th class="th22 must">葬儀費用</th>
<td class="td22"><samp>1,500,000</samp><span>円</span>
<input type="hidden" name="f22" id="f22" value="1500000" />
</td>
</tr>
<tr>
<th class="th23">逸失利益</th>
<td class="td23">
<samp>--</samp><span>円</span>
<input type="hidden" name="f23" id="f23" value="" />

<div>
事故前年度の年収
<select id="f23-01">
<option value="" selected="selected">選択してください</option>
<optgroup label="働いている人">
<option value="5267600">30歳未満（男性）</option>
<option value="3559000">30歳未満（女性）</option>
<option value="0">30歳以上</option>
</optgroup>
<optgroup label="働いていない男性">
<option value="6460200">大学・大学院卒</option>
<option value="4775500">高専・短大卒</option>
<option value="4588900">高校卒</option>
<option value="3883100">中学卒</option>
<option value="3559000">家事従事者</option>
<option value="5267600">年少者</option>
<option value="1">年金生活者</option>
</optgroup>
<optgroup label="働いていない女性">
<option value="4448240">大学・大学院卒</option>
<option value="3830600">高専・短大卒</option>
<option value="2957700">高校卒</option>
<option value="2410100">中学卒</option>
<option value="3559000">家事従事者</option>
<option value="4709300">年少者</option>
<option value="2">年金生活者</option>
</optgroup>
</select>
</div>
<div class="over30" style="display:none;">前年度の収入<input id="f23-01-01" class="s" value="" type="number" /><span>円</span></div>
<div>
生活費控除率
<select id="f23-02">
<option value="">選択してください</option>
<option value="0.6" class="m">男性（被扶養者１人）</option>
<option value="0.7" class="m">男性（被扶養者2人以上）</option>
<option value="0.5" class="m">男性（独身・幼児・高齢者）</option>
<option value="0.7" class="f">女性</option>
</select>
<select id="f23-02-01" style="display:none">
<option value="">選択してください</option>
<option value="1" class="d">男性</option>
<option value="2" class="d">女性</option>
</select>
</div>

<div>
症状固定時の年齢
<select id="f23-03" class="cs" order="col">
<option value="">選択してください</option>
<option value="7.549">0</option>
<option value="7.927">1</option>
<option value="8.323">2</option>
<option value="8.739">3</option>
<option value="9.176">4</option>
<option value="9.635">5</option>
<option value="10.117">6</option>
<option value="10.623">7</option>
<option value="11.154">8</option>
<option value="11.712">9</option>
<option value="12.297">10</option>
<option value="12.912">11</option>
<option value="13.558">12</option>
<option value="14.236">13</option>
<option value="14.947">14</option>
<option value="15.695">15</option>
<option value="16.48">16</option>
<option value="17.304">17</option>
<option value="18.169">18</option>
<option value="18.077">19</option>
<option value="17.981">20</option>
<option value="17.88">21</option>
<option value="17.774">22</option>
<option value="17.663">23</option>
<option value="17.546">24</option>
<option value="17.423">25</option>
<option value="17.294">26</option>
<option value="17.159">27</option>
<option value="17.017">28</option>
<option value="16.868">29</option>
<option value="16.711">30</option>
<option value="16.547">31</option>
<option value="16.374">32</option>
<option value="16.193">33</option>
<option value="16.003">34</option>
<option value="15.803">35</option>
<option value="15.593">36</option>
<option value="15.372">37</option>
<option value="15.141">38</option>
<option value="14.898">39</option>
<option value="14.643">40</option>
<option value="14.375">41</option>
<option value="14.094">42</option>
<option value="13.799">43</option>
<option value="13.489">44</option>
<option value="13.163">45</option>
<option value="12.821">46</option>
<option value="12.462">47</option>
<option value="12.085">48</option>
<option value="11.69">49</option>
<option value="11.274">50</option>
<option value="10.838">51</option>
<option value="10.38">52</option>
<option value="9.899">53</option>
<option value="9.899">54</option>
<option value="9.899">55</option>
<option value="9.394">56</option>
<option value="9.394">57</option>
<option value="8.863">58</option>
<option value="8.863">59</option>
<option value="8.863">60</option>
<option value="8.306">61</option>
<option value="8.306">62</option>
<option value="7.722">63</option>
<option value="7.722">64</option>
<option value="7.722">65</option>
<option value="7.108">66</option>
<option value="7.108">67</option>
<option value="6.643">68</option>
<option value="6.643">69</option>
<option value="6.643">70</option>
<option value="5.786">71</option>
<option value="5.786">72</option>
<option value="5.786">73</option>
<option value="5.076">74</option>
<option value="5.076">75</option>
<option value="5.076">76</option>
<option value="4.329">77</option>
<option value="4.329">78</option>
<option value="4.329">79</option>
<option value="4.329">80</option>
<option value="3.546">81</option>
<option value="3.546">82</option>
<option value="3.546">83</option>
<option value="3.546">84</option>
<option value="2.723">85</option>
<option value="2.723">86</option>
<option value="2.723">87</option>
<option value="2.723">88</option>
<option value="2.723">89</option>
<option value="2.723">90</option>
<option value="1.859">91</option>
<option value="1.859">92</option>
<option value="1.859">93</option>
<option value="1.859">94</option>
<option value="1.859">95</option>
<option value="1.859">96</option>
<option value="1.859">97</option>
<option value="1.859">98</option>
<option value="1.859">99</option>
<option value="1.859">100</option>
<option value="0.952">101以上</option>
</select>
</div>

<div class="nenkin" style="display:none;">受給年金額(年間)<input id="f23-04" name="f23-04" class="m" value="" type="number"><span>円</span></div>

</td>
</tr>
<tr>
<th class="th24 must">死亡慰謝料</th>
<td class="td24"><samp>--</samp><span>円</span>
<input type="hidden" name="f24" id="f24" value="" />

<div>
<select id="f24-01">
<option value="">選択してください</option>
<option value="28000000">一家の支柱</option>
<option value="24000000">母親・配偶者</option>
<option value="20000000">その他</option>
</select>
</div>

</td>
</tr>

<tr>
<th class="td25">過失割合</th>
<td>
<select id="f25" name="f25">
<option value="0" selected="selected">0</option>
<option value="5">5</option>
<option value="10">10</option>
<option value="15">15</option>
<option value="20">20</option>
<option value="25">25</option>
<option value="30">30</option>
<option value="35">35</option>
<option value="40">40</option>
<option value="45">45</option>
<option value="50">50</option>
<option value="55">55</option>
<option value="60">60</option>
<option value="65">65</option>
<option value="70">70</option>
<option value="75">75</option>
<option value="80">80</option>
<option value="85">85</option>
<option value="90">90</option>
<option value="95">95</option>
<option value="100">100</option>
</select>
<span>%</span>
<br /><span class="hosoku">ご自分の過失割合を選択してください。</span>
</td>

</tr>
<tr>
<th class="td26">既払額</th>
<td class="td26"><input id="f26" name="f26" class="m" value="" type="number"><span>円</span>
<br /><span class="hosoku">すでに支払われた金額がある場合は、入力してください。</span>
</td>
</tr>
<tr>
<th class="td13">保険会社の提示額</th>
<td class="td13"><input id="f13" name="f13" class="m" value="" type="number"><span>円</span></td>
</tr>

</tbody></table>



<div id="button">
		<input name="cal_submit2" id="cal_submit2" value="慰謝料を計算する" type="submit">
	</div>
</fieldset>
</form>

</article>


<?php else:
//print_r($_POST);

if($_POST['shib_flag']):
	//if($_POST['f25'] == 0) $_POST['f25'] = 100;
	if($_POST['f26']) $shiharai = number_format($_POST['f26']);
	$mathall = ($_POST['f16'] + $_POST['f17'] + $_POST['f18'] + $_POST['f19'] + $_POST['f20'] + $_POST['f21'] + $_POST['f22'] + $_POST['f23'] + $_POST['f24']) * (1 - $_POST['f25']/100);
	$jian = 'jian_shibo';
	$query_tmp = $wpdb->prepare(" AND meta_value LIKE %s" , '%'. like_escape($jian). '%');
elseif($_POST['koui_flag']):
	//if($_POST['f11'] == 0) $_POST['f11'] = 100;
	if($_POST['f12']) $shiharai = number_format($_POST['f12']);
	$mathall = ($_POST['f01'] + $_POST['f02'] + $_POST['f03'] + $_POST['f04'] + $_POST['f05'] + $_POST['f06'] + $_POST['f07'] + $_POST['f08'] + $_POST['f09'] + $_POST['f10']) * (1 - $_POST['f11']/100);
	if($_POST['f09']){
	$jian = 'jian_kouisyo';
	}else{
	$jian = 'jian_isya';
	}
	$query_tmp = $wpdb->prepare(" AND meta_value LIKE %s" , '%'. like_escape($jian). '%');
endif;
?>

<article id="total">
<p>自動計算シミュレーションは、弁護士基準での一般的な計算方法を用いています。<br />
実際の慰謝料額は、個別の事情により異なりますので、より正確に知りたい方は個別にご相談ください。</p>

<?php if(!empty($_POST['f09'])||$_POST['f24']):?>
<h2>計算の結果、弁護士に依頼することで慰謝料増額の可能性が高いです。</h2>
<?php else:  ?>
<h2>後遺障害の等級認定を受けないと大幅増額の可能性は低いです。</h2>
<?php endif;?>

<div class="is">
<h4>【計算結果】弁護士基準での慰謝料目安</h4>
<p><samp><?php echo number_format($mathall);?></samp>円</p>
<!-- //is--></div>
<p>※計算結果はあくまで目安金額となります。</p>

<?php if(!empty($_POST['f13'])):?>
<h3>弁護士に依頼すると<b><?php echo floor($mathall / $_POST['f13']);?>倍以上増額</b>する可能性があります。</h3>

<table>
<tr>
<th>保険会社の提示額</th>
<td><strong><?php echo number_format($_POST['f13']);?></strong>円</td>
</tr>
<?php if($shiharai):?>
<tr>
<th>既に支払われた金額</th>
<td><strong><?php echo $shiharai;?></strong>円</td>
</tr>
<?php endif;?>
<tr>
<th>弁護士に依頼すると</th>
<td>保険会社の提示額より<strong><?php echo number_format($mathall - $_POST['f13']);?></strong>円増額する可能性があります。</td>
</tr>
</table>
<?php endif;?>

</article>
<div class="j">

<p>
<?php if($_POST['a01']||$_POST['a15']):
if($_POST['a01']) $areaids = $_POST['a01'];
if($_POST['a15']) $areaids = $_POST['a15'];
switch ($areaids){
case 'hokkaido':
  echo '北海道';
  break;
case 'aomori':
  echo '青森県';
  break;
case 'iwate':
  echo '岩手県';
  break;
case 'akita':
  echo '秋田県';
  break;
case 'miyagi':
  echo '宮城県';
  break;
case 'yamagata':
  echo '山形県';
  break;
case 'fukushima':
  echo '福島県';
  break;
case 'tokyo':
  echo '東京都';
  break;
case 'saitama':
  echo '埼玉県';
  break;
case 'kanagawa':
  echo '神奈川県';
  break;
case 'chiba':
  echo '千葉県';
  break;
case 'ibaraki':
  echo '茨城県';
  break;
case 'tochigi':
  echo '栃木県';
  break;
case 'gumma':
  echo '群馬県';
  break;
case 'niigata':
  echo '新潟県';
  break;
case 'yamanashi':
  echo '山梨県';
  break;
case 'nagano':
  echo '長野県';
  break;
case 'ishikawa':
  echo '石川県';
  break;
case 'toyama':
  echo '富山県';
  break;
case 'fukui':
  echo '福井県';
  break;
case 'aichi':
  echo '愛知県';
  break;
case 'shizuoka':
  echo '静岡県';
  break;
case 'gifu':
  echo '岐阜県';
  break;
case 'mie':
  echo '三重県';
  break;
case 'osaka':
  echo '大阪府';
  break;
case 'kyoto':
  echo '京都府';
  break;
case 'nara':
  echo '奈良県';
  break;
case 'hyougo':
  echo '兵庫県';
  break;
case 'shiga':
  echo '滋賀県';
  break;
case 'wakayama':
  echo '和歌山県';
  break;
case 'hiroshima':
  echo '広島県';
  break;
case 'okayama':
  echo '岡山県';
  break;
case 'yamaguchi':
  echo '山口県';
  break;
case 'tottori':
  echo '鳥取県';
  break;
case 'shimane':
  echo '島根県';
  break;
case 'kagawa':
  echo '香川県';
  break;
case 'tokushima':
  echo '徳島県';
  break;
case 'ehime':
  echo '愛媛県';
  break;
case 'kochi':
  echo '高知県';
  break;
case 'fukuoka':
  echo '福岡県';
  break;
case 'kumamoto':
  echo '熊本県';
  break;
case 'oita':
  echo '大分県';
  break;
case 'miyazaki':
  echo '宮崎県';
  break;
case 'saga':
  echo '佐賀県';
  break;
case 'nagasaki':
  echo '長崎県';
  break;
case 'kagoshima':
  echo '鹿児島県';
  break;
case 'okinawa':
  echo '沖縄県';
  break;
default:
}
echo 'で';
endif;
?>
<?php if($_POST['f24']):?>死亡事故<? elseif(empty($_POST['f09'])):?>慰謝料<?php else:?>後遺障害認定<?php if($_POST['f09']):
if($_POST['f09']) $shogainintei = $_POST['f09'];
switch ($shogainintei){
case '28000000':
  echo '第1級';
  break;
case '23700000':
  echo '第2級';
  break;
case '19900000':
  echo '第3級';
  break;
case '16700000':
  echo '第4級';
  break;
case '14000000':
  echo '第5級';
  break;
case '11800000':
  echo '第6級';
  break;
case '10000000':
  echo '第7級';
  break;
case '8300000':
  echo '第8級';
  break;
case '6900000':
  echo '第9級';
  break;
case '5500000':
  echo '第10級';
  break;
case '4200000':
  echo '第11級';
  break;
case '2900000':
  echo '第12級';
  break;
case '1800000':
  echo '第13級';
  break;
case '1100000':
  echo '第14級';
  break;

default:
}
endif;
?>
<?php endif;?>
に強い弁護士を探す！</p>

</div>


<?php endif;?>



<!-- //inner--></div>
<!-- //front--></div>

<div class="front bengoarchive">
<div class="inner">


<?php

$args = array(
    'post_type' => 'bengo',
    'order' => 'ASC',
    'posts_per_page' => 10,
    //'paged' => $paged
);



$term_slug = $areaids;
/*
if($term_slug):
$ret = $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE meta_key = 'bengo_order' AND meta_value LIKE %s" , '%'. like_escape($term_slug). '%') . $query_tmp);
	if($status){ $order_key2 = $status; }else{ $order_key2 = $jian; }
	$order_key = $term_slug;
elseif($query_tmp):
$ret = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE meta_key = 'bengo_order'" . $query_tmp);
	if($status){ $order_key2 = $status; }else{ $order_key2 = $jian; }
	$order_key = 'zenkoku';
endif;

//print_r($query_tmp);
//print_r($ret);
//print_r($order_key);

if($order_key||$order_key2):
	foreach($ret as $orders):
	$sss = json_decode($orders->meta_value, true);
	$ooo[$orders->post_id] = $sss[$order_key] + $sss[$order_key2];
	endforeach;
endif;
*/

if(! $term_slug){
  $term_slug = 'zenkoku';
}
if($status){
  $bengo_order_jian = $status;
} elseif ($jian) {
  $bengo_order_jian = $jian;
} else {
  $bengo_order_jian = null;
}

$ret = $wpdb->get_results(TBO::getQueryOfPostsByBengoOrderTypeOrdered($term_slug, $bengo_order_jian));

foreach($ret as $orders):
$sss = json_decode($orders->meta_value, true);
if ($status){
  $sort = $orders->sort + $sss[$status];
} elseif ($jian) {
  $sort = $orders->sort + $sss[$jian];
} else {
  $sort = $orders->sort;
}
$ooo[$orders->ID] = $sort;
endforeach;

//print_r($ooo);
if(is_array($ooo)){
arsort($ooo);
$args['post__in'] = array_keys($ooo);
$args['orderby'] = 'post__in';
} else {
//$args['orderby'] = 'ID';
}


$the_query = new WP_Query( $args );

?>
<!--<?php //print_r($query_tmp);?>-->



<article id="bengoshi">
<ul class="list">


<?php
if($the_query->have_posts() && $args['post__in']):
while ( $the_query->have_posts() ) : $the_query->the_post();?>

<li>
<p><span class="area">対応：<?php echo get_post_meta($post->ID , 'bengo_area', TRUE);?></span>
<?php if(get_post_meta($post->ID , 'shozai_area', TRUE)): ?><span class="shozai">事務所：<?php echo get_post_meta($post->ID , 'shozai_area', TRUE);?></span><?php endif; ?></p>
<h2><?php echo get_post_meta($post->ID , 'bengo_catch', TRUE);?></h2>
<div class="bengo_image">
<a href="<?php the_permalink();?>"><?php echo wp_get_attachment_image(post_custom('File Upload'), 'medium', false, array('class' => 'visual', 'alt' => get_the_title()));?></a><div class="helpline">
<strong>お電話での無料相談窓口</strong>
<a href="<?php the_permalink(); ?>?pid=<?php echo $post->ID?>" rel="nofollow"><?php echo get_post_meta($post->ID , 'bengo_tel', TRUE);?></a>
<table>
<tr>
<th>受付時間</th>
<td><?php echo get_post_meta($post->ID , 'bengo_open', TRUE);?></td>
</tr>
</table>
</div>
</div>


<div class="bengo_detail">
<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
<table>
<tr>
<td><em>相談料</em><?php echo get_post_meta($post->ID , 'bengo_price', TRUE);?></td>
<td><em>着手金</em><?php echo nl2br(get_post_meta($post->ID , 'bengo_start', TRUE));?></td>
<td><em>報酬金</em><?php echo nl2br(get_post_meta($post->ID , 'bengo_success', TRUE));?></td>
</tr>
</table>
<div class="sub">
<ul>
<?php $tmp_status = get_post_meta($post->ID , 'bengo_status', FALSE);?>
<?php $tmp_jian = get_post_meta($post->ID , 'bengo_jian', FALSE);?>
<li<?php if(!in_array('area' ,$tmp_status)) echo ' class="n"';?>>全国対応</li>
<li<?php if(!in_array('pps' ,$tmp_status)) echo ' class="n"';?>>後払い可</li>
<li<?php if(!in_array('tokuyaku' ,$tmp_status)) echo ' class="n"';?>>弁護特約</li>
<li<?php if(!in_array('jian_donichi' ,$tmp_jian)) echo ' class="n"';?>>土日対応</li>
</ul>

<h4>料金体系</h4>
<p><?php echo nl2br(get_post_meta($post->ID , 'bengo_ryokin', TRUE));?></p>
</div>
<div class="view">
<aside>
<h5>インターネットから相談</h5>
<a href="/bengo_contact?post_id=<?php the_ID();?>" rel="nofollow"><img src="<?php bloginfo('template_directory'); ?>/img/btn_s_freeform.png" alt="無料相談フォーム" class="btn_form" /></a>
<p>※無料相談フォームは24時間365日受付</p>
</aside>
<a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/btn_s_detail.png" alt="詳細情報を見る" class="btn_detail"  /></a>
</div>

</div>




</li>
<?php endwhile;?>
<?php else:?>
<?php /* ◆◆◆◆◆◆◆◆◆◆◆ */ ?><li><h2>計算後、最も適した弁護士事務所が表示されます。</h2></li>
<?php endif;?>
</ul>
</article>



<?php wp_reset_query();?>


<!-- //inner--></div>
<!-- //front--></div>

<?php get_footer();
