<?php get_header(); ?>

<?php $cat_title = single_cat_title( '', FALSE ); ?>
<div class="front bengoarchive">
<div class="inner">
<header>
<?php
if(isset($_GET['jian'])){

  if($_GET['jian'] == "jian_isya"){
    $jian = "「慰謝料」に強い弁護士";
  }elseif($_GET['jian'] == "jian_songai"){
    $jian = "「損害賠償」に強い弁護士";
  }elseif($_GET['jian'] == "jian_jidan"){
    $jian = "「示談交渉」に強い弁護士";
  }elseif($_GET['jian'] == "jian_kashitsu"){
    $jian = "「過失割合」に強い弁護士";
  }elseif($_GET['jian'] == "jian_busson"){
    $jian = "「物損事故」に強い弁護士";
  }elseif($_GET['jian'] == "jian_jinshin"){
    $jian = "「人身事故」に強い弁護士";
  }elseif($_GET['jian'] == "jian_shibo"){
    $jian = "「死亡事故」に強い弁護士";
  }elseif($_GET['jian'] == "jian_kouisyo"){
    $jian = "「後遺障害」に強い弁護士";
  }elseif($_GET['jian'] == "jian_donichi"){
    $jian = "「土日対応」可能な弁護士";
  }elseif($_GET['jian'] == "jian_24h"){
    $jian = "「24時間電話」可能な弁護士";
  }else{
    $jian = "交通事故に強い弁護士";
  }

}elseif(isset($_GET['status'])){
  if($_GET['status'] == "tyakusyu"){
    $jian = "「着手金0円」の弁護士";
  }elseif($_GET['status'] == "free"){
    $jian = "「相談料0円」の弁護士";
  }
}else{
  $jian = "交通事故に強い弁護士";
}
?>
<span>【厳選掲載】交通事故問題に強い弁護士ランキング</span>
<h1><?php echo $cat_title;?>で<?php echo $jian; ?></h1>
</header>
<div class="check">
<ul>
<?php
$jian = htmlspecialchars($_GET['jian']);
$status = htmlspecialchars($_GET['status']);
//if($jian) $query_tmp = $wpdb->prepare(" AND meta_value LIKE %s" , '%'. like_escape($jian). '%');
//if($status) $query_tmp = $wpdb->prepare(" AND meta_value LIKE %s" , '%'. like_escape($status). '%');
if ($jian != "") {
  $bengo_order_jian = $jian;
} elseif ($status != "") {
  $bengo_order_jian = $status;
} else {
  $bengo_order_jian = null;
}

?>
<li<?php if($jian == 'jian_isya') echo' class="r"';?>><a href="<?php if($paged):?>..<?php endif;?>?jian=jian_isya"><em>慰謝料</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_songai') echo' class="r"';?>><a href="<?php if($paged):?>..<?php endif;?>?jian=jian_songai"><em>損害賠償</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_jidan') echo' class="r"';?>><a href="<?php if($paged):?>..<?php endif;?>?jian=jian_jidan"><em>示談交渉</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_kashitsu') echo' class="r"';?>><a href="<?php if($paged):?>..<?php endif;?>?jian=jian_kashitsu"><em>過失割合</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_busson') echo' class="r"';?>><a href="<?php if($paged):?>..<?php endif;?>?jian=jian_busson"><em>物損事故</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_jinshin') echo' class="r"';?>><a href="<?php if($paged):?>..<?php endif;?>?jian=jian_jinshin"><em>人身事故</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_shibo') echo' class="r"';?>><a href="<?php if($paged):?>..<?php endif;?>?jian=jian_shibo"><em>死亡事故</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_kouisyo') echo' class="r"';?>><a href="<?php if($paged):?>..<?php endif;?>?jian=jian_kouisyo"><em>後遺障害</em>に強い弁護士</a></li>
<li<?php if($jian == 'jian_donichi') echo' class="r"';?>><a href="<?php if($paged):?>..<?php endif;?>?jian=jian_donichi"><em>土日対応</em>可能な弁護士</a></li>
<li<?php if($jian == 'jian_24h') echo' class="r"';?>><a href="<?php if($paged):?>..<?php endif;?>?jian=jian_24h"><em>24H電話</em>可能な弁護士</a></li>
<li<?php if($status == 'tyakusyu') echo' class="r"';?>><a href="<?php if($paged):?>..<?php endif;?>?status=tyakusyu"><em>着手金0円</em>の弁護士</a></li>
<li<?php if($status == 'free') echo' class="r"';?>><a href="<?php if($paged):?>..<?php endif;?>?status=free"><em>相談料0円</em>の弁護士</a></li>
</ul>
</div>



<?php
$term_slug =get_query_var('bengo_cat');
$cats = get_term_by( 'slug' , $term_slug, 'bengo_cat' );

$args = array(
    'post_type' => 'bengo',
    'order' => 'DESC',
/*    'meta_key' => 'bengo_order',
    'meta_query' => array(
    	'relation'=>'AND',
    	array('relation'=>'OR',
	    	array( 'key' => 'bengo_taiou', 'value' => 'japan', 'compare' => 'IN' ),
	    	array( 'key' => 'bengo_taiou', 'value' => $term_slug, 'compare' => 'IN' )
    	),
    	$query_tmp,
    ),*/
    //'posts_per_page' => 10,
    'paged' => $paged
);



//$ret =$wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE meta_key = 'bengo_order' AND meta_value LIKE %s" , '%'. like_escape($term_slug). '%') . $query_tmp);?>

<!--<?php
/*
if($query_tmp):
 if($status){
  $order_key2 = $status;
 }else{
  $order_key2 = $jian;
 }
 $order_key = $term_slug;
else:
 $order_key = $term_slug;
endif;
foreach($ret as $orders):
$sss = json_decode($orders->meta_value, true);
$ooo[$orders->post_id] = $sss[$order_key] + $sss[$order_key2];
endforeach;
*/

// 弁護士表示区分「関西ランキング」の記事リスト取得
$ret = $wpdb->get_results(TBO::getQueryOfPostsByBengoOrderTypeOrdered($term_slug, $bengo_order_jian));

foreach($ret as $orders):
$sss = json_decode($orders->meta_value, true);
//$ooo[$orders->post_id] = $sss[$order_key] + $sss[$order_key2];
if ($status){
  $sort = $orders->sort + $sss[$status];
} elseif ($jian) {
  $sort = $orders->sort + $sss[$jian];
} else {
  $sort = $orders->sort;
}
$ooo[$orders->ID] = $sort;
endforeach;

if(is_array($ooo)){
arsort($ooo);
var_dump($ooo);
$args['post__in'] = array_keys($ooo);
} else {
}
$args['orderby'] = 'post__in';

$the_query = new WP_Query( $args );
//print_r($the_query);

?>-->



<nav class="pager">
<?php if($the_query->have_posts() && $args['post__in']): ?>
<?php custum_pagination($the_query->max_num_pages); ?>
<div class="count"><?php my_result_count_tax($the_query->found_posts, $the_query->post_count);?></div>
<?php endif;?>
</nav>


<article>
<ul class="list">


<?php
if($the_query->have_posts() && $args['post__in']):
while ( $the_query->have_posts() ) : $the_query->the_post();?>

<li<?php if(mb_strlen(get_the_title()) > 20):?> class="ll<?php if(get_post_meta($post->ID , 'osusume_flag', FALSE)):?> osusume<?php endif;?>"<?php endif;?><?php if(get_post_meta($post->ID , 'osusume_flag', FALSE)):?> class="osusume"<?php endif;?>>

<?php /* ◆◆◆◆◆  順位調整時に確認したい数値（ログイン時のみしか表示されません）  ◆◆◆◆◆◆ */
 if ( is_user_logged_in() ) : ?><?php $jsond = json_decode(get_post_meta($post->ID , 'bengo_order', TRUE));echo $jsond->$term_slug;?>　<?php edit_post_link('Edit','[',']'); ?><?php endif;?>

<p><span class="area">対応：<?php echo get_post_meta($post->ID , 'bengo_area', TRUE);?></span>
<?php if(get_post_meta($post->ID , 'shozai_area', TRUE)): ?><span class="shozai">事務所：<?php echo get_post_meta($post->ID , 'shozai_area', TRUE);?></span><?php endif; ?></p>
<h2><?php echo get_post_meta($post->ID , 'bengo_catch', TRUE);?></h2>
<div class="bengo_image">
<a href="<?php the_permalink();?>"><?php echo wp_get_attachment_image(post_custom('File Upload'), 'medium', false, array('class' => 'visual', 'alt' => get_the_title()));?></a><div class="helpline">
<strong>お電話での相談窓口【通話無料】</strong>
<a href="<?php the_permalink(); ?>?pid=<?php echo $post->ID?>" rel="nofollow"><?php echo get_post_meta($post->ID , 'bengo_tel', TRUE);?></a>
<table>
<tr>
<th>受付時間</th>
<td><?php echo get_post_meta($post->ID , 'bengo_open', TRUE);?></td>
</tr>
</table>
</div>
</div>


<div class="bengo_detail">
<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
<table>
<tr>
<td><em>相談料</em><?php echo get_post_meta($post->ID , 'bengo_price', TRUE);?></td>
<td><em>着手金</em><?php echo nl2br(get_post_meta($post->ID , 'bengo_start', TRUE));?></td>
<td><em>報酬金</em><?php echo nl2br(get_post_meta($post->ID , 'bengo_success', TRUE));?></td>
</tr>
</table>
<div class="sub">
<ul>
<?php $tmp_status = get_post_meta($post->ID , 'bengo_status', FALSE);?>
<?php $tmp_jian = get_post_meta($post->ID , 'bengo_jian', FALSE);?>
<li<?php if(!in_array('area' ,$tmp_status)) echo ' class="n"';?>>全国対応</li>
<li<?php if(!in_array('pps' ,$tmp_status)) echo ' class="n"';?>>後払い可</li>
<li<?php if(!in_array('tokuyaku' ,$tmp_status)) echo ' class="n"';?>>弁護特約</li>
<li<?php if(!in_array('jian_donichi' ,$tmp_jian)) echo ' class="n"';?>>土日対応</li>
</ul>

<h4>料金体系</h4>
<p><?php echo nl2br(get_post_meta($post->ID , 'bengo_ryokin', TRUE));?></p>
</div>
<div class="view">
<aside>
<?php if(get_post_meta($post->ID , 'mail_flag', FALSE)):?>
<?php if(in_array('tokuyaku' ,$tmp_status) && !wp_is_mobile()){ ?>
<h5 class="int">弁護士特約利用可能</h5>
<a href="<?php the_permalink(); ?>#tokuyaku"><img src="<?php bloginfo('template_directory'); ?>/img/btn_s_tokuyaku.png" alt="弁護士費用特約利用可能" /></a>
<p>自己負担なしで弁護士への依頼が可能</p>
<?php } ?>
<?php else: ?>

<h5>インターネットから相談</h5>
<a href="/bengo_contact?post_id=<?php the_ID();?>" rel="nofollow"><img src="<?php bloginfo('template_directory'); ?>/img/btn_s_freeform.png" alt="無料相談フォーム" class="btn_form" /></a>
<p>※無料相談フォームは24時間365日受付</p>

<?php endif; ?>
</aside>
<a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/btn_s_detail.png" alt="詳細情報を見る" class="btn_detail"  /></a>
</div>

</div>

<?php if(get_post_meta($post->ID , 'osusume_flag', FALSE)):?>
<div class="osusume_flag_title">【実績多数】おすすめ弁護士事務所</div>
<span class="osusume_flag_badge"><img src="<?php bloginfo('template_directory'); ?>/img/ic_osusume_badge.png" alt="注目弁護士" /></span>
<?php endif;?>

</li>
<?php endwhile; wp_reset_query();wp_reset_postdata();?>
<?php else:?>
<li><h2>検索結果がありませんでした。</h2></li>
<?php endif;?>
</ul>
</article>


<nav class="pager">
<?php if($the_query->have_posts() && $args['post__in']): ?>
<?php custum_pagination($the_query->max_num_pages); ?>
<div class="count"><?php my_result_count_tax($the_query->found_posts, $the_query->post_count);?></div>
<?php endif;?>
</nav>
<?php wp_reset_query();wp_reset_postdata();?>



<div class="check">
<dl>
<dt>北海道・東北地方</dt>
<dd><a href="/bengo/hokkaido">北海道</a></dd>
<dd><a href="/bengo/aomori">青森</a></dd>
<dd><a href="/bengo/iwate">岩手</a></dd>
<dd><a href="/bengo/akita">秋田</a></dd>
<dd><a href="/bengo/miyagi">宮城</a></dd>
<dd><a href="/bengo/yamagata">山形</a></dd>
<dd><a href="/bengo/fukushima">福島</a></dd>
<dt>関東</dt>
<dd><a href="/bengo/tokyo">東京</a></dd>
<dd><a href="/bengo/saitama">埼玉</a></dd>
<dd><a href="/bengo/kanagawa">神奈川</a></dd>
<dd><a href="/bengo/chiba">千葉</a></dd>
<dd><a href="/bengo/ibaraki">茨城</a></dd>
<dd><a href="/bengo/tochigi">栃木</a></dd>
<dd><a href="/bengo/gunma">群馬</a></dd>
<dt>北陸・甲信越</dt>
<dd><a href="/bengo/niigata">新潟</a></dd>
<dd><a href="/bengo/yamanashi">山梨</a></dd>
<dd><a href="/bengo/nagano">長野</a></dd>
<dd><a href="/bengo/ishikawa">石川</a></dd>
<dd><a href="/bengo/toyama">富山</a></dd>
<dd><a href="/bengo/fukui">福井</a></dd>
<dt>東海</dt>
<dd><a href="/bengo/aichi">愛知</a></dd>
<dd><a href="/bengo/shizuoka">静岡</a></dd>
<dd><a href="/bengo/gifu">岐阜</a></dd>
<dd><a href="/bengo/mie">三重</a></dd>
<dt>関西</dt>
<dd><a href="/bengo/osaka">大阪</a></dd>
<dd><a href="/bengo/kyoto">京都</a></dd>
<dd><a href="/bengo/nara">奈良</a></dd>
<dd><a href="/bengo/hyogo">兵庫</a></dd>
<dd><a href="/bengo/shiga">滋賀</a></dd>
<dd><a href="/bengo/wakayama">和歌山</a></dd>
<dt>中国・四国</dt>
<dd><a href="/bengo/hiroshima">広島</a></dd>
<dd><a href="/bengo/okayama">岡山</a></dd>
<dd><a href="/bengo/yamaguchi">山口</a></dd>
<dd><a href="/bengo/tottori">鳥取</a></dd>
<dd><a href="/bengo/shimane">島根</a></dd>
<dd><a href="/bengo/kagawa">香川</a></dd>
<dd><a href="/bengo/tokushima">徳島</a></dd>
<dd><a href="/bengo/ehime">愛媛</a></dd>
<dd><a href="/bengo/kochi">高知</a></dd>
<dt>九州</dt>
<dd><a href="/bengo/fukuoka">福岡</a></dd>
<dd><a href="/bengo/kumamoto">熊本</a></dd>
<dd><a href="/bengo/oita">大分</a></dd>
<dd><a href="/bengo/miyazaki">宮崎</a></dd>
<dd><a href="/bengo/saga">佐賀</a></dd>
<dd><a href="/bengo/nagasaki">長崎</a></dd>
<dd><a href="/bengo/kagoshima">鹿児島</a></dd>
<dd><a href="/bengo/okinawa">沖縄</a></dd>
</dl>
</div>


</div>
<!-- //front--></div>


<div id="wrap" class="bengoarchive">

<h2><span>弁護士に相談する前に！<br><?php echo $cat_title;?>の交通事故問題について知る</span></h2>

<article>

<h3><?php echo $cat_title;?>の交通事故 特徴</h3>
<?php echo nl2br(category_description()); ?>
<h3><?php echo $cat_title;?>の交通事故傾向</h3>
<?php $term_bengodetail = get_option('term_bengodetail'); echo $term_bengodetail[$cats->term_id];?>
<h4>弁護士をお探しの<?php echo $cat_title;?>在住の方へ</h4>
<p><?php echo $cat_title;?>にお住まいで、万が一交通事故問題に巻き込まれてしまったら、<?php echo $cat_title;?>に事務所を構える弁護士事務所か、交通事故問題に強い出張可能な弁護士に相談することで示談交渉がスムーズに進み、賠償金（慰謝料）の増額につながることは間違いありません。</p>
<p>まずはお気軽に無料相談してみてください。</p>
</article>


<?php get_footer();
