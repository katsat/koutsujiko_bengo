<?php if(!is_tax('bengo_cat') AND !is_post_type_archive('bengo') AND !is_page(array('32','733','735','1170','6265'))):?>
<div id="side">
<div id="side-inner">

<?php if(get_post_type() == 'bengo'):?>
<div class="side_office fix">
<?php
 $post_ids = $post->ID;
$args = array(
	'p' => $post_ids,
    'post_type' => 'bengo',
    'orderby' => 'menu_order',
    'order' => 'ASC',
    //'meta_query' => array(),
    'posts_per_page' => '1'
);
query_posts( $args );
?>
<?php while ( have_posts() ) : the_post();?>

<?php echo wp_get_attachment_image(post_custom('File Upload'), array('260', '170'), false, array('class' => 'visual', 'alt' => get_the_title()));?>
<h5><?php if(get_post_meta($post->ID , 'bengo_officialname', TRUE)):?><?php echo get_post_meta($post->ID , 'bengo_officialname', TRUE);?><?php else:?><?php the_title(); ?><?php endif;?></h5>
<table>
<tr>
<th>スマホ・携帯からも無料通話</th>
</tr>
<tr>
<td class="tel"><a href="<?php the_permalink(); ?>?pid=<?php echo $post->ID?>" rel="nofollow"><?php echo get_post_meta($post->ID , 'bengo_tel', TRUE);?></a></td>
</tr>
<tr>
<th>受付時間</th>
</tr>
<tr>
<td class="tel"><span><?php echo get_post_meta($post->ID , 'bengo_open', TRUE);?></span>
<p>
<?php $jdi = japan_date_info();
if(get_post_meta($post->ID , 'bengo_open_option', TRUE) == 3) {
	$jdi_flag = TRUE;
} elseif(get_post_meta($post->ID , 'bengo_open_option', TRUE) == 2) {
	if(empty($jdi['2'])) $jdi_flag = TRUE;
	if($jdi['2'] == Sat || $jdi['2'] == Sun) $jdi_flag = TRUE;
} elseif(get_post_meta($post->ID , 'bengo_open_option', TRUE) == 1) {
	if(empty($jdi['2'])) $jdi_flag = TRUE;
	if($jdi['2'] == Sat) $jdi_flag = TRUE;
} elseif(get_post_meta($post->ID , 'bengo_open_option', TRUE) == 4) {
	if(empty($jdi['2'])) $jdi_flag = TRUE;
	if($jdi['2'] == Sun) $jdi_flag = TRUE;
}
if(empty($jdi['2'])) $jdi_flag = TRUE;//平日なら全部OK
?>
<?php if( $jdi_flag && (date_i18n("Hi") >= get_post_meta($post->ID , 'bengo_open_start', TRUE) and date_i18n("Hi") <= get_post_meta($post->ID , 'bengo_open_end', TRUE))):?>
現在<?php echo date_i18n("H:i");?>です。お気軽にお電話ください。
<?php else:?>
現在<?php echo date_i18n("H:i");?>。電話受付時間外です。無料相談フォームよりご連絡ください。
<?php endif;?>
</p>
</td>
</tr>
<tr>
<th>住所</th>
</tr>
<tr>
<td><?php echo get_post_meta($post->ID , 'bengo_address', TRUE);?></td>
</tr>
</table>
<?php endwhile; wp_reset_query();?>

<div class="btn">
<?php if(!get_post_meta($post_ids , 'mail_flag', FALSE)):?>
<a href="/bengo_contact?post_id=<?php echo $post_ids;?>" class="link" rel="nofollow">無料相談フォームはコチラ</a>
<p>無料相談フォームは24時間受付中</p>
<?php endif; ?>
<div class="ssl">
<!-- GeoTrust Smart Icon tag. Do not edit. -->
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript" CHARSET="Shift_JIS" SRC="//smarticon.geotrust.com/si.js"></SCRIPT>
<!-- END of GeoTrust Smart Icon tag -->
GeoTrustのSSLサーバ証明書を使用して、利用者の個人情報を保護しています。
</div>
</div>

<!-- //side_office--></div>


<?php else: ?>

<div class="side_point">
<p>ご相談状況</p>
<ul>
<?php
//$numposts = $wpdb->get_var($wpdb->prepare("SELECT count(*) FROM $wpdb->postmeta WHERE meta_key = 'bengo_order' AND meta_value LIKE %s" , '%'. like_escape('zenkoku'). '%'));
$ret = $wpdb->get_results(TBO::getQueryOfPostsByBengoOrderTypeOrdered('zenkoku'));
$numposts = $wpdb->num_rows;
if (0 < $numposts) $numposts = number_format($numposts);?>
<li class="p01">電話相談：<span>完全無料</span></li>
<li class="p02">弁護士：<span><?php echo $numposts;?></span>事務所</li>
<li class="p03">相談件数：<span>20,000</span>件 突破</li>
</ul>
</div>

<h4>【注目】交通事故に強い弁護士</h4>
<div class="side_office wraplinkbengo">
<?php
//$post_ids[] = 2771;//ジェネシス
//$post_ids[] = 1215;//ゆう
//$post_ids[] = 24;//みずき
//$post_ids[] = 756;//アヴァンセ
$post_ids[] = 7629;//サリュ
$post_ids[] = 1831;//卯月
$post_id = $post_ids[array_rand($post_ids)];

$args = array(
	'p' => $post_id,
    'post_type' => 'bengo',
    'orderby' => 'menu_order',
    'order' => 'ASC',
    //'meta_query' => array(),
    'posts_per_page' => '1'
);
query_posts( $args );
?>
<?php while ( have_posts() ) : the_post();?>

<h5><?php echo get_post_meta($post->ID , 'bengo_catch', TRUE);?></h5>
<a href="<?php the_permalink();?>"><?php echo wp_get_attachment_image(post_custom('File Upload'), array('260', '170'), false, array('class' => 'visual', 'alt' => get_the_title()));?></a>
<h6><?php the_title();?></h6>
<p><?php echo get_post_meta($post->ID , 'bengo_msg', TRUE);?></p>
<?php endwhile; wp_reset_query();?>
<!-- //side_office--></div>

<div class="banner"><a href="/calculate"><img src="<?php bloginfo('template_directory'); ?>/img/btn_keisan.png" alt="弁護士基準で慰謝料を自動計算する" /></a></div>

<?php endif; ?>

<?php if(get_post_type() != 'bengo'):?>

<div class="search_area">
<h5>都道府県から弁護士を探す</h5>
<dl>
<dt>北海道・東北地方</dt>
<dd><a href="/bengo/hokkaido">北海道</a></dd>
<dd><a href="/bengo/aomori">青森</a></dd>
<dd><a href="/bengo/iwate">岩手</a></dd>
<dd><a href="/bengo/akita">秋田</a></dd>
<dd><a href="/bengo/miyagi">宮城</a></dd>
<dd><a href="/bengo/yamagata">山形</a></dd>
<dd><a href="/bengo/fukushima">福島</a></dd>
<dt>関東</dt>
<dd><a href="/bengo/tokyo">東京</a></dd>
<dd><a href="/bengo/saitama">埼玉</a></dd>
<dd><a href="/bengo/kanagawa">神奈川</a></dd>
<dd><a href="/bengo/chiba">千葉</a></dd>
<dd><a href="/bengo/ibaraki">茨城</a></dd>
<dd><a href="/bengo/tochigi">栃木</a></dd>
<dd><a href="/bengo/gunma">群馬</a></dd>
<dt>北陸・甲信越</dt>
<dd><a href="/bengo/niigata">新潟</a></dd>
<dd><a href="/bengo/yamanashi">山梨</a></dd>
<dd><a href="/bengo/nagano">長野</a></dd>
<dd><a href="/bengo/ishikawa">石川</a></dd>
<dd><a href="/bengo/toyama">富山</a></dd>
<dd><a href="/bengo/fukui">福井</a></dd>
<dt>東海</dt>
<dd><a href="/bengo/aichi">愛知</a></dd>
<dd><a href="/bengo/shizuoka">静岡</a></dd>
<dd><a href="/bengo/gifu">岐阜</a></dd>
<dd><a href="/bengo/mie">三重</a></dd>
<dt>関西</dt>
<dd><a href="/bengo/osaka">大阪</a></dd>
<dd><a href="/bengo/kyoto">京都</a></dd>
<dd><a href="/bengo/nara">奈良</a></dd>
<dd><a href="/bengo/hyogo">兵庫</a></dd>
<dd><a href="/bengo/shiga">滋賀</a></dd>
<dd><a href="/bengo/wakayama">和歌山</a></dd>
<dt>中国・四国</dt>
<dd><a href="/bengo/hiroshima">広島</a></dd>
<dd><a href="/bengo/okayama">岡山</a></dd>
<dd><a href="/bengo/yamaguchi">山口</a></dd>
<dd><a href="/bengo/tottori">鳥取</a></dd>
<dd><a href="/bengo/shimane">島根</a></dd>
<dd><a href="/bengo/kagawa">香川</a></dd>
<dd><a href="/bengo/tokushima">徳島</a></dd>
<dd><a href="/bengo/ehime">愛媛</a></dd>
<dd><a href="/bengo/kochi">高知</a></dd>
<dt>九州</dt>
<dd><a href="/bengo/fukuoka">福岡</a></dd>
<dd><a href="/bengo/kumamoto">熊本</a></dd>
<dd><a href="/bengo/oita">大分</a></dd>
<dd><a href="/bengo/miyazaki">宮崎</a></dd>
<dd><a href="/bengo/saga">佐賀</a></dd>
<dd><a href="/bengo/nagasaki">長崎</a></dd>
<dd><a href="/bengo/kagoshima">鹿児島</a></dd>
<dd><a href="/bengo/okinawa">沖縄</a></dd>
</dl>
<!-- //search_japan--></div>

<div class="search_status">
<h5>条件から弁護士を探す</h5>
<ul>
<li><a href="/bengo?jian=jian_isya">慰謝料</a></li>
<li><a href="/bengo?jian=jian_songai">損害賠償</a></li>
<li><a href="/bengo?jian=jian_jidan">示談交渉</a></li>
<li><a href="/bengo?jian=jian_kashitsu">過失割合</a></li>
<li><a href="/bengo?jian=jian_kouisyo">後遺障害</a></li>
<li><a href="/bengo?jian=jian_jinshin">人身事故</a></li>
<li><a href="/bengo?jian=jian_shibo">死亡事故</a></li>
<li><a href="/bengo?status=tyakusyu">相談料0円</a></li>
<li><a href="/bengo?status=free">着手金0円</a></li>
</ul>
<!-- //search_status--></div>

<div class="search_nintei">
<h5>後遺障害等級認定一覧</h5>
<ul>
<li><a href="/grade-certification/requiring-long-term-care.html">後遺障害1級・2級(要介護認定)</a></li>
<li><a href="/grade-certification/disability-primary.html">後遺障害1級</a></li>
<li><a href="/grade-certification/disability-grade2.html">後遺障害2級</a></li>
<li><a href="/grade-certification/disability-grade3.html">後遺障害3級</a></li>
<li><a href="/grade-certification/disability-grade4.html">後遺障害4級</a></li>
<li><a href="/grade-certification/disability-grade5.html">後遺障害5級</a></li>
<li><a href="/grade-certification/disability-grade6.html">後遺障害6級</a></li>
<li><a href="/grade-certification/disability-grade7.html">後遺障害7級</a></li>
<li><a href="/grade-certification/disability-grade8.html">後遺障害8級</a></li>
<li><a href="/grade-certification/disability-grade9.html">後遺障害9級</a></li>
<li><a href="/grade-certification/disability-grade10.html">後遺障害10級</a></li>
<li><a href="/grade-certification/disability-grade11.html">後遺障害11級</a></li>
<li><a href="/grade-certification/disability-grade12.html">後遺障害12級</a></li>
<li><a href="/grade-certification/disability-grade13.html">後遺障害13級</a></li>
<li><a href="/grade-certification/disability-grade14.html">後遺障害14級</a></li>
</ul>
<!-- //search_nintei--></div>

<div class="side_contents">
<h5><img src="<?php bloginfo('template_directory'); ?>/img/side_contents.png" alt="コンテンツ一覧" /></h5>
<ul>

<li><a href="/knowledge">交通事故の基礎知識</a></li>
<li><a href="/residual-disability">後遺障害の種類</a></li>
<li><a href="/grade-certification">後遺障害の等級認定</a></li>
<li><a href="/car-insurance">自動車保険</a></li>
<li><a href="/damages">損害賠償</a></li>
<li><a href="/negligence-percentage">過失割合</a></li>
<li><a href="/resolution">裁判・和解編</a></li>
<li><a href="/out-of-court">示談編</a></li>
<li><a href="/lawyer">弁護士相談編</a></li>
<li><a href="/manual">交通事故対応マニュアル</a></li>
<li><a href="/punishment">加害者に刑罰を与えたい</a></li>
<li><a href="/principal-litigation">本人訴訟</a></li>
</ul>
<!-- //side_contents--></div>

<div class="banner"><a href="/whiplash-consolationmoney"><img src="<?php bloginfo('template_directory'); ?>/img/side_bnr_muchiuchi.png" alt="むち打ちの慰謝料相場" /></a></div>

<div class="side_credit">
<p>交通事故弁護士相談広場は、交通事故に遭われた被害者のための情報ポータルサイトです。交通事故関連のコンテンツを掲載し、皆様のお役に立てるWEBサイトを目指しております。交通事故に遭われた場合には、保険会社との示談交渉や損害賠償、後遺障害など日常生活では馴染みのない問題が発生します。納得のいく解決を迎えるためには弁護士に相談し、介入してもらうことで示談金や慰謝料が増額される可能性が高まります。</p>
<ul>
<li><a href="/company">運営会社</a></li>
<li><a href="/privacy">プライバシーポリシー</a></li>
<li><a href="/contract">ご利用規約</a></li>
<li><a href="/contact">お問い合わせ</a></li>
</ul>
<!-- //side_credit--></div>
<?php endif;?>
<!-- //side-inner --></div>
<!-- //side--></div>

<?php endif;?>

<!-- //wrap--></div>



<footer id="globalfooter">
<form action="/" id="search" method="get" action="<?php echo home_url( '/' ); ?>"><fieldset><input type="text" value="<?php echo wp_specialchars($s, 1);?>" name="s" id="s" placeholder="サイト内検索" /><input type="submit" value="検索" /></fieldset></form>
<div class="inner">
<dl class="contents">
<dt>コンテンツ一覧</dt>
<dd><a href="/knowledge">交通事故の基礎知識</a></dd>
<dd><a href="/residual-disability">後遺障害の種類</a></dd>
<dd><a href="/grade-certification">後遺障害の等級認定</a></dd>
<dd><a href="/car-insurance">自動車保険</a></dd>
<dd><a href="/damages">損害賠償</a></dd>
<dd><a href="/negligence-percentage">過失割合</a></dd>
<dd><a href="/resolution">裁判・和解編</a></dd>
<dd><a href="/out-of-court">示談編</a></dd>
<dd><a href="/lawyer">弁護士相談編</a></dd>
<dd><a href="/manual">交通事故対応マニュアル</a></dd>
<dd><a href="/punishment">加害者に刑罰を与えたい</a></dd>
<dd><a href="/principal-litigation">本人訴訟</a></dd>
</dl>

<div class="search">
<h6>条件から弁護士を探す</h6>
<ul>
<li><a href="/bengo?jian=jian_isya">慰謝料</a></li>
<li><a href="/bengo?jian=jian_songai">損害賠償</a></li>
<li><a href="/bengo?jian=jian_jidan">示談交渉</a></li>
<li><a href="/bengo?jian=jian_kashitsu">過失割合</a></li>
<li><a href="/bengo?jian=jian_kouisyo">後遺障害</a></li>
<li><a href="/bengo?jian=jian_jinshin">人身事故</a></li>
<li><a href="/bengo?jian=jian_shibo">死亡事故</a></li>
<li><a href="/bengo?status=tyakusyu">着手金0円</a></li>
<li><a href="/bengo?status=free">相談料0円</a></li>
</ul>
<h6>都道府県から弁護士を探す</h6>
<dl>
<dt>北海道・東北地方</dt>
<dd><a href="/bengo/hokkaido">北海道</a></dd>
<dd><a href="/bengo/aomori">青森</a></dd>
<dd><a href="/bengo/iwate">岩手</a></dd>
<dd><a href="/bengo/akita">秋田</a></dd>
<dd><a href="/bengo/miyagi">宮城</a></dd>
<dd><a href="/bengo/yamagata">山形</a></dd>
<dd><a href="/bengo/fukushima">福島</a></dd>
<dt>関東</dt>
<dd><a href="/bengo/tokyo">東京</a></dd>
<dd><a href="/bengo/saitama">埼玉</a></dd>
<dd><a href="/bengo/kanagawa">神奈川</a></dd>
<dd><a href="/bengo/chiba">千葉</a></dd>
<dd><a href="/bengo/ibaraki">茨城</a></dd>
<dd><a href="/bengo/tochigi">栃木</a></dd>
<dd><a href="/bengo/gunma">群馬</a></dd>
<dt>北陸・甲信越</dt>
<dd><a href="/bengo/niigata">新潟</a></dd>
<dd><a href="/bengo/yamanashi">山梨</a></dd>
<dd><a href="/bengo/nagano">長野</a></dd>
<dd><a href="/bengo/ishikawa">石川</a></dd>
<dd><a href="/bengo/toyama">富山</a></dd>
<dd><a href="/bengo/fukui">福井</a></dd>
<dt>東海</dt>
<dd><a href="/bengo/aichi">愛知</a></dd>
<dd><a href="/bengo/shizuoka">静岡</a></dd>
<dd><a href="/bengo/gifu">岐阜</a></dd>
<dd><a href="/bengo/mie">三重</a></dd>
<dt>関西</dt>
<dd><a href="/bengo/osaka">大阪</a></dd>
<dd><a href="/bengo/kyoto">京都</a></dd>
<dd><a href="/bengo/nara">奈良</a></dd>
<dd><a href="/bengo/hyogo">兵庫</a></dd>
<dd><a href="/bengo/shiga">滋賀</a></dd>
<dd><a href="/bengo/wakayama">和歌山</a></dd>
<dt>中国・四国</dt>
<dd><a href="/bengo/hiroshima">広島</a></dd>
<dd><a href="/bengo/okayama">岡山</a></dd>
<dd><a href="/bengo/yamaguchi">山口</a></dd>
<dd><a href="/bengo/tottori">鳥取</a></dd>
<dd><a href="/bengo/shimane">島根</a></dd>
<dd><a href="/bengo/kagawa">香川</a></dd>
<dd><a href="/bengo/tokushima">徳島</a></dd>
<dd><a href="/bengo/ehime">愛媛</a></dd>
<dd><a href="/bengo/kochi">高知</a></dd>
<dt>九州</dt>
<dd><a href="/bengo/fukuoka">福岡</a></dd>
<dd><a href="/bengo/kumamoto">熊本</a></dd>
<dd><a href="/bengo/oita">大分</a></dd>
<dd><a href="/bengo/miyazaki">宮崎</a></dd>
<dd><a href="/bengo/saga">佐賀</a></dd>
<dd><a href="/bengo/nagasaki">長崎</a></dd>
<dd><a href="/bengo/kagoshima">鹿児島</a></dd>
<dd><a href="/bengo/okinawa">沖縄</a></dd>
</dl>
</div>
<!-- //inner--></div>
<!-- //globalfooter--></footer>
<div class="credit">
<ul>
<li><a href="/">HOME</a></li>
<li><a href="/company">運営会社</a></li>
<li><a href="/privacy">プライバシーポリシー</a></li>
<li><a href="/contract">ご利用規約</a></li>
<li><a href="/contact">お問い合わせ</a></li>
<li><a href="/keisai">当サイトへの掲載をご希望の弁護士の方へ</a></li>
</ul>
<address>
<em>【運営】株式会社Agoora</em>
〒166-0003 東京都杉並区高円寺南4-7-1-302　TEL：03-5929-7575
</address>
<small>&copy; 2014 Agoora.inc.</small>
</div>
<?php wp_footer();?>
<?php if ( is_user_logged_in() ) : ?>

<?php else: ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60039035-1', 'auto');
  ga('send', 'pageview');

</script>
<?php endif; ?>
<?php if ( wp_is_mobile() ) {?>
<?php if(!is_singular( 'bengo' ) AND !is_tax('bengo_cat') AND !is_post_type_archive('bengo') AND !is_page(array('20','32','733','735','1170','6265'))):?>
<div id="page-navi"><h6>相談料無料!慰謝料が3倍以上UPの実績多数</h6><a href="/#search_area">お近くの弁護士を探す</a><a href="/calculate">弁護士基準で慰謝料計算</a></div>
<?php /* ◆◆◆◆◆◆
	<div id="smp_nav">
	<a href="/bengo"><img src="<?php bloginfo('template_directory'); ?>/img/ft_btn01.png" alt="弁護士を探す" ></a>
	<a href="/calculate"><img src="<?php bloginfo('template_directory'); ?>/img/ft_btn02.png" alt="慰謝料計算" ></a>
	<a href="/lawyer/cost02.html"><img src="<?php bloginfo('template_directory'); ?>/img/ft_btn03.png" alt="弁護士費用" ></a>
	<a href="/lawyer/lawyer-rider.html"><img src="<?php bloginfo('template_directory'); ?>/img/ft_btn04.png" alt="弁護士特約" ></a>
	<a href="/resolve"><img src="<?php bloginfo('template_directory'); ?>/img/ft_btn05.png" alt="解決の流れ" ></a>
	</div>

◆◆◆◆◆ */ ?>
<?php endif;?>
<?php }else{ ?>
<p id="page-top"><a href="#top" rel="nofollow">TOP</a></p>
<?php } ?>
</body>
</html>
