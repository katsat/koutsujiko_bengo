<div class="helpdesk">
<em>交通事故に巻き込まれてしまい、弁護士をお探しの方へ</em>
<h3>当てはまるなら、すぐに弁護士に相談!</h3>
<ul>
<li>保険会社の慰謝料提示額に納得がいかない</li>
<li>過失割合提示額に納得がいかない。</li>
<li>後遺障害（むちうち症など）の認定を相談したい</li>
</ul>
<a href="/bengo" class="btn">交通事故問題に強い弁護士を探す</a>
<dl>
<dt>弁護士に相談することに不安が残る方へ</dt>
<dd class="keisan"><p>「保険会社の慰謝料提示額が適正なの？」疑問があるなら、まずは下記の自動計算シミュレーションでで弁護士基準の慰謝料額を確認してみてください。</p>
<a href="/calculate" class="keisanbtn">弁護士基準で慰謝料を自動計算する</a>
</dd>
<dd><a href="/resolve">交通事故解決までの流れ</a></dd>
<dd><a href="/lawyer-criteria">弁護士依頼で慰謝料が増額する理由</a></dd>
<dd><a href="/lawyer/cost02.html">交通事故の弁護士費用の相場</a></dd>
<dd><a href="/whiplash-consolationmoney">むち打ち症の慰謝料相場</a></dd>
<dd><a href="/grade-certification">後遺障害の等級認定について知る</a></dd>
<dd><a href="/lawyer/lawyer-rider.html">損は無し！保険の弁護士特約を利用しよう</a></dd>
<dd class="keisan"><a href="/compensation" class="keisanbtn">交通事故の慰謝料（賠償金）増額事例</a></dd>
</dl>

<!-- //helpdesk--></div>