<?php
/*
Plugin Name: WP Previous Next Post Links
Plugin URI: http://www.koikikukan.com/plugins/
Description: 現在のブログ記事の前後記事のリストを表示します。
Version: 0.1
Author: Yujiro Araki
Author URI: http://www.koikikukan.com/
*/

function previous_post_links($format='&laquo; %link', $link='%title', $in_same_cat = false, $excluded_categories = '', $number, $sort_order = 'DESC') {
	plugin_adjacent_post_link($format, $link, $in_same_cat, $excluded_categories, true, $number, $sort_order);
}

function next_post_links($format='%link &raquo;', $link='%title', $in_same_cat = false, $excluded_categories = '', $number, $sort_order = 'ASC') {
	plugin_adjacent_post_link($format, $link, $in_same_cat, $excluded_categories, false, $number, $sort_order);
}

function plugin_adjacent_post_link( $format, $link, $in_same_cat = false, $excluded_categories = '', $previous = true, $number = 1, $sort_order) {
//	if ( $previous && is_attachment() )
//		$post = get_post( get_post()->post_parent );
//	else
		$post = plugin_get_adjacent_post( $in_same_cat, $excluded_categories, $previous, $number, $sort_order );


//	if ( ! $post ) {
//		$output = '';
//	} else {
//		$title = $post->post_title;

//		if ( empty( $post->post_title ) )
//			$title = $previous ? __( 'Previous Post' ) : __( 'Next Post' );

//		$title = apply_filters( 'the_title', $title, $post->ID );
//		$date = mysql2date( get_option( 'date_format' ), $post->post_date );
//		$rel = $previous ? 'prev' : 'next';

//		$string = '<a href="' . get_permalink( $post ) . '" rel="'.$rel.'">';
//		$inlink = str_replace( '%title', $title, $link );
//		$inlink = str_replace( '%date', $date, $inlink );
//		$inlink = $string . $inlink . '</a>';

//		$output = str_replace( '%link', $inlink, $format );
//	}

//	$adjacent = $previous ? 'previous' : 'next';

//	echo apply_filters( "{$adjacent}_post_link", $output, $format, $link, $post );
echo $post;

}

function plugin_get_adjacent_post( $in_same_cat = false, $excluded_categories = '', $previous = true, $number, $sort_order ) {
	global $wpdb;

	if ( ! $post = get_post() )
		return null;

	$current_post_date = $post->post_date;

	$join = '';
	$posts_in_ex_cats_sql = '';
	if ( $in_same_cat || ! empty( $excluded_categories ) ) {
		$join = " INNER JOIN $wpdb->term_relationships AS tr ON p.ID = tr.object_id INNER JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id";

		if ( $in_same_cat ) {
			if ( ! is_object_in_taxonomy( $post->post_type, 'category' ) )
				return '';
			$cat_array = wp_get_object_terms($post->ID, 'category', array('fields' => 'ids'));
			if ( ! $cat_array || is_wp_error( $cat_array ) )
				return '';
			$join .= " AND tt.taxonomy = 'category' AND tt.term_id IN (" . implode(',', $cat_array) . ")";
		}

		$posts_in_ex_cats_sql = "AND tt.taxonomy = 'category'";
		if ( ! empty( $excluded_categories ) ) {
			if ( ! is_array( $excluded_categories ) ) {
				// back-compat, $excluded_categories used to be IDs separated by " and "
				if ( strpos( $excluded_categories, ' and ' ) !== false ) {
					_deprecated_argument( __FUNCTION__, '3.3', sprintf( __( 'Use commas instead of %s to separate excluded categories.' ), "'and'" ) );
					$excluded_categories = explode( ' and ', $excluded_categories );
				} else {
					$excluded_categories = explode( ',', $excluded_categories );
				}
			}

			$excluded_categories = array_map( 'intval', $excluded_categories );

			if ( ! empty( $cat_array ) ) {
				$excluded_categories = array_diff($excluded_categories, $cat_array);
				$posts_in_ex_cats_sql = '';
			}

			if ( !empty($excluded_categories) ) {
				$posts_in_ex_cats_sql = " AND tt.taxonomy = 'category' AND tt.term_id NOT IN (" . implode($excluded_categories, ',') . ')';
			}
		}
	}

	$adjacent = $previous ? 'previous' : 'next';
	$op = $previous ? '<' : '>';
	$order = $previous ? 'DESC' : 'ASC';

	$join  = apply_filters( "get_{$adjacent}_post_join", $join, $in_same_cat, $excluded_categories );
	$where = apply_filters( "get_{$adjacent}_post_where", $wpdb->prepare("WHERE p.post_date $op %s AND p.post_type = %s AND p.post_status = 'publish' $posts_in_ex_cats_sql", $current_post_date, $post->post_type), $in_same_cat, $excluded_categories );
	$sort  = apply_filters( "get_{$adjacent}_post_sort", "ORDER BY p.post_date $order LIMIT $number" );

	$query = "SELECT * FROM $wpdb->posts AS p $join $where $sort";
	$query_key = 'adjacent_post_' . md5($query);
//	$result = wp_cache_get($query_key, 'counts');
//	if ( false !== $result ) {
//		if ( $result )
//			$result = get_post( $result );
//		return $result;
//	}

///////////////////////
	$results = $wpdb->get_results( $query );
	if (!$previous) {
		if ( $sort_order == 'ASC') {
			ksort( $results , SORT_STRING );
		}
		if ( $sort_order == 'DESC') {
			krsort( $results , SORT_STRING );
		}
	} else {
		if ( $sort_order == 'ASC') {
			krsort( $results , SORT_STRING );
		}
		if ( $sort_order == 'DESC') {
			ksort( $results , SORT_STRING );
		}
	}
	foreach ( $results as $result ) {
	    $data .= '<li class="list-item"><a href="'. get_permalink($result->ID) . '">' . $result->post_title . '</a></li>';
	}
///////////////////////

	if ( null === $result )
		$result = '';

	wp_cache_set($query_key, $result, 'counts');

//	if ( $result )
//		$result = get_post( $result );

	return $data;
}

?>
