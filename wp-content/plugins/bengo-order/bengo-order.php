<?php
/*
Plugin Name: 弁護士事務所の並べ替え
Description: 弁護士事務所のタグ別（エリア別）の並び順をドラッグ・アンド・ドロップで編集できます
Version: 1.0.0
Author: threebottles
*/

define('TBOPATH',   plugin_dir_path(__FILE__));
define('TBOURL',    plugins_url('', __FILE__));

global $tbo_db_version;
$tbo_db_version = '1.0';

/* uninstall hock */
register_uninstall_hook( __FILE__, 'tbo_uninstall' );
function tbo_uninstall()
{
	global $wpdb;

	/* termsテーブルの並び順カラムを削除 */
	$result = $wpdb->query( "DESCRIBE $wpdb->terms `tbo_term_order`" );
	if ( $result ){
		$query = "ALTER TABLE $wpdb->terms DROP `tbo_term_order`";
		$result = $wpdb->query( $query );
	}

	/* bengo_ordersテーブルを削除 */
	$table_name = $wpdb->prefix . TBO_TABLE_BENGO_ORDER;

	$wpdb->query("DROP TABLE IF EXISTS $table_name");

	delete_option( "tbo_db_version" );
	delete_option( 'tbo_activation' );
}

register_activation_hook(__FILE__, 'tbo_activation');
function tbo_activation()
{
  global $wpdb;
	global $tbo_db_version;

  /* termsテーブルに並び順を設定するカラムを追加 */
  $result = $wpdb->query( "DESCRIBE $wpdb->terms `tbo_term_order`" );
  if ( !$result ) {
    $query = "ALTER TABLE $wpdb->terms ADD `tbo_term_order` INT( 4 ) NULL DEFAULT '0'";
    $result = $wpdb->query( $query );
  }

	/* 「bengo_order_type」タクソノミー別の並び順を設定するテーブルを追加 */
	$table_name = $wpdb->prefix . 'bengo_orders';
  $charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
	    id mediumint(9) NOT NULL AUTO_INCREMENT,
			post_id bigint(20) DEFAULT 0 NOT NULL,
	    bengo_order_type varchar(255) NOT NULL,
	    sort int DEFAULT 0 NOT NULL,
	    UNIQUE KEY id (id)
	  ) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'tbo_db_version', $tbo_db_version );

  if ( !get_option( 'tbo_activation' ) ) tbo_install_data();

  update_option( 'tbo_activation', 1 );

}

function tbo_install_data()
{
  global $wpdb;

  require_once TBOPATH.'include/tbo_seeds.php';

  foreach ($bengo_order_types as $seq => $bot) {
    /*
     * 弁護士事務所 表示順区分の初期データ登録
     */
    $wpdb->insert(
      $wpdb->terms,
      array(
        'name' => $bot['name'],
        'slug' => $bot['slug'],
        'tbo_term_order' => $seq+1,
      )
    );

    $term_id = $wpdb->insert_id;

    $wpdb->insert(
      $wpdb->term_taxonomy,
      array(
        'term_id' => $term_id,
        'taxonomy' => 'bengo_order_type',
        'description' => "",
      )
    );

    $term_taxonomy_id = $wpdb->insert_id;

    // 弁護士事務所の表示エリアと並び順の移行
    $sql = $wpdb->prepare("
      SELECT *
      FROM $wpdb->postmeta
      WHERE meta_key = 'bengo_order'
        AND meta_value LIKE %s" , '%'. like_escape($bot['slug']). '%'
    );
    $ret = $wpdb->get_results($sql);
    $ooo = array();
    foreach($ret as $orders){
      $sss = json_decode($orders->meta_value, true);
      $ooo[$orders->post_id] = $sss[$bot['slug']];
    }
    arsort($ooo);
    $args = array(
        'post_type' => 'bengo',
        'order' => 'DESC',
        'post__in' => array_keys($ooo),
        'orderby' => 'post__in',
        'nopaging' => true,
    );
    $the_query = new WP_Query( $args );
		$total = $the_query->found_posts;
    if ( $the_query->have_posts() ) {
      $seq = 0;
      while ( $the_query->have_posts() ) : $the_query->the_post();
        $post_id = $the_query->post->ID;

        // postとterm_taxonomyの紐付け
        $wpdb->insert(
          $wpdb->term_relationships,
          array(
            'object_id' => $post_id,
            'term_taxonomy_id' => $term_taxonomy_id,
            'term_order' => 0,
          )
        );

        // bengo_ordersにbengo_order_type別にソート番号を登録
        $wpdb->insert(
          $wpdb->prefix . 'bengo_orders',
          array(
            'post_id' => $post_id,
            'bengo_order_type' => $bot['slug'],
            'sort' => ($total-$seq),
          )
        );

        $seq = $seq + 1;
      endwhile;

      // taxonomyのcountを更新
      $wpdb->update(
        $wpdb->term_taxonomy,
        array(
          'count' => $total,
        ),
        array(
          'term_taxonomy_id' => $term_taxonomy_id,
        )
      );
    }
    wp_reset_query();
    wp_reset_postdata();
  }
}

register_deactivation_hook(__FILE__, 'tbo_deactivation');
function tbo_deactivation()
{

}

/*
 * 新規で弁護士事務所を投稿した際に、紐付けた表示区分毎にpostmetaにソート番号を追加する
 */
function insert_bengo_order_type_postmeta($post_id){

  global $wpdb;

	$terms = wp_get_post_terms($post_id, 'bengo_order_type');
	if (!empty($terms)) {
		foreach($terms as $term) {

			$query = TBO::getQueryOfPostsByBengoOrderTypeOrdered($term->slug);
			$wpdb->get_results($query);
			$total = $wpdb->num_rows;
			$sort = $total + 1; // 追加した弁護士事務所は一番上に出したいので一番大きなweightを登録

			$wpdb->insert(
				$wpdb->prefix . bengo_orders,
				array(
					'post_id' => $post_id,
					'bengo_order_type' => $term->slug,
					'sort' => $sort,
				)
			);

		}
	}
}
// 投稿保存時のアクション
add_action('edit_post', 'insert_bengo_order_type_postmeta');

// bengo_orderクラス
include_once(TBOPATH . '/include/tbo_class.php');
$bengo_order = new TBO();
