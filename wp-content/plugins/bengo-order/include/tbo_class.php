<?php
class TBO
{
    function __construct()
    {
        add_action( 'admin_init', array($this, 'registerFiles'), 11 );
        add_action( 'admin_init', array($this, 'refresh' ) );
        add_action( 'admin_menu', array($this, 'addMenu') );
        add_action( 'wp_ajax_update-menu-order-tags', array($this, 'updateMenuOrderTags' ) );
        add_action( 'wp_ajax_update-bengo-order', array($this, 'saveAjaxBengoOrder') );

        // reorder taxonomies
    		add_filter( 'get_terms_orderby', array( $this, 'tboGetTermsOrderby' ), 10, 3 );
    		add_filter( 'wp_get_object_terms', array( $this, 'tboGetObjectTerms' ), 10, 3 );
        add_filter( 'get_terms', array( $this, 'tboGetObjectTerms' ), 10, 3 );
    }

    function getTboOptionsObjects()
  	{
      return array('bengo');

  	}
  	function getTboOptionsTags()
  	{
  		return array('bengo_cat', 'bengo_order_type');
  	}

    function _checkLoadScriptCss()
  	{
  		$active = false;

  		$objects = $this->getTboOptionsObjects();
  		$tags = $this->getTboOptionsTags();

      // exclude (sorting, addnew page, edit page)
  		if ( isset( $_GET['orderby'] ) || strstr( $_SERVER['REQUEST_URI'], 'action=edit' ) || strstr( $_SERVER['REQUEST_URI'], 'wp-admin/post-new.php' ) ) return false;

      if (strstr( $_SERVER['REQUEST_URI'], 'wp-admin/edit-tags.php') && in_array($_GET['post_type'], $objects )) {
        $active = true;
      }

      if (strstr( $_SERVER['REQUEST_URI'], 'wp-admin/edit.php') && in_array($_GET['post_type'], $objects )) {
        if (isset($_GET['page']) && $_GET['page'] == 'bengo-order') {
          $active = true;
        }
      }

  		return $active;
  	}

    function registerFiles()
    {
      if ( $this->_checkLoadScriptCss() ) {
        wp_enqueue_script('jquery');
        wp_enqueue_script('jquery-ui-sortable');
        wp_enqueue_script('TBOScripts', TBOURL . '/js/tbo.js', array('jquery'), null, true);

        wp_register_style('TBOStyleSheets', TBOURL . '/css/bengo_order.css');
        wp_enqueue_style( 'TBOStyleSheets');
      }
    }

    function addMenu()
    {
        add_submenu_page('edit.php?post_type=bengo', '事務所並べ替え', '事務所並べ替え', 'edit_posts', 'bengo-order', array(&$this, 'SortPage') );
    }

    function refresh()
  	{
  		global $wpdb;
  		$tags = $this->getTboOptionsTags();

  		if ( !empty( $tags ) ) {
  			foreach( $tags as $taxonomy ) {
  				$result = $wpdb->get_results( "
  					SELECT count(*) as cnt, max(tbo_term_order) as max, min(tbo_term_order) as min
  					FROM $wpdb->terms AS terms
  					INNER JOIN $wpdb->term_taxonomy AS term_taxonomy ON ( terms.term_id = term_taxonomy.term_id )
  					WHERE term_taxonomy.taxonomy = '".$taxonomy."'
  				" );
  				if ( $result[0]->cnt == 0 || $result[0]->cnt == $result[0]->max ) continue;

  				$results = $wpdb->get_results( "
  					SELECT terms.term_id
  					FROM $wpdb->terms AS terms
  					INNER JOIN $wpdb->term_taxonomy AS term_taxonomy ON ( terms.term_id = term_taxonomy.term_id )
  					WHERE term_taxonomy.taxonomy = '".$taxonomy."'
  					ORDER BY tbo_term_order ASC
  				" );
  				foreach( $results as $key => $result ) {
  					$wpdb->update( $wpdb->terms, array( 'tbo_term_order' => $key+1 ), array( 'term_id' => $result->term_id ) );
  				}
  			}
  		}
  	}

    function updateMenuOrderTags()
  	{
  		global $wpdb;

  		parse_str( $_POST['sort'], $data );

  		if ( !is_array( $data ) ) return false;

  		$id_arr = array();
  		foreach( $data as $key => $values ) {
  			foreach( $values as $position => $id ) {
  				$id_arr[] = $id;
  			}
  		}

  		$menu_order_arr = array();
  		foreach( $id_arr as $key => $id ) {
  			$results = $wpdb->get_results( "SELECT tbo_term_order FROM $wpdb->terms WHERE term_id = ".intval( $id ) );
  			foreach( $results as $result ) {
  				$menu_order_arr[] = $result->tbo_term_order;
  			}
  		}
  		sort( $menu_order_arr );

  		foreach( $data as $key => $values ) {
  			foreach( $values as $position => $id ) {
  				$wpdb->update( $wpdb->terms, array( 'tbo_term_order' => $menu_order_arr[$position] ), array( 'term_id' => intval( $id ) ) );
  			}
  		}
  	}

    function tboGetTermsOrderby( $orderby, $args )
  	{
  		if ( is_admin() ) return $orderby;

  		$tags = $this->getTboOptionsTags();

  		if( !isset( $args['taxonomy'] ) ) return $orderby;

  		$taxonomy = $args['taxonomy'];
  		if ( !in_array( $taxonomy, $tags ) ) return $orderby;

  		$orderby = 't.tbo_term_order';
  		return $orderby;
  	}

  	function tboGetObjectTerms( $terms )
  	{
  		$tags = $this->getTboOptionsTags();

  		if ( is_admin() && isset( $_GET['orderby'] ) ) return $terms;

  		foreach( $terms as $key => $term ) {
  			if ( is_object( $term ) && isset( $term->taxonomy ) ) {
  				$taxonomy = $term->taxonomy;
  				if ( !in_array( $taxonomy, $tags ) ) return $terms;
  			} else {
  				return $terms;
  			}
  		}

  		usort( $terms, array( $this, 'taxcmp' ) );
  		return $terms;
  	}

  	function taxcmp( $a, $b )
  	{
  		if ( $a->tbo_term_order ==  $b->tbo_term_order ) return 0;
  		return ( $a->tbo_term_order < $b->tbo_term_order ) ? -1 : 1;
  	}

    function saveAjaxBengoOrder()
    {
        set_time_limit(600);

        global $wpdb;

				$bengo_order_type = $_POST['bengo_order_type'];
        parse_str($_POST['sort'], $data);

        if (is_array($data['item'])) {
						$total = count($data['item']);
            foreach( $data['item'] as $position => $post_id ){
								$sort = $total - $position;

                $res = $wpdb->update(
                  $wpdb->prefix . 'bengo_orders',
                  array(
                    'sort' => $sort,
                  ),
                  array(
                    'post_id' => $post_id,
                    'bengo_order_type' => $bengo_order_type,
                  )
                );
	          }
        }
    }

    function SortPage()
    {
        // 並べ替え編集対象の事務所表示順区分
        $input_bengo_order_type = ($_GET['bengo_order_type']) ? wp_unslash($_GET['bengo_order_type']) : 'rank_zenkoku';

        global $wpdb;

        $bengo_order_types = $this->getBengoOrderTypes();

        foreach ($bengo_order_types as $bot) {
          if ($bot->slug == $input_bengo_order_type) {
            $bot_label = $bot->name;
          }
        }

        ?>
        <div id="TBO" class="wrap">
            <div class="icon32" id="icon-edit"><br></div>
            <h1>弁護士事務所 - <?= $bot_label ?>&nbsp;並べ替え</h1>

            <div id="ajax-response"></div>

            <noscript>
                <div class="error message">
                    <p>このプラグインはブラウザのjavascriptを有効にしないと利用できません。</p>
                </div>
            </noscript>

            <div>
              事務所表示順区分：
              <select id="bengo_order_type_select" name="bengo_order_type">
              <?php foreach($bengo_order_types as $bot): ?>
                <option value="<?php echo $bot->slug ?>"<?php if ($input_bengo_order_type == $bot->slug) echo " selected" ?>><?php echo $bot->name ?></option>
              <?php endforeach; ?>
              </select>
              <span id="loading_indicator" style="display:none;"><img src="<?php echo TBOURL . '/image/spin.gif' ?>"></span>
            </div>
            <hr>

            <div id="bengo-order">
                <?php echo $this->listPages($input_bengo_order_type); ?>

                <div class="clear"></div>
            </div>

            <p class="submit">
                <a href="javascript: void(0)" id="save-order" class="button-primary">更新する</a>
            </p>

            <div id="overlay">
              <img src="<?php echo TBOURL . '/image/loading.gif' ?>" id="img-load"/>
            </div>

            <script type="text/javascript">
                jQuery(document).ready(function() {
                    jQuery("#bengo_order_type_select").bind("change", function() {
                      jQuery("#loading_indicator").show();
                      window.location.href = '/wp-admin/edit.php?post_type=bengo&page=bengo-order&bengo_order_type='+jQuery(this).val();
                    });
                    jQuery("#sortable").sortable({
                        'tolerance':'intersect',
                        'cursor':'pointer',
                        'items':'li',
                        'placeholder':'placeholder',
                        'nested': 'ul'
                    });

                    jQuery("#sortable").disableSelection();
                    jQuery("#save-order").bind( "click", function() {

                      jQuery("#overlay").show();
                      jQuery("html, body").animate({ scrollTop: 0 }, "fast");

                        jQuery.post( ajaxurl, {
                            action:'update-bengo-order',
                            bengo_order_type: jQuery("#bengo_order_type_select").val(),
                            sort: jQuery("#sortable").sortable("serialize")
                          },
                          function() {
                            window.location.href = '/wp-admin/edit.php?post_type=bengo&page=bengo-order&bengo_order_type='+jQuery("#bengo_order_type_select").val();
                          });
                    });
                });
            </script>

        </div>

        <?php
    }

    function listPages($bengo_order_type)
    {
        global $wpdb;

        $output = '<ul id="sortable">';

        $query = self::getQueryOfPostsByBengoOrderTypeOrdered($bengo_order_type);
        $posts =$wpdb->get_results($query);

        if ( !empty($posts) ){
			      foreach ($posts as $seq => $post){
			        $rank = $seq+1;
			        $output .= '<li id="item_'.$post->ID.'" class="ui-sortable-handle"><span>'.$rank.': '.$post->post_title.'</span></li>';
			      }
        }

        $output .= '</ul>';

        return $output;
    }

    function getBengoOrderTypes()
    {
      global $wpdb;
      $sql = "
        SELECT t.term_id, t.name, t.slug
          FROM wp_terms AS t
          INNER JOIN wp_term_taxonomy AS tt ON tt.term_id = t.term_id
          WHERE tt.taxonomy = 'bengo_order_type'
          ORDER BY t.tbo_term_order ASC
      ";

      return $wpdb->get_results($sql);
    }

    public static function getQueryOfPostsByBengoOrderTypeOrdered($bengo_order_type, $jian = null) {
      global $wpdb;

      $prepare = "
        SELECT wp_posts.ID, wp_posts.post_title, wp_bengo_orders.sort, wp_postmeta.meta_value
        FROM wp_posts
          INNER JOIN wp_bengo_orders ON (wp_posts.ID = wp_bengo_orders.post_id AND wp_bengo_orders.bengo_order_type = '%s')
          INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id)
          INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id)
          INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_taxonomy.term_id AND wp_terms.slug = '%s')
          LEFT JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.ID AND wp_postmeta.meta_key = 'bengo_order')
        WHERE wp_posts.post_type = 'bengo'
          AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'future' OR wp_posts.post_status = 'draft' OR wp_posts.post_status = 'pending' OR wp_posts.post_status = 'private')";
      if (! is_null($jian)){
        $prepare .= " AND wp_postmeta.meta_value LIKE '%%%s%%'";
      }
      $prepare .="
        GROUP BY wp_posts.ID
        ORDER BY wp_bengo_orders.sort DESC
      ";

      $args = array(
        $bengo_order_type,
        $bengo_order_type
      );
      if (! is_null($jian)){
        $args[] = $jian;
      }

      $sql = $wpdb->prepare($prepare, $args);

      return $sql;
    }
}
