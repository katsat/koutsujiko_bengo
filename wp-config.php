<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-22sS~<$0rs.9c:GV=*p`{Z;/d~S$gJ`JGV03pOG]_9gy:)+nY~X=Xe<rp<#VI,9');
define('SECURE_AUTH_KEY',  'V0YQmvBA1tLH)*gY(<G1fqshMNL&7th3UD{g=3;:I|~H<_{6M |_I|fEShY%fa1y');
define('LOGGED_IN_KEY',    '~#A6i@*~RqrFM*@(lCh;MZtBr|-Zo6{mPWB2r}cL^0&ME(,f|>?@B4-.^pamF~](');
define('NONCE_KEY',        '1{XRG]lT$x8P:q[E|>=WupQ?fS9/0on0onj-yM_&Vkm21Q:Wxf*=~{1SBS02:!2+');
define('AUTH_SALT',        'S*On%?|K9! RQfR|~4+`R-#BQqi`y4ak|>qTYpuxzORzkTl84o-YIIO2rP--L6PX');
define('SECURE_AUTH_SALT', 'JnBGMQIt@;$R!3,~v(HJ:t3e4x~]KDi-M/x-Jnfq-B-d/`3}H2hN#eBGu{HR>6t^');
define('LOGGED_IN_SALT',   '`&.3?/Z.-65*l9TCTa<5I0ih5r-COYCihwn7Iqgv2rV$C!<6C(h(-V:J)Qx4_88N');
define('NONCE_SALT',       'uo#N:)+=^|g@4i)JG;N^)r~:;[+1c1H/r({|)FiCvFn*l+gv|OZ-{(|uI]N~Be,1');


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'WP_HOME', 'http://vccw.dev' );
define( 'WP_SITEURL', 'http://vccw.dev' );
define( 'JETPACK_DEV_DEBUG', true );
define( 'WP_DEBUG', false );
define( 'FORCE_SSL_ADMIN', false );
define( 'SAVEQUERIES', false );


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
